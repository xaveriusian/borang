<head>
<script src="https://kit.fontawesome.com/11dd8dbdc4.js" crossorigin="anonymous"></script>
</head>
<ul class="menu-items">
    <!-- <li class="m-t-30 <?php if($menu ?? '' == 'dashboard'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("home")); ?>" class="detailed">Dashboard</a>
        <span class="icon-thumbnail"><i class="fa fa-tachometer-alt fs-18"></i>
        </span>
    </li>

    <li class="<?php if($menu ?? '' == 'account'): ?><?php echo e('active'); ?><?php endif; ?> visible-xs visible-md">
        <a href="<?php echo e(route("home")); ?>" class="detailed">Akun</a>
        <span class="icon-thumbnail"><i class="fas fa-user-circle fs-18"></i>
        </span>
    </li>

    <?php if(list_permit(['sermon.view'])): ?>
    <li class="<?php if($menu ?? '' == 'sermon'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("sermon.index")); ?>" class="detailed">(P) Penetapan</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-envelopes-bulk fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <?php if(list_permit(['article.view'])): ?>
    <li class="<?php if($menu ?? '' == 'article'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("article.index")); ?>" class="detailed">(P) Pelaksanaan</a>
        <span class="icon-thumbnail"><i class="fas fa-newspaper fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <?php if(list_permit(['bulletin.view'])): ?>
    <li class="<?php if($menu ?? '' == 'bulletin'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("bulletin.index")); ?>" class="detailed">(E) Evaluasi</a>
        <span class="icon-thumbnail"><i class="fa-regular fa-newspaper fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <?php if(list_permit(['pengendalian.view'])): ?>
    <li class="<?php if($menu ?? '' == 'pengendalian'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("pengendalian.index")); ?>" class="detailed">(P) Pengendalian</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-handshake-angle fs-16"></i>
        </span>
    </li>
    <?php endif; ?>
    <?php if(list_permit(['peningkatan.view'])): ?>
    <li class="<?php if($menu ?? '' == 'peningkatan'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("resolution.index")); ?>" class="detailed">(P) Peningkatan</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-arrow-up-right-dots fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <?php if(list_permit(['meeting_result.view'])): ?>
    <li class="<?php if($menu ?? '' == 'meeting_result'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("meeting-result.index")); ?>" class="detailed">(-) Lain-nya</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-clipboard-list fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <?php if(list_permit(['meeting_result.view'])): ?>
    <li class="<?php if($menu ?? '' == 'document'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="<?php echo e(route("document.index")); ?>" class="detailed">Semua Data</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-clipboard-list fs-16"></i>
        </span>
    </li>
    <?php endif; ?>

    <li class="<?php if($menu ?? '' == 'instrumen'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="instrumen" class="detailed">Instrumen Akreditasi</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-clipboard-list fs-16"></i>
        </span>
    </li>

    <?php if(list_permit(['organization.view','user.view', 'role.view', 'mui_title.view'])): ?>
    <li class="<?php if(in_array($menu ?? '', ['organization', 'user', 'role', 'mui_title'])): ?><?php echo e('active open'); ?><?php endif; ?>">
        <a href="javascript:;" class="detailed">
            <span class="title"><?php echo e(_l("administrasi")); ?></span>
            <span class="arrow <?php if(in_array($menu ?? '', ['organization', 'role', 'user',  'mui_title'])): ?><?php echo e('active open'); ?><?php endif; ?>"></span>
        </a>
        <span class="icon-thumbnail"><i class="fas fa-database fs-18"></i></span>
        <ul class="sub-menu">

            <<?php if(list_permit(['organization.view'])): ?>
            <li class="<?php if($menu ?? '' == 'organization'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("organization.index")); ?>">Organisasi</a>
                <span class="icon-thumbnail"><i class="fas fa-users fs-18"></i></span>
            </li>
            <?php endif; ?> -->

            <!-- <?php if(list_permit(['mui_title.view'])): ?>
            <li class="<?php if($menu ?? '' == 'mui_title'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("mui-title.index")); ?>">Jabatan MUI</a>
                <span class="icon-thumbnail"><i class="fas fa-user-cog fs-18"></i></span>
            </li>
            <?php endif; ?> -->


            <!-- <?php if(list_permit(['user.view'])): ?>
            <li class="<?php if($menu ?? '' == 'user'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("user.index")); ?>">User</a>
                <span class="icon-thumbnail"><i class="fas fa-user-tie fs-18"></i></span>
            </li>
            <?php endif; ?> -->

            <!-- <?php if(list_permit(['role.view'])): ?>
            <li class="<?php if($menu ?? '' == 'role'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("role.index")); ?>"><?php echo e(_l("role")); ?></a>
                <span class="icon-thumbnail"><i class="fas fa-user-shield fs-18"></i></span>
            </li>
            <?php endif; ?> -->
        <!-- </ul> -->
    </li>
    <!-- <?php endif; ?> --> 

    

    <li class="<?php if($menu ?? ''  == 'berkas'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="berkas" class="detailed">Visitasi Asesor</a>
        <span class="icon-thumbnail"><i class="fa-regular fa-newspaper fs-16"></i>
        </span>
    </li>

    <li class="<?php if($menu ?? '' == 'instrumen'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="https://uptmutu.bakrie.ac.id/downloads/60-spme/6-instrumen-akreditasi" class="detailed">Penjaminan Mutu</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-arrow-up-right-dots fs-16"></i>
        </span>
    </li>
    
    <li>
        <a href="javascript:void(0)" onclick="$('#logout-form').submit()">Logout</a>
        <span class="icon-thumbnail"><i class="fas fa-power-off fs-18"></i></span>
    </li>
</ul>
<?php /**PATH /Users/ubakriebti/Files/code/php/borang/resources/views/layouts/menu.blade.php ENDPATH**/ ?>