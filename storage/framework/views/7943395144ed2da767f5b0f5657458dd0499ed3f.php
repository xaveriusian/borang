<?php $__env->startSection('content'); ?>

<br>
<br>
<br>
<h2 style="margin-left:50px;">Instrumen Akreditasi</h2>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive" data-pattern="priority-columns">
        <table summary="This table shows how to create responsive tables using RWD-Table-Patterns' functionality" class="table table-bordered table-hover">
          <caption class="text-center">Kriteria 9 Luaran dan Capaian		<a href="http://bakrie.ac.id" target="_blank"> Universitas Bakrie</a>:</caption>
          <thead>
            <tr>
              <!-- <th>Country</th> -->
              <th data-priority="1">Kriteria 9 Luaran dan Capaian	</th>
              <th data-priority="2">Link</th>
              <!-- <th data-priority="3">Median Age</th>
              <th data-priority="4">Area (Km²)</th> -->
            </tr>
          </thead>
          <tbody>
            <tr>
              <!-- <td>Argentina</td> -->
              <td>9.1 Capaian Pembelajaran Lulusan (CPL)</td>
              <td><a data-toggle="modal" data-target="#myModal"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>31.3</td>
              <td>2,780,387</td> -->
            </tr>
            <tr>
              <!-- <td>Australia</td> -->
              <td>9.2 Rata-Rata IPK, Prestasi Mahasiswa.</td>
              <td><a data-toggle="modal" data-target="#myModal1"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal1" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>37.3</td>
              <td>7,739,983</td> -->
            </tr>
            <tr>
              <!-- <td>Greece</td> -->
              <td>9.3 Kelulusan Tepat Waktu</td>
              <td><a data-toggle="modal" data-target="#myModal2"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal2" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.4 Pelacakan dan Perekaman data Lulusan</td>
              <td><a data-toggle="modal" data-target="#myModal3"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal3" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.5 Rata-Rata Masa Tunggu</td>
              <td><a data-toggle="modal" data-target="#myModal4"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal4" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.6 Kesesuaian Bidang Kerja Dengan Bidang Program Studi</td>
              <td><a data-toggle="modal" data-target="#myModal5"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal5" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.7 Karya Dosen/Mahasiswa yang Mendapat HKI</td>
              <td><a data-toggle="modal" data-target="#myModal6"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal6" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.8 Jumlah Publikasi Penelitian DTPR Dengan Tema Bidang Infokom</td>
              <td><a data-toggle="modal" data-target="#myModal7"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal7" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.9 Jumlah Penelitian DTPR Bersama Mahasiswa Dengan Tema Bidang Infokom</td>
              <td><a data-toggle="modal" data-target="#myModal8"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal8" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.10 Jumlah Artikel Karya Ilmiah DTPR Bidang Infokom yang Disitasi</td>
              <td><a data-toggle="modal" data-target="#myModal9"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal9" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.11 Jumlah Penelitian Bidang Infokom yang Mendapat Pengakuan HKI</td>
              <td><a data-toggle="modal" data-target="#myModal10"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal10" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.12 Jumlah Kegiatan PkM yang Relevan Dengan Bidang Infokom yang Diadopsi</td>
              <td><a data-toggle="modal" data-target="#myModal11"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal11" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>9.13 Jumlah PkM Bidang Infokom yang Mendapat Pengakuan HKI</td>
              <td><a data-toggle="modal" data-target="#myModal12"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal12" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

           
          </tbody>
          <tfoot>
            <tr>
            <td colspan="5" class="text-center">Program Studi <a href="http://sisteminformasi.bakrie.ac.id" target="_blank">Sistem Informasi</a> Universitas Bakrie</td>
            </tr>
          </tfoot>
        </table>
        <button type="button" onclick="document.location='berkas' " >Kembali</button>
      </div><!--end of .table-responsive-->
    </div>
  </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/ubakriebti/Files/code/php/borang/resources/views/berkas/kriteria9.blade.php ENDPATH**/ ?>