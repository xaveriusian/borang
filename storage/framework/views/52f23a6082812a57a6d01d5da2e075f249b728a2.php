

<?php $__env->startSection("breadcrumb"); ?>
<?php echo dja_breadcrumb([route("home") => _l("dashboard"), route("user.index") => $title, "" => _l('form')]); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="<?php if(isset($data)): ?><?php echo e(route('user.update', $data->id)); ?><?php else: ?><?php echo e(route('user.store')); ?><?php endif; ?>" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php if(isset($data)): ?>
                        <?php echo method_field("put"); ?>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-6">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> Data Diri</p>
                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'name', 'label' => "Nama Lengkap", 'value' => isset($data) ? $data->name : ""]); ?>

                                <!-- <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'serial_number', 'label' => "Nomor Urut", 'value' => isset($data) ? $data->serial_number : ""]); ?> -->
                                <!-- <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'member_number', 'label' => "Nomor Anggota", 'value' => isset($data) ? $data->member_number : ""]); ?>


                                
                                <?php echo dja_form_select(['name' => 'gender','label' => _l('gender'), 'options' => $list_gender, 'value' => isset($data) ? $data->gender : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [], 'name' => 'last_education', 'label' => "Pendidikan Terakhir", 'value' => isset($data) ? $data->last_education : ""]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [], 'name' => 'name_according_to_nik', 'label' => "Nama Sesuai NIK", 'value' => isset($data) ? $data->name_according_to_nik : ""]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'nik_number', 'label' => "NIK", 'value' => isset($data) ? $data->nik_number : ""]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [], 'name' => 'birthplace', 'label' => "Tempat Lahir", 'value' => isset($data) ? $data->birthplace : ""]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'],'input_class' => 'datep ', 'name' => 'birthdate', 'label' => "Tanggal Lahir", 'value' => isset($data) ? tgl_indo($data->birthdate) : ""]); ?>


                                <?php echo dja_form_select(['name' => 'organization_id','label' => "Utusan Ormas", 'options' => $list_organization, 'value' => isset($data) ? $data->organization_id : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'phone_number', 'label' => "Handphone (I)", 'value' => isset($data) ? $data->phone_number : ""]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'phone_number_2', 'label' => "Handphone (II)", 'value' => isset($data) ? $data->phone_number_2 : ""]); ?>

                                <?php echo dja_form_select(['name' => 'mui_title_id','label' => "Jabatan di MUI", 'options' => $list_mui_titles, 'value' => isset($data) ? $data->mui_title_id : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [], 'name' => 'field', 'label' => "Bidang", 'value' => isset($data) ? $data->field : ""]); ?> -->
                            </div>

                            <!-- <div class="col-md-6">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> Alamat sesuai KTP</p>
                                <?php echo dja_form_select(['name' => 'province_as_ktp','label' => "Provinsi", 'options' => $list_provinces, 'value' => isset($data) ? $data->province_as_ktp : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_select(['name' => 'regency_as_ktp','label' => "Kabupaten/Kota", 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_select(['name' => 'district_as_ktp','label' => "Kecamatan", 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_select(['name' => 'village_as_ktp','label' => "Kelurahan", 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'rt_as_ktp', 'label' => "RT", 'value' => isset($data) ? $data->rt_as_ktp : ""]); ?>

                                    </div>
                                    <div class="col-xs-6">
                                        <?php echo dja_form_input(['type' => 'text', 'input_attr' => [ 'data-init-plugin' => 'inputmask-number'], 'name' => 'rw_as_ktp', 'label' => "RW", 'value' => isset($data) ? $data->rw_as_ktp : ""]); ?>

                                    </div>
                                </div>
                                <?php echo dja_form_input(['type' => 'text', 'input_attr' => [], 'name' => 'residence_address', 'label' => "Alamat Domisili (Bila tidak sesuai ktp)", 'value' => isset($data) ? $data->residence_address : ""]); ?>


                                <p class="text-blue-mandiri bold text-uppercase m-b-20 m-t-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> Wilayah Kerja</p>


                                <?php echo dja_form_select(['name' => 'province_working_area','label' => "Provinsi", 'options' => $list_provinces, 'value' => isset($data) ? $data->province_working_area : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_select(['name' => 'regency_working_area','label' => "Kabupaten/Kota", 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]); ?>

                                <?php echo dja_form_select(['name' => 'district_working_area','label' => "Kecamatan", 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]); ?> -->

                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> Lainnya</p>
                                <?php echo dja_form_select(['name' => 'role_id','label' => _l('role'), 'options' => $list_role, 'value' => isset($data) ? $data->role_id : '','input_attr' => ['data-init-plugin' => 'select2']]); ?>


                                <?php echo dja_form_input(['type' => 'email', 'input_attr' => ['required' => 'required'], 'name' => 'email', 'label' => _l("email"), 'value' => isset($data) ? $data->email : ""]); ?>

                                <div class="check-email fs-12 m-b-10"></div>

                                <?php echo dja_form_input(['type' => 'password', 'input_attr' => ['autocomplete' => 'off'], 'name' => 'password', 'label' => _l("password"), "hint" => $password_hint]); ?>


                                <!-- <?php if(isset($data)): ?>
                                <?php echo dja_form_input(['type' => 'file', 'input_attr' => ['accept' => 'image/*'], 'name' => 'picture', 'label' => _l("picture"), "hint" => $change_hint,'value' => '']); ?>

                                <?php else: ?>
                                <?php echo dja_form_input(['type' => 'file', 'input_attr' => ['accept' => 'image/*'], 'name' => 'picture', 'label' => _l("picture"), 'value' => ""]); ?>

                                <?php endif; ?> -->

                                <?php echo dja_form_switchery(['name' => 'is_active', 'value' => 1, 'label' => _l("active"), 'hint' => $active_hint, 'checked' => isset($data) && $data->is_active == 1 ? true : false]); ?>


                                <!-- <?php if(Auth::user()->is_admin == 1): ?>
                                <?php echo dja_form_switchery(['name' => 'is_admin', 'value' => 1, 'label' => _l("admin"), 'hint' => $admin_hint, 'checked' => isset($data) && $data->is_admin == 1 ? true : false]); ?>

                                <?php endif; ?> -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right m-t-20">
                                    <a href="<?php if(isset($data) && permit('berkas.index')): ?><?php echo e(route('user.show', $data->id)); ?><?php else: ?><?php echo e(route('user.index')); ?><?php endif; ?>" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i><?php echo e(_l('cancel')); ?></a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i><?php echo e(_l('save')); ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush("css"); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/plugins/switchery/css/switchery.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/bootstrap-datepicker/css/datepicker.css")); ?>">
<?php $__env->stopPush(); ?>


<?php $__env->startPush("scripts"); ?>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/bootstrap-select2/select2.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-validation/js/jquery.validate.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/switchery/js/switchery.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js")); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset("assets/js/form.js")); ?>"></script>

<script>
    $(function() {
        $("body").on("keyup", "#email", function() {
            if ($(this).valid() == 1 <?php if(isset($data)): ?> && $(this).val().toLowerCase() != '<?php echo e($data->email); ?>'
                <?php endif; ?>) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo e(route('user.email.check')); ?>",
                    data: {
                        "email": $('#email').val()
                    },
                    cache: false,
                    success: function(data) {
                        if (data.status == 1) {
                            $(".check-email")
                                .removeClass("text-danger")
                                .addClass("text-success")
                                .text(data.message)
                                .show();

                            $('button[type="submit"]').removeAttr("disabled");
                        } else {
                            $(".check-email")
                                .removeClass("text-success")
                                .addClass("text-danger")
                                .text(data.message)
                                .show();

                            $('button[type="submit"]').attr("disabled", true);
                        }
                    }
                });
            } else {
                $('.check-email').hide();
            }
        });
    });
</script>


<script>
    var reg_val = parseInt('<?php echo e(isset($data) ? $data->regency_as_ktp : 0); ?>')
    var dis_val = parseInt('<?php echo e(isset($data) ? $data->district_as_ktp : 0); ?>')
    var vil_val = parseInt('<?php echo e(isset($data) ? $data->village_as_ktp : 0); ?>')

    async function get_regency() {
        pro_id = $("#province_as_ktp").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/regencies/${pro_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${reg_val && reg_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            reg_val = 0
            $("#regency_as_ktp").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }

    async function get_district() {
        reg_id = $("#regency_as_ktp").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/districts/${reg_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${dis_val && dis_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            dis_val = 0
            $("#district_as_ktp").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }


    async function get_village() {
        vil_id = $("#district_as_ktp").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/villages/${vil_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${vil_val && vil_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            vil_val = 0
            $("#village_as_ktp").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }
    $(function() {
        $(document).on("change", "#province_as_ktp", function() {
            get_regency()
        })

        $(document).on("change", "#regency_as_ktp", function() {
            get_district()
        })

        $(document).on("change", "#district_as_ktp", function() {
            get_village()
        })

        $("#province_as_ktp").trigger("change")
    });
</script>


<script>
    var reg_val_working_area = parseInt('<?php echo e(isset($data) ? $data->regency_as_ktp : 0); ?>')
    var dis_val_working_area = parseInt('<?php echo e(isset($data) ? $data->district_as_ktp : 0); ?>')
    var vil_val_working_area = parseInt('<?php echo e(isset($data) ? $data->village_as_ktp : 0); ?>')

    async function get_working_area_regency() {
        pro_id = $("#province_working_area").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/regencies/${pro_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${reg_val_working_area && reg_val_working_area == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            reg_val_working_area = 0
            $("#regency_working_area").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }

    async function get_working_area_district() {
        reg_id = $("#regency_working_area").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/districts/${reg_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${dis_val_working_area && dis_val_working_area == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            dis_val_working_area = 0
            $("#district_working_area").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }

    $(function() {
        $(document).on("change", "#province_working_area", function() {
            get_working_area_regency()
        })

        $(document).on("change", "#regency_working_area", function() {
            get_working_area_district()
        })

        $("#province_working_area").trigger("change")
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/borang/resources/views/user/form.blade.php ENDPATH**/ ?>