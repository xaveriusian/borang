<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login | <?php echo e(config('app.name')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

    <meta name="description" content="">
    <meta name="author" content="megonesia.com">

    <link href="<?php echo e(asset('assets/css/megonesia.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!--[if lte IE 9]>
    <link href="<?php echo e(asset('assets/pages/css/ie9.css')); ?>" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function() {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo e(asset("assets/css/windows.chrome.fix.css")); ?>" />'
        }
    </script>
</head>

<body class="fixed-header">

    <div class="login-wrapper">
        <div class="bg-pic">
            <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                <h1 class="bold m-b-0 text-white">
                    Borang Monitoring
                </h1>
                <p>
                    Universitas Bakrie
                </p>
            </div>
        </div>
        <div class="login-container bg-white" style="position: relative">
            <?php $__errorArgs = ['active'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div id="alerts">
                <div class="alert alert-danger">
                    <i class="fa fa-exclamation-triangle m-r-10"></i><?php echo e($message); ?>

                </div>
            </div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div id="alerts">
                <div class="alert alert-danger">
                    <i class="fa fa-exclamation-triangle m-r-10"></i><?php echo e($message); ?>

                </div>
            </div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 sm-p-l-15 sm-p-r-15 sm-p-t-40">
                <img src="<?php echo e(asset('assets/img/logo.png')); ?>" alt="<?php echo e(config('app.name')); ?>" class="logo">
                <p class="p-t-10">Silahkan login menggunakan E-Mail dan Password anda.</p>
                <form id="form-login" class="" role="form" method="post" action="<?php echo e(route('login')); ?>" autocomplete="off">
                    <?php echo csrf_field(); ?>
                    <div class="form-group form-group-default">
                        <label>E-mail</label>
                        <div class="controls">
                            <input type="email" name="email" placeholder="E-mail" class="form-control" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>
                        </div>
                    </div>

                    <div class="form-group form-group-default">
                        <label>Password</label>
                        <div class="controls">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember" id="remember" class="m-r-20" <?php echo e(old('remember') ? 'checked' : ''); ?> />
                        <label for="remember" class="cursor">Remember me</label>
                    </div>
                    <button class="btn btn-success btn-block btn-cons m-t-10" type="submit"><i class="fa fa-unlock-alt m-r-10"></i> Login </button>
                </form>
                <p class="text-center m-b-0 m-t-20">
                    Doesn't have an account? <a href="<?php echo e(route('register.index')); ?>">Create now</a>.
                </p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/pace/pace.min.js")); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery/jquery-1.11.1.min.js")); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/modernizr.custom.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-ui/jquery-ui.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/boostrapv3/js/bootstrap.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery/jquery-easy.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-unveil/jquery.unveil.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-bez/jquery.bez.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-ios-list/jquery.ioslist.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-actual/jquery.actual.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/classie/classie.js")); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-validation/js/jquery.validate.min.js")); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset("assets/js/pages.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("assets/plugins/toastr/toastr.min.js")); ?>"></script>


    <script>
        $(function() {
            toastr.options.progressBar = true;
            toastr.options.timeOut = 10000;

            <?php if ($alert_message = Session::get("alert-success")) { ?>
                toastr.success('<?php echo e($alert_message); ?>');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-warning")) { ?>
                toastr.warning('<?php echo e($alert_message); ?>');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-danger")) { ?>
                toastr.error('<?php echo e($alert_message); ?>');
            <?php } ?>

            <?php if (count($errors) > 0) {
                foreach ($errors->all() as $e) {
            ?>
                    toastr.error('<?php echo e($e); ?>');
            <?php }
            } ?>
        });
    </script>

    <script>
        $(function() {
            $('#form-login').validate({
                rules: {
                    email: {
                        email: true,
                        required: true
                    },
                    password: {
                        minlength: 5,
                        required: true
                    }
                }
            })
        });

        setTimeout(function() {
            $('#alerts').slideUp();
        }, 3500);
    </script>

</body>

</html><?php /**PATH /Users/ubakriebti/Files/code/php/borang/resources/views/auth/login.blade.php ENDPATH**/ ?>