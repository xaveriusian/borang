<?php $__env->startSection('content'); ?>

<br>
<br>
<br>
<h2 style="margin-left:50px;">Instrumen Akreditasi</h2>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive" data-pattern="priority-columns">
        <table summary="This table shows how to create responsive tables using RWD-Table-Patterns' functionality" class="table table-bordered table-hover">
          <caption class="text-center">Berkas Visitasi <a href="http://bakrie.ac.id" target="_blank"> Universitas Bakrie</a>:</caption>
          <thead>
            <tr>
              <!-- <th>Country</th> -->
              <th data-priority="1">Berkas Visitasi</th>
              <th data-priority="2">Link</th>
              <!-- <th data-priority="3">Median Age</th>
              <th data-priority="4">Area (Km²)</th> -->
            </tr>
            
          </thead>
          <tbody >
          <tr>
              <!-- <td>Argentina</td> -->
              <td>Berkas Profil UPPS dan Program Studi</td>
              <td><a href="kriteria0"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>31.3</td>
              <td>2,780,387</td> -->
            </tr>
            <tr>
              <!-- <td>Argentina</td> -->
              <td>Kriteria 1 VMTS</td>
              <td><a href="kriteria1"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>31.3</td>
              <td>2,780,387</td> -->
            </tr>
            <tr>
              <!-- <td>Australia</td> -->
              <td>Kriteria 2 Tata Pamong</td>
              <td><a href="kriteria2"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>37.3</td>
              <td>7,739,983</td> -->
            </tr>
            <tr>
              <!-- <td>Greece</td> -->
              <td>Kriteria 3 Mahasiswa</td>
              <td><a href="kriteria3"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>
            <tr>
              <!-- <td>Luxembourg</td> -->
              <td>Kriteria 4 SDM</td>
              <td><a href="kriteria4"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>39.1</td>
              <td>2,586</td> -->
            </tr>
            <tr>
              <!-- <td>Russia</td> -->
              <td>Kriteria 5 Keuangan, Sarana dan Prasarana</td>
              <td><a href="kriteria5"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>38.4</td>
              <td>17,076,310</td> -->
            </tr>
            <tr>
              <!-- <td>Sweden</td> -->
              <td>Kriteria 6 Pendidikan</td>
              <td><a href="kriteria6"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
              <td>449,954</td> -->
            </tr>
            <tr>
              <!-- <td>Sweden</td> -->
              <td>Kriteria 7 Penelitian</td>
              <td><a href="kriteria7"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
              <td>449,954</td> -->
            </tr>
              <!-- <td>Sweden</td> -->
              <td>Kriteria 8 Pengabdian</td>
              <td><a href="kriteria8"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
              <td>449,954</td> -->
            </tr>
              <!-- <td>Sweden</td> -->
              <td>Kriteria 9 Luaran dan Capaian</td>
              <td><a href="kriteria9"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
              <td>449,954</td> -->
            </tr>
            <!-- <td>Sweden</td> -->
            <td>Lampiran Analisis dan Penetapan Program Pengembangan UPPS</td>
              <td><a href="kriteria10"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
            
              <td>449,954</td> -->
            </tr>
              <td>Suplement dan Program Pengembangan</td>
              <td><a href="kriteria11"><i class="fa-regular fa-folder-open"></i></a></td>
              <!-- <td>41.1</td>
              <td>449,954</td> -->
          </tbody>
          <tfoot>
            <tr>
              <td colspan="5" class="text-center">Program Studi <a href="http://sisteminformasi.bakrie.ac.id" target="_blank">Sistem Informasi</a> Universitas Bakrie</td>
            </tr>
          </tfoot>
        </table>
      </div><!--end of .table-responsive-->
    </div>
  </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/borang/resources/views/berkas/index.blade.php ENDPATH**/ ?>