

<?php $__env->startSection("breadcrumb"); ?>
<?php echo dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a href="<?php echo e(route('role.create')); ?>" class="btn btn-primary">
                        <i class="fa fa-plus m-r-10"></i> <?php echo e($title); ?>

                    </a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $table; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css")); ?>">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css")); ?>">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/datatables-responsive/css/datatables.responsive.css")); ?>" media="screen">
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/datatables-responsive/js/datatables.responsive.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/datatables-responsive/js/lodash.min.js")); ?>"></script>
<script>
    $(function() {
        dja_datatable("<?php echo e(route('role.table')); ?>");
    })
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/ubakriebti/Files/code/php/borang/resources/views/role/table.blade.php ENDPATH**/ ?>