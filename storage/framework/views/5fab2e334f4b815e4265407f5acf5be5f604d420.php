<?php $__env->startSection('content'); ?>

<br>
<br>
<br>
<h2 style="margin-left:50px;">Instrumen Akreditasi</h2>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive" data-pattern="priority-columns">
        <table summary="This table shows how to create responsive tables using RWD-Table-Patterns' functionality" class="table table-bordered table-hover">
          <caption class="text-center">Kriteria 4 SDM	<a href="http://bakrie.ac.id" target="_blank"> Universitas Bakrie</a>:</caption>
          <thead>
            <tr>
              <!-- <th>Country</th> -->
              <th data-priority="1">Kriteria 4 SDM	</th>
              <th data-priority="2">Link</th>
              <!-- <th data-priority="3">Median Age</th>
              <th data-priority="4">Area (Km²)</th> -->
            </tr>
          </thead>
          <tbody>
            <tr>
              <!-- <td>Argentina</td> -->
              <td>4.1 Kecukupan Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>31.3</td>
              <td>2,780,387</td> -->
            </tr>
            <tr>
              <!-- <td>Australia</td> -->
              <td>4.2 Kualifikasi Akademik Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal1"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal1" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>37.3</td>
              <td>7,739,983</td> -->
            </tr>
            <tr>
              <!-- <td>Greece</td> -->
              <td>4.3 Jabatan Akademik Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal2"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal2" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.4 Sertifikasi Profesi/Kompetensi/Industri Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal3"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal3" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.5 Rasio Jumlah Mahasiswa Terhadap Jumlah Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal4"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal4" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.6 Penugasan Dosen Sebagai Pembimbing Utama Tugas Akhir / Pembimbing Akademik</td>
              <td><a data-toggle="modal" data-target="#myModal5"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal5" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.7 Ekuivalensi Waktu Mengajar Penuh (EWMP)</td>
              <td><a data-toggle="modal" data-target="#myModal6"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal6" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.8 Keanggotaan Dalam Organisasi Profesi</td>
              <td><a data-toggle="modal" data-target="#myModal7"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal7" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.9 Kepemilikan Sertifikasi Profesi</td>
              <td><a data-toggle="modal" data-target="#myModal8"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal8" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.10 Dosen Tidak Tetap, Dosen Industri/Praktisi</td>
              <td><a data-toggle="modal" data-target="#myModal9"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal9" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.11 Pengakuan/Rekognisi atas Kepakaran/Prestasi/Kinerja Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal10"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal10" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.12 Publikasi Ilmiah dan Sitasi Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal11"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal11" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.13 Produk dan Jasa Karya Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal12"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal12" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.14  Iuaran Penelitian/PkM Dosen</td>
              <td><a data-toggle="modal" data-target="#myModal13"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal13" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.15 Pengembangan DTPR </td>
              <td><a data-toggle="modal" data-target="#myModal14"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal14" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.16 Pengembangan Tenaga Kependidikan</td>
              <td><a data-toggle="modal" data-target="#myModal15"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal15" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>

            <tr>
              <!-- <td>Greece</td> -->
              <td>4.17 Pengakuan/Rekognisi atas Kepakaran/Prestasi/Kinerja DTPR</td>
              <td><a data-toggle="modal" data-target="#myModal16"><i class="fa-regular fa-folder-open"></i></a></td>

              <!-- Modal -->
              <div id="myModal16" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Kriteria</h4> <br>
                    </div>
                    <div class="modal-body" style="display:flex; padding 25px 5px; justify-content:space-evenly;">
                        <a href="#"><p>Penetapan</p></a>
                        <a href="#"><p>Pelaksanaan</p></a>
                        <a href="#"><p>Evaluasi</p></a>
                        <a href="#"><p>Pengendalian</p></a>
                        <a href="#"><p>Peningkatan</p></a>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                    </div>

                </div>
                </div>
              <!-- <td>43.2</td>
              <td>131,956</td> -->
            </tr>
          </tbody>
          <tfoot>
            <tr>
            <td colspan="5" class="text-center">Program Studi <a href="http://sisteminformasi.bakrie.ac.id" target="_blank">Sistem Informasi</a> Universitas Bakrie</td>
            </tr>
          </tfoot>
        </table>
        <button type="button" onclick="document.location='berkas' " >Kembali</button>
      </div><!--end of .table-responsive-->
    </div>
  </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/borang/resources/views/berkas/kriteria4.blade.php ENDPATH**/ ?>