<head>
<script src="https://kit.fontawesome.com/11dd8dbdc4.js" crossorigin="anonymous"></script>
</head>
<ul class="menu-items">

    <li class="<?php if($menu ?? ''  == 'berkas'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="berkas" class="detailed">Visitasi Asesor</a>
        <span class="icon-thumbnail"><i class="fa-regular fa-newspaper fs-16"></i>
        </span>
    </li>

    <li class="<?php if($menu ?? '' == 'instrumen'): ?><?php echo e('active'); ?><?php endif; ?>">
        <a href="https://uptmutu.bakrie.ac.id/downloads/60-spme/6-instrumen-akreditasi" class="detailed">Penjaminan Mutu</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-arrow-up-right-dots fs-16"></i>
        </span>
    </li>
    
    <li>
        <a href="javascript:void(0)" onclick="$('#logout-form').submit()">Logout</a>
        <span class="icon-thumbnail"><i class="fas fa-power-off fs-18"></i></span>
    </li>

    
    <li class="<?php if(in_array($menu ?? '', ['organization', 'user', 'role', 'mui_title'])): ?><?php echo e('active open'); ?><?php endif; ?>">
        <a href="javascript:;" class="detailed">
            <span class="title"><?php echo e(_l("administrasi")); ?></span>
            <span class="arrow <?php if(in_array($menu ?? '', ['organization', 'role', 'user',  'mui_title'])): ?><?php echo e('active open'); ?><?php endif; ?>"></span>
        </a>
        <span class="icon-thumbnail"><i class="fas fa-database fs-18"></i></span>
        <ul class="sub-menu">
            
            <?php if(list_permit(['user.view'])): ?>
            <li class="<?php if($menu ?? '' == 'user'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("user.index")); ?>">User</a>
                <span class="icon-thumbnail"><i class="fas fa-user-tie fs-18"></i></span>
            </li>
            <?php endif; ?> 

            <?php if(list_permit(['role.view'])): ?>
            <li class="<?php if($menu ?? '' == 'role'): ?><?php echo e('active'); ?><?php endif; ?>">
                <a href="<?php echo e(route("role.index")); ?>"><?php echo e(_l("role")); ?></a>
                <span class="icon-thumbnail"><i class="fas fa-user-shield fs-18"></i></span>
            </li>
            <?php endif; ?> 
        </ul>
    </li>
    
</ul>
<?php /**PATH /private/borang/resources/views/layouts/menu.blade.php ENDPATH**/ ?>