

<?php $__env->startSection("breadcrumb"); ?>
<?php echo dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <?php if(!empty(auth()->user()->picture) && file_exists(public_path("uploads/users/" . auth()->user()->picture))): ?>
                    <a href="javascript:void(0)" onclick="$('.delete-picture').submit()" class="badge badge-danger pull-right m-b-20"><i class="fa fa-trash m-r-10"></i><?php echo e(_l('delete_picture')); ?></a>
                    <?php endif; ?>
                    <?php
                    $image = asset("assets/img/user.jpg");
                    if (!empty(auth()->user()->picture) && file_exists(public_path("uploads/users/" . auth()->user()->picture))) {
                        $image = asset("uploads/users/" . auth()->user()->picture);
                    }
                    ?>
                    <div class="p-l-60 p-r-60 text-center">
                        <img src="<?php echo e($image); ?>" alt="" class="img-responsive img-circle img-thumbnail">
                    </div>
                    <p class="text-center m-t-20 m-b-0 fs-24 bold"><?php echo e(auth()->user()->name); ?></p>
                    <p class="text-center m-t-0 m-b-0 fs-14"></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                        <li class="active"><a data-toggle="tab" href="#profile"><?php echo e(_l('profile')); ?></a></li>
                        <li><a data-toggle="tab" href="#change-password"><?php echo e(_l('change_password')); ?></a></li>
                    </ul>
                    <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                    <select class="form-control visible-xs" id="tab_selector">
                        <option value="0" selected><?php echo e(_l('profile')); ?></option>
                        <option value="1"><?php echo e(_l('change_password')); ?></option>
                    </select>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile">
                            <form method="post" action="<?php echo e(route('account.update')); ?>" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                                <?php echo method_field("put"); ?>
                                <?php echo csrf_field(); ?>
                                <?php echo $profile; ?>

                                <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i><?php echo e(_l("save")); ?></button>
                            </form>
                        </div>
                        <div class="tab-pane" id="change-password">
                            <form method="post" action="<?php echo e(route('account.password')); ?>" role="form" class="myform form-change-password" autocomplete="off" novalidate enctype="multipart/form-data">
                                <?php echo method_field('put'); ?>
                                <?php echo csrf_field(); ?>
                                <?php echo dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "new_pass1", "type" => "password", "label" => _l("new_password")]); ?>

                                <?php echo dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "new_pass2", "type" => "password", "label" => _l("confirm_new_password")]); ?>

                                <?php echo dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "password", "type" => "password", "label" => _l("current_password")]); ?>

                                <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i><?php echo e(_l("save")); ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!empty(auth()->user()->picture) && file_exists(public_path("uploads/participants/" . auth()->user()->picture))): ?>
<form action="<?php echo e(route('account.delete_picture')); ?>" method="post" class="form-delete delete-picture d-none">
    <?php echo csrf_field(); ?>
    <?php echo method_field("delete"); ?>
</form>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush("scripts"); ?>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/bootstrap-select2/select2.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-validation/js/jquery.validate.min.js")); ?>"></script>
<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });

        $(".form-change-password").validate({
            rules: {
                password: "required",
                new_pass1: "required",
                new_pass2: {
                    equalTo: "#new_pass1"
                }
            },
        });
    })
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/borang/resources/views/account/index.blade.php ENDPATH**/ ?>