

<?php $__env->startSection("breadcrumb"); ?>
<?php echo dja_breadcrumb([route("home") => _l("dashboard"), route("user.index") => $title, "" => _l('detail')]); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid container-fixed-lg">
    <h4></h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="alert alert-success fs-16 padding-20">
                                Biodata has been marked as complete.
                            </div>
                        </div>
                       
                        <div class="col-md-12">
                            <div class="d-flex justify-content-end flex-wrap">
                               
                                <button class="btn btn-success m-l-10" type="button" onclick="$('.form-mark-biodata').submit()">
                                    <i class="fas fa-check m-r-10"></i>Mark biodata as complete
                                </button>
                               
                                <?php if(list_permit(['user.edit','user.delete'])): ?>
                                <div class="dropdown m-l-10">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <?php echo e(_l("action")); ?>

                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-action">
                                        <?php if(list_permit(['user.edit'])): ?>
                                       
                                        <?php endif; ?>
                                        <?php if(list_permit(['user.edit']) && !empty($data->picture) && file_exists(public_path("uploads/users/$data->picture"))): ?>
                                        <li><a href="javascript:void(0);" onclick="$('.delete-picture').submit()"><i class="fa fa-image m-r-10 text-danger"></i><?php echo e(_l('delete_picture')); ?></a></li>
                                        <?php endif; ?>
                                        <?php if(list_permit(['user.delete'])): ?>
                                        <li><a href="javascript:void(0);" onclick="$('.delete-user').submit()"><i class="fa fa-trash m-r-10 text-danger"></i><?php echo e(_l('delete')); ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <?php endif; ?>
                                <a href="<?php echo e(route('user.index')); ?>" class="btn btn-primary m-l-10">
                                    <i class="fa fa-chevron-left m-r-10"></i><?php echo e(_l('back')); ?>

                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Detail</a></li>
                                <li><a data-toggle="tab" href="#tab-2">Alamat</a></li>
                                <li><a data-toggle="tab" href="#tab-3">Pendidikan Formal</a></li>
                                <li><a data-toggle="tab" href="#tab-4">Pengalaman Organisasi</a></li>
                                <li><a data-toggle="tab" href="#tab-5">Pengalaman Kerja</a></li>
                                <li><a data-toggle="tab" href="#tab-6">Kegiatan</a></li>
                            </ul>
                            <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                            <select class="form-control visible-xs" id="tab_selector">
                                <option value="0" selected>Detail</option>
                                <option value="1" selected>Alamat</option>
                                <option value="2" selected>Pendidikan Formal</option>
                                <option value="3" selected>Pengalaman Organisasi</option>
                                <option value="4" selected>Pengalaman Kerja</option>
                                <option value="5" selected>Kegiatan</option>
                            </select>
                        </div>
                        <div class="col-md-12 m-t-20">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <?php
                                            $image = asset("assets/img/user.jpg");
                                            if (!empty($data->picture) && file_exists(public_path("uploads/users/$data->picture"))) {
                                                $image = asset("uploads/users/$data->picture");
                                            }
                                            ?>
      
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                        <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                        Data Diri
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table class="table table-bordered table-normal">
                                                        <tbody>
                                                            <tr>
                                                                <td>Nomor Urut</td>
                                                                <td><?php echo e($data->serial_number ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nomor Anggota</td>
                                                                <td><?php echo e($data->member_number ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nama Lengkap</td>
                                                                <td><?php echo e($data->name ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Usia</td>
                                                               

                                                            <tr>
                                                                <td>Jenis Kelamin</td>
             
                                                            </tr>
                                                            <tr>
                                                                <td>Pendidikan Terakhir</td>
                                                                <td><?php echo e($data->last_education ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nama Sesuai NIK</td>
                                                                <td><?php echo e($data->name_according_to_nik ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>NIK</td>
                                                                <td><?php echo e($data->nik_number ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tempat Lahir</td>
                                                                <td><?php echo e($data->birthplace ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tanggal Lahir</td>
                                           
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered table-normal">
                                                        <tbody>
                                                            <tr>
                                                                <td>Utusan Ormas</td>
                                                   
                                                            </tr>
                                                            <tr>
                                                                <td>Handphone (I)</td>
                                                                <td><?php echo e($data->phone_number ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Handphone (II)</td>
                                                                <td><?php echo e($data->phone_number_2 ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jabatan di MUI</td>
                                                                <td><?php echo e($data->mui_title->name ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bidang</td>
                                                                <td><?php echo e($data->field ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Peran</td>
                                                                <td><?php echo e($data->role->name ?? '-'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo e(_l('email')); ?></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo e(_l('role')); ?></td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo e(_l('active')); ?></td>
                                                                <td>
                                                           
                                                                    <span class="badge badge-success"><?php echo e(_l('active')); ?></span>
                                                                
                                                                    <span class="badge badge-danger"><?php echo e(_l('inactive')); ?></span>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo e(_l('admin')); ?></td>
                                                                <td>
                                                             
                                                                    <span class="badge badge-success"><?php echo e(_l('yes')); ?></span>
                                                                   
                                                                    <span class="badge badge-danger"><?php echo e(_l('no')); ?></span>
                                                                 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                Alamat sesuai KTP
                                            </p>
                                            <table class="table table-bordered table-normal">
                                                <tbody>
                                                    <tr>
                                                        <td>Provinsi</td>
                                                        <td><?php echo e($data->ktp_province->name ?? '-'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kabupaten/Kota</td>
                                                        <td><?php echo e($data->ktp_regency->name ?? '-'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kecamatan</td>
                                                        <td><?php echo e($data->ktp_district->name ?? '-'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kelurahan</td>
                                                        <td><?php echo e($data->ktp_village->name ?? '-'); ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>RT/RW</td>
                                                        <td><?php echo e($data->rt_as_ktp ?? '-'); ?>/<?php echo e($data->rw_as_ktp ?? '-'); ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Alamat Domisili (Bila tidak sesuai ktp)</td>
                                                        <td><?php echo e($data->residence_address ?? '-'); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                Wilayah Kerja
                                            </p>
                                            <table class="table table-bordered table-normal">
                                                <tbody>
                                                    <tr>
                                                        <td>Provinsi</td>
                                                        <td><?php echo e($data->working_area_province->name ?? '-'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kabupaten/Kota</td>
                                                        <td><?php echo e($data->working_area_regency->name ?? '-'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kecamatan</td>
                                                        <td><?php echo e($data->working_area_district->name ?? '-'); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(list_permit(['user.add'])): ?>
                            
                                                <i class="fa fa-plus m-r-10"></i> Address
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            <?php echo $table_address; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(list_permit(['user.add'])): ?>
                                           
                                                <i class="fa fa-plus m-r-10"></i> Education
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            <?php echo $table_education; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(list_permit(['user.add'])): ?>
                                     
                                                <i class="fa fa-plus m-r-10"></i> Organization Experience
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            <?php echo $table_organization_experience; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(list_permit(['user.add'])): ?>
                        
                                                <i class="fa fa-plus m-r-10"></i> Work Experience
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            <?php echo $table_work_experience; ?>

                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="tab-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(list_permit(['user.add'])): ?>
                                    
                                                <i class="fa fa-plus m-r-10"></i> Experience
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            <?php echo $table_experience; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    <?php echo csrf_field(); ?>
    <?php echo method_field("delete"); ?>
</form>


<?php if(list_permit(['user.edit']) && !empty($data->picture) && file_exists(public_path("uploads/users/$data->picture"))): ?>
<form action="<?php echo e(route('user.delete_picture', $data->id)); ?>" method="post" class="form-delete delete-picture d-none">
    <?php echo csrf_field(); ?>
    <?php echo method_field("delete"); ?>
</form>
<?php endif; ?>




<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css")); ?>">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css")); ?>">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/datatables-responsive/css/datatables.responsive.css")); ?>" media="screen">
<link rel="stylesheet" href="<?php echo e(asset("assets/plugins/bootstrap-datepicker/css/datepicker.css")); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startPush("scripts"); ?>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/datatables-responsive/js/datatables.responsive.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/datatables-responsive/js/lodash.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/bootstrap-select2/select2.min.js")); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset("assets/js/form.js")); ?>"></script>

<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });

        
    })
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/borang/resources/views/user/detail.blade.php ENDPATH**/ ?>