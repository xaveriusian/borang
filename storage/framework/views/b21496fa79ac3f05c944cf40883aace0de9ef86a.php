

<?php $__env->startSection("breadcrumb"); ?>
<?php echo dja_breadcrumb([route("home") => _l("dashboard"), route("role.index") => $title, "" => _l('form')]); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="<?php if(isset($data)): ?><?php echo e(route('role.update', $data->id)); ?><?php else: ?><?php echo e(route('role.store')); ?><?php endif; ?>" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php if(isset($data)): ?>
                        <?php echo method_field("put"); ?>
                        <?php endif; ?>

                        <div class="alert alert-warning fs-16">
                            <?php if(Auth::user()->language == 1): ?>
                            Peran digunakan untuk memberikan hak akses kepada user group untuk setiap modul yang ada.
                            <?php else: ?>
                            The role is used to give access to the user group for each module.
                            <?php endif; ?>
                        </div>
                        <?php echo $form; ?>

                        <div class="table-responsive">
                            <table class="table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th><?php echo e(_l('permission')); ?></th>
                                        <th><?php echo e(_l('view')); ?></th>
                                        <th><?php echo e(_l('add')); ?></th>
                                        <th><?php echo e(_l('edit')); ?></th>
                                        <th><?php echo e(_l('delete')); ?></th>
                                        <th><?php echo e(_l('detail')); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php echo dja_role(['title' => "Khotbah", 'name' => 'sermon', 'role_permission' => $rolep]);; ?>

                                    <?php echo dja_role(['title' => "Artikel", 'name' => 'article', 'role_permission' => $rolep]);; ?>

                                    <?php echo dja_role(['title' => "Bulletin", 'name' => 'bulletin', 'role_permission' => $rolep]);; ?>

                                    <?php echo dja_role(['title' => "Hasil Rapat", 'name' => 'meeting_result', 'role_permission' => $rolep]);; ?>

                                    <?php echo dja_role(['title' => "Fatwa", 'name' => 'resolution', 'role_permission' => $rolep]);; ?>

                                    <?php echo dja_role(['title' => "Dokumen", 'name' => 'document', 'role_permission' => $rolep]);; ?>

                                    <tr>
                                        <td class="text-center bold text-uppercase" colspan="6"><?php echo e(_l('Administration')); ?></td>
                                    </tr>
                                    <tr>
                                        <?php echo dja_role(['title' => _l('organization'), 'name' => 'organization', 'role_permission' => $rolep]);; ?>

                                        <?php echo dja_role(['title' => "Jabatan MUI", 'name' => 'mui_title', 'role_permission' => $rolep]);; ?>

                                        <?php echo dja_role(['title' => _l('role'), 'name' => 'role', 'role_permission' => $rolep]);; ?>

                                        <?php echo dja_role(['title' => _l('user'), 'name' => 'user', 'role_permission' => $rolep]);; ?>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right m-t-20">
                            <a href="<?php echo e(route('role.index')); ?>" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i><?php echo e(_l('cancel')); ?></a>
                            <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i><?php echo e(_l('save')); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush("css"); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/plugins/switchery/css/switchery.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startPush("scripts"); ?>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/jquery-validation/js/jquery.validate.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("assets/plugins/switchery/js/switchery.min.js")); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset("assets/js/form.js")); ?>"></script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/ubakriebti/Files/code/php/borang/resources/views/role/form.blade.php ENDPATH**/ ?>