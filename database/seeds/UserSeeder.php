<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'name' => "Crew",
        //     'email' => 'gmail@gmail.com',
        //     'password' => Hash::make('demo1234'),
        //     'role_id' => 1,
        //     'is_admin' => 1,
        //     'is_active' => 1
        // ]);

        $new = new User();
        $new->name = "crew";
        $new->email = "gmail@gmail.com";
        $new->password = Hash::make('demo1234');
        $new->role_id = 1;
        $new->is_admin = 1;
        $new->is_active = 1;

        $new->save();
    }
}
