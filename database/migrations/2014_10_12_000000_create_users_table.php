<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->string('picture')->nullable();
            $table->integer('gender')->default(0);
            $table->integer('language')->default(1);
            $table->integer('is_active')->default(0);

            $table->string('last_ip')->nullable();
            $table->dateTime("last_login")->nullable();
            $table->string("last_device")->nullable();
            $table->integer("notification")->default(0);
            $table->integer('is_admin')->default(0);
            $table->uuid('role_id');

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
