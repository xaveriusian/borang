<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resolutions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("title");
            $table->String("content_type");
            $table->text('content')->nullable();
            $table->string("link")->nullable();

            $table->dateTime("date")->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->uuid("created_by")->default(0);
            $table->uuid("updated_by")->default(0);
            $table->uuid("deleted_by")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resolutions');
    }
}
