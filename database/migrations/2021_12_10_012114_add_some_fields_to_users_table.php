<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("serial_number")->nullable();
            $table->string("member_number")->nullable();
            $table->string("last_education")->nullable();
            $table->string("name_according_to_nik")->nullable();
            $table->string("nik_number")->nullable();
            $table->string("birthplace")->nullable();
            $table->string("province_as_ktp")->nullable();
            $table->string("regency_as_ktp")->nullable();
            $table->string("district_as_ktp")->nullable();
            $table->string("village_as_ktp")->nullable();
            $table->string("rt_as_ktp")->nullable();
            $table->string("rw_as_ktp")->nullable();
            $table->string("residence_address")->nullable();
            $table->string("phone_number_2")->nullable();
            $table->uuid("mui_title_id")->nullable();
            $table->string("field")->nullable();

            $table->string("province_working_area")->nullable();
            $table->string("regency_working_area")->nullable();
            $table->string("district_working_area")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("serial_number");
            $table->dropColumn("member_number");
            $table->dropColumn("last_education");
            $table->dropColumn("name_according_to_nik");
            $table->dropColumn("nik_number");
            $table->dropColumn("birthplace");
            $table->dropColumn("province_as_ktp");
            $table->dropColumn("regency_as_ktp");
            $table->dropColumn("district_as_ktp");
            $table->dropColumn("village_as_ktp");
            $table->dropColumn("rt_as_ktp");
            $table->dropColumn("rw_as_ktp");
            $table->dropColumn("residence_address");
            $table->dropColumn("phone_number_2");
            $table->dropColumn("mui_title_id");
            $table->dropColumn("field");

            $table->dropColumn("province_working_area");
            $table->dropColumn("regency_working_area");
            $table->dropColumn("district_working_area");
        });
    }
}
