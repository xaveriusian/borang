<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('user_id');

            $table->string("province_id");
            $table->string("regency_id");
            $table->string("district_id");
            $table->string("village_id");
            $table->string("address");
            $table->integer("postal_code");

            $table->timestamps();
            $table->softDeletes();

            $table->uuid("created_by")->default(0);
            $table->uuid("updated_by")->default(0);
            $table->uuid("deleted_by")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
