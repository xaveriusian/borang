<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneNumberToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("phone_number")->nullable();
            $table->integer("country_id")->nullable();
            $table->string("ktp_number")->nullable();
            $table->date("birthdate")->nullable();
            $table->integer("is_biodata_done")->default(0);
            $table->integer("religion")->default(0);
            $table->uuid("organization_id")->default("none");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("phone_number");
            $table->dropColumn("country_id");
            $table->dropColumn("ktp_number");
            $table->dropColumn("birthdate");
            $table->dropColumn("is_biodata_done");
            $table->dropColumn("religion");
            $table->dropColumn("organization_id");
        });
    }
}
