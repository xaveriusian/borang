@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("role.index") => $title, "" => _l('form')]) !!}
@endsection
// hihihihihihihih
@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="@if(isset($data)){{ route('role.update', $data->id) }}@else{{ route('role.store') }}@endif" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($data))
                        @method("put")
                        @endif

                        <div class="alert alert-warning fs-16">
                            @if(Auth::user()->language == 1)
                            Peran digunakan untuk memberikan hak akses kepada user group untuk setiap modul yang ada.
                            @else
                            The role is used to give access to the user group for each module.
                            @endif
                        </div>
                        {!! $form !!}
                        <div class="table-responsive">
                            <table class="table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>{{ _l('permission') }}</th>
                                        <th>{{ _l('view') }}</th>
                                        <th>{{ _l('add') }}</th>
                                        <th>{{ _l('edit') }}</th>
                                        <th>{{ _l('delete') }}</th>
                                        <th>{{ _l('detail') }}</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    {!! dja_role(['title' => "Khotbah", 'name' => 'sermon', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Artikel", 'name' => 'article', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Bulletin", 'name' => 'bulletin', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Hasil Rapat", 'name' => 'meeting_result', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Fatwa", 'name' => 'resolution', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Dokumen", 'name' => 'document', 'role_permission' => $rolep]); !!}
                                    {!! dja_role(['title' => "Berkas", 'name' => 'berkas', 'role_permission' => $rolep]); !!}
                                    <tr>
                                        <td class="text-center bold text-uppercase" colspan="6">{{ _l('Administration') }}</td>
                                    </tr>
                                    <tr>
                                        {!! dja_role(['title' => _l('organization'), 'name' => 'organization', 'role_permission' => $rolep]); !!}
                                        {!! dja_role(['title' => "Jabatan MUI", 'name' => 'mui_title', 'role_permission' => $rolep]); !!}
                                        {!! dja_role(['title' => _l('role'), 'name' => 'role', 'role_permission' => $rolep]); !!}
                                        {!! dja_role(['title' => _l('user'), 'name' => 'user', 'role_permission' => $rolep]); !!}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right m-t-20">
                            <a href="{{ route('role.index') }}" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i>{{ _l('cancel') }}</a>
                            <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l('save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
<link rel="stylesheet" href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}">
@endpush

@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/switchery/js/switchery.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>
@endpush
