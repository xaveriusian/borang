@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("peningkatan.index") => $title, "" => _l('form')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form id="form-content" method="post" action="@if(isset($data)){{ route('peningkatan.update', $data->id) }}@else{{ route('peningkatan.store') }}@endif" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($data))
                        @method("put")
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> {{ _l('form') }}</p>
                            </div>
                            <div class="col-md-12">
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'title', 'label' => _l("title"), 'value' => isset($data) ? $data->title : ""]) !!}
                                {!! dja_form_select(['name' => 'content_type','label' => _l('content type'), 'options' => $list_content_types, 'value' => isset($data) ? $data->content_type : '','input_attr' => ['data-init-plugin' => 'select2']]) !!}
                                {!! dja_form_input(['type' => 'text', 'name' => 'link', 'label' => _l("link"), 'value' => isset($data) ? $data->link : ""]) !!}
                                {!! dja_form_input(['type' => 'file', 'name' => 'file', 'label' => _l("file"), "hint" => (isset($data) && $data->content_type == "file" ? "Leave it blank if you doesn't want to change it" : ''),'value' => '']) !!}

                                {!! dja_form_input(['type' => 'textarea', 'input_attr' => ['rows' => '3'], 'name' => 'content', 'label' => _l("content"), 'value' => isset($data) ? $data->content : ""]) !!}

                            </div>
                            <div class="col-md-12">
                                @if(isset($data))
                                <button type="button" onclick="$('.form-delete').submit()" class="btn btn-danger m-t-20">
                                    <i class="fas fa-trash m-r-10"></i>Delete
                                </button>
                                @endif
                                <div class="pull-right m-t-20">
                                    <a href="{{ route('peningkatan.index') }}" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i>{{ _l('cancel') }}</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l('save') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($data))
<form class="form-delete" action="{{route('peningkatan.destroy', $data->id)}}" method="post">
    @method("DELETE")
    @csrf
</form>
@endif
@endsection

@push("css")
<link rel="stylesheet" href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}">
@endpush


@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/switchery/js/switchery.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/ckeditor/ckeditor.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>


<script>
    function change_content_type() {
        if ($("#content_type").val() == "papers") {
            $("#content").closest(".form-group").slideDown()
            $("#link").closest(".form-group").slideUp()
            $("#file").closest(".form-group").slideUp()
        } else if ($("#content_type").val() == "link") {
            $("#link").closest(".form-group").slideDown()
            $("#content").closest(".form-group").slideUp()
            $("#file").closest(".form-group").slideUp()
        } else if ($("#content_type").val() == "file") {
            $("#file").closest(".form-group").slideDown()
            $("#content").closest(".form-group").slideUp()
            $("#link").closest(".form-group").slideUp()
        }
    }
    $(function() {
        CKEDITOR.replace("content", {
            toolbar: "toolbarGroups",
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        });

        CKEDITOR.on('dialogDefinition', function(ev) {
            var dialogName = ev.data.name,
                dialogDefinition = ev.data.definition;
            if (dialogName === 'image') {
                dialogDefinition.removeContents('Upload');
            }
        });

        change_content_type()
        $(document).on("change", "#content_type", function(e) {
            change_content_type()
        })

        $("#form-content").on("submit", function(e) {
            e.preventDefault()

            if ($(this).validate()) {
                console.log($("#content").val())
                if ($("#content_type").val() == "papers" && CKEDITOR.instances.content.getData() == '') {
                    return toastr['error']('Content field is required.')
                }
                if ($("#content_type").val() == "file" && !$("#file")[0].files.length && '{{isset($data) && $data->content_type == "file" ? "yes" : "no"}}' == 'no') {
                    return toastr['error']('File field is required.')
                }
                if ($("#content_type").val() == "link" && $("#link").val() == "") {
                    return toastr['error']('Link field is required.')
                }
                if ($("#content_type").val() == "link" && !isValidHttpUrl($("#link").val())) {
                    return toastr['error']('Link is not valid.')
                }

                $(this).off("submit")
                $(this)[0].submit()
            }
        })
    });
</script>
@endpush