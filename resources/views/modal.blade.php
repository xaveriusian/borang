<div class="modal fade" role="dialog" id="my-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="my-form" method="post" action="{{ isset($action) ? $action : '' }}" role="form" class="" autocomplete="off" novalidate enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title m-b-20">{{ isset($title) ? $title : "" }}</h5>
                </div>

                <div class="modal-body">
                    {!! $form !!}
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <a href="" class="btn btn-danger but-del">
                            <i class="fa fa-trash m-r-10"></i>{{_l("delete")}}
                        </a>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ _l("cancel") }}</button>
                        <button type="submit" class="btn btn-success">{{_l("save")}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>