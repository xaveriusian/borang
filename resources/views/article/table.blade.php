@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                @if(list_permit(['article.add']))
                <div class="panel-heading">
                    <a href="{{ route('article.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus m-r-10"></i> {{$title}}
                    </a>
                </div>
                @endif
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! $table !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/datatables-responsive/css/datatables.responsive.css") }}" media="screen">
<link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-datepicker/css/datepicker.css") }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/datatables.responsive.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/lodash.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>

<script>
    $(function() {
        dja_datatable("{{ route('article.table') }}");
    })
</script>
@endpush