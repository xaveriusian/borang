@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="btn btn-primary but-add">
                        <i class="fa fa-plus m-r-10"></i> {{$title}}
                    </button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! $table !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include("modal", ['form' => $form, 'title' => $title])

@endsection

@push('css')
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/datatables-responsive/css/datatables.responsive.css") }}" media="screen">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/datatables.responsive.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/lodash.min.js") }}"></script>


<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>
<script>
    $(function() {
        var datatable = $('#mytable').DataTable({
            'processing': true,
            'serverSide': true,
            "sDom": "l<'table-responsive't><'datatable-footer'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "lengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            'ajax': {
                url: `{{ route('mui-title.table') }}`,
                type: 'post',
                error: function() {
                    $(".table-error").html("");
                    $("#mytable").append('<tbody class="table-error"><tr><th colspan="10">No data found in the server</th></tr></tbody>');
                },
                data: function(dt) {
                    $("#mytable tfoot input, #mytable tfoot select").each(function() {
                        var n = $(this).attr('name');
                        dt[n] = $('[name=' + n + ']').val();
                    });
                },
            },
            "drawCallback": function() {
                $(document).on('click', '.but-modal', function(e) {
                    e.preventDefault();

                    $.ajax({
                        type: 'GET',
                        url: $(this).attr("href"),
                        async: false,
                        success: function(data) {
                            $.each(data, function(index, value) {
                                if ($("#my-form [name='" + index + "']").length) {
                                    $("#my-form [name='" + index + "']").val(value);
                                }
                            });

                            $(".but-del").show();
                            $("#my-form")
                                .attr('method', 'PUT')
                                .attr('action', `${base_url}/mui-title/${data.id}`);

                            $(".but-del")
                                .attr('href', `${base_url}/mui-title/${data.id}`);

                            $("#my-modal").modal('show');
                        }
                    });

                });
            }
        });

        $(".but-add").click(function(e) {
            e.preventDefault();

            $("#my-form")[0].reset();
            $("#my-form")
                .attr('method', 'POST')
                .attr('action', `{{ route('mui-title.store') }}`);

            $(".but-del").hide();
            $("#my-modal").modal('show');
        });

        $("#my-form").submit(function(e) {
            e.preventDefault();
        });
        $('#my-form').validate({
            rules: {
                name: {
                    required: true,
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    url: $(form).attr('action'),
                    type: $(form).attr('method'),
                    data: $(form).serialize(),
                    error: function(e) {
                        let data = JSON.parse(e.responseText)

                        $.each(data.errors, function() {
                            $.each(this, function(index, value) {
                                toastr.error(value);
                            });
                        });
                    },
                    success: function() {
                        $("#my-modal").modal('hide');
                        if (this.type == 'PUT') {
                            toastr.success("{{ _l('data_successfully_changed') }}");
                        } else {
                            toastr.success("{{ _l('data_saved_successfully') }}");
                        }
                        datatable.ajax.reload(null, false);
                    }
                });
            }
        });

        $(document).on('click', '.but-del', function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: $(this).attr("href"),
                        success: function() {
                            $("#my-modal").modal('hide');
                            toastr.success("{{ _l('data_successfully_deleted') }}");
                            datatable.ajax.reload(null, false);
                        },
                    });
                }
            })
        });

        $(document).on('change keyup', '#mytable tfoot input, #mytable tfoot select', function() {
            datatable.ajax.reload(null, false);
        });
    })
</script>
@endpush