<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Register | {{config('app.name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

    <meta name="description" content="">
    <meta name="author" content="megonesia.com">

    <link href="{{ asset('assets/css/megonesia.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!--[if lte IE 9]>
    <link href="{{ asset('assets/pages/css/ie9.css') }}" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function() {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ asset("assets/css/windows.chrome.fix.css") }}" />'
        }
    </script>
</head>

<body class="full-screen" style="background-color: #FAFAFA;">

    <div class="w-full h-full d-flex justify-content-center align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h1 class="text-center m-b-20">
                        Daftar Sebagai
                    </h1>
                    <div class="list-group border-gray">
                        <a href="{{route('register.teologian')}}" class="list-group-item">
                            <i class="fas fa-chevron-right m-r-10 text-black"></i>Ulama/Zuama/Cendekiawan
                        </a>
                        <a href="{{route('register.user')}}" class="list-group-item">
                            <i class="fas fa-chevron-right m-r-10 text-black"></i>User
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset("assets/plugins/pace/pace.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-1.11.1.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/modernizr.custom.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/boostrapv3/js/bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-easy.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-unveil/jquery.unveil.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-bez/jquery.bez.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ios-list/jquery.ioslist.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-actual/jquery.actual.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/classie/classie.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/js/pages.min.js") }}"></script>


</body>

</html>