<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Register | {{config('app.name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

    <meta name="description" content="">
    <meta name="author" content="megonesia.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('assets/css/megonesia.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!--[if lte IE 9]>
    <link href="{{ asset('assets/pages/css/ie9.css') }}" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function() {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ asset("assets/css/windows.chrome.fix.css") }}" />'
        }
    </script>
</head>

<body class="" style="background-color: #FAFAFA;">

    <div class="w-full d-flex justify-content-center align-items-center">
        <div class="container w-full p-t-20 p-b-20">
            <form id="form-content" action="{{route('register.teologian.store')}}" method="post" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xs-12 col-md-2 col-lg-4"></div>
                    <div class="col-xs-12 col-md-8 col-lg-4">
                        <h1 class="text-center m-b-0">
                            Daftar
                        </h1>
                        <p class="m-b-20 fs-14 text-center">
                            Ulama/Zuama/Cendekiawan
                        </p>
                        {!! dja_form_input(['type' => 'text', 'name' => 'name','input_attr' => ['required' => 'required'], 'label' => 'Nama', 'value' => old('name') ?? ""]) !!}
                        {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required', 'data-init-plugin' => 'inputmask-number'], 'name' => 'phone_number', 'label' => "Nomor Telepon", 'value' => old('phone_number') ?? ""]) !!}

                        {!! dja_form_select(['name' => 'role_id','label' => "Peran", 'options' => $list_roles, 'value' => old('role_id') ?? '','input_attr' => ['data-init-plugin' => 'select2']]) !!}
                        {!! dja_form_select(['name' => 'organization_id','label' => "Organisasi", 'options' => $list_organizations, 'value' => old('organization_id') ?? '','input_attr' => ['data-init-plugin' => 'select2']]) !!}

                        {!! dja_form_input(['type' => 'email', 'input_attr' => ['required' => 'required'], 'name' => 'email', 'label' => "email", 'value' => old('email') ?? ""]) !!}
                        <div class="check-email fs-12 m-b-10"></div>

                        {!! dja_form_input(['type' => 'password', 'input_attr' => ['required' => 'required'], 'name' => 'password', 'label' => "password", 'value' => ""]) !!}
                        {!! dja_form_input(['type' => 'password', 'input_attr' => ['required' => 'required'], 'name' => 'password_confirmation', 'label' => "password confirmation", 'value' => ""]) !!}

                        <button type="submit" class="btn btn-success btn-block m-t-10">
                            Daftar
                        </button>

                        <p class="text-center m-b-0 m-t-20">
                            Bukan Ulama/Zuama/Cendekiawan? <a href="{{route('register.user')}}">Daftar sebagai user</a>.
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset("assets/plugins/pace/pace.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-1.11.1.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/modernizr.custom.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/boostrapv3/js/bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-easy.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-unveil/jquery.unveil.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-bez/jquery.bez.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ios-list/jquery.ioslist.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-actual/jquery.actual.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/classie/classie.js") }}"></script>


    <script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/js/dja_admin.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/toastr/toastr.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>


    <script>
        $(function() {
            toastr.options.progressBar = true;
            toastr.options.timeOut = 10000;

            <?php if ($alert_message = Session::get("alert-success")) { ?>
                toastr.success('{{ $alert_message }}');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-warning")) { ?>
                toastr.warning('{{ $alert_message }}');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-danger")) { ?>
                toastr.error('{{ $alert_message }}');
            <?php } ?>

            <?php if (count($errors) > 0) {
                foreach ($errors->all() as $e) {
            ?>
                    toastr.error('{{ $e }}');
            <?php }
            } ?>
        });
    </script>

    <script>
        $("body").on("keyup", "#email", function() {
            if ($(this).valid() == 1 @if(isset($data)) && $(this).val().toLowerCase() != '{{ $data->email }}'
                @endif) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('user.email.check') }}",
                    data: {
                        "email": $('#email').val()
                    },
                    cache: false,
                    success: function(data) {
                        if (data.status == 1) {
                            $(".check-email")
                                .removeClass("text-danger")
                                .addClass("text-success")
                                .text(data.message)
                                .show();

                            $('button[type="submit"]').removeAttr("disabled");
                        } else {
                            $(".check-email")
                                .removeClass("text-success")
                                .addClass("text-danger")
                                .text(data.message)
                                .show();

                            $('button[type="submit"]').attr("disabled", true);
                        }
                    }
                });
            } else {
                $('.check-email').hide();
            }
        });
    </script>
</body>

</html>