@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <iframe src="/laravel-filemanager" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection