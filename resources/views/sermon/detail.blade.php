@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("sermon.index") => $title, "" => _l('detail')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <h4>{{ $data->title }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ isset($_GET['ref']) && $_GET['ref'] == 'home' ? route('home') : route('sermon.index') }}" class="btn btn-primary pull-right">
                                <i class="fa fa-chevron-left m-r-10"></i>{{ _l('back') }}
                            </a>
                            @if(list_permit(['sermon.edit','sermon.delete']))
                            <div class="dropdown pull-right m-r-10">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {{ _l("action") }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-action">
                                    @if(list_permit(['sermon.edit']))
                                    <li><a href="{{ route("sermon.edit", $data->id) }}"><i class="fa fa-edit m-r-10 text-warning"></i>{{ _l('edit') }}</a></li>
                                    @endif
                                    @if(list_permit(['sermon.delete']))
                                    <li><a href="javascript:void(0);" onclick="$('.delete-sermon').submit()"><i class="fa fa-trash m-r-10 text-danger"></i>{{ _l('delete') }}</a></li>
                                    @endif
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                <li class="active"><a data-toggle="tab" href="#tab-1">{{ _l('detail') }}</a></li>
                            </ul>
                            <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                            <select class="form-control visible-xs" id="tab_selector">
                                <option value="0" selected>{{ _l('detail') }}</option>
                            </select>
                        </div>
                        <div class="col-md-12 m-t-20">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                {{ _l('Detail') }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-bordered table-normal">
                                                <tbody>
                                                    <tr>
                                                        <td>{{ _l('title') }}</td>
                                                        <td>{{ $data->title }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ _l('content type') }}</td>
                                                        <td>{{ $list_content_types[$data->content_type] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ _l('created by') }}</td>
                                                        <td>{{ $data->author->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ _l('created at') }}</td>
                                                        <td>{{ tgl_indo($data->created_at, 'x', 1) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                {{ _l('preview') }}
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            @if($data->content_type == 'papers')
                                            <div class="padding-20 b-a b-grey dont-break-out">
                                                {!! $data->content !!}
                                            </div>
                                            @elseif($data->content_type == "link")
                                            <div class="youtube-ratio">
                                                <iframe src="{{$data->link}}" height="600px" width="100%" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                            @elseif($data->content_type == "file" && $document)
                                            @if(get_extension($document->link) == 'doc' || get_extension($document->link) == 'docx' || get_extension($document->link) == 'xls' || get_extension($document->link) == 'xlsx' || get_extension($document->link) == 'ppt' || get_extension($document->link) == 'pptx')
                                            <iframe src='https://view.officeapps.live.com/op/embed.aspx?src={{ $document->link }}' width='100%' height='565px' frameborder='0'></iframe>
                                            @elseif(get_extension($document->link) == 'png' || get_extension($document->link) == 'jpg' || get_extension($document->link) == 'jpeg' || get_extension($document->link) == 'jfif'|| get_extension($document->link) == 'pjpeg' || get_extension($document->link) == 'pjp' || get_extension($document->link) == 'apng'|| get_extension($document->link) == 'gif'|| get_extension($document->link) == 'ico' || get_extension($document->link) == 'cur'|| get_extension($document->link) == 'svg')
                                            <img class="w-full" src="{{$document->link}}" alt="dokumen">
                                            @elseif(get_extension($document->link) == 'pdf')
                                            <iframe src="{{ $document->link }}" width="100%" height="565px" frameborder="0">
                                            </iframe>
                                            @elseif(get_extension($document->link) == 'mp4' || get_extension($document->link) == 'webm' || get_extension($document->link) == 'ogg')
                                            <video autoplay="" loop="" controls="" width="100%">
                                                <source src="{{ $document->link }}">
                                            </video>
                                            @else
                                            <p class="mb-0 p-4 bg-gray-100 border text-center">
                                                Tidak ada preview.
                                            </p>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(list_permit(['sermon.delete']))
<form action="{{ route('sermon.destroy', $data->id) }}" method="post" class="form-delete delete-sermon d-none">
    @csrf
    @method("delete")
</form>
@endif

@endsection

@push("scripts")
<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });
    })
</script>
@endpush