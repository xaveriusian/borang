@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("meeting-result.index") => $title, "" => _l('detail')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <h4>{{ $data->name }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('document.index') }}" class="btn btn-primary pull-right">
                                <i class="fa fa-chevron-left m-r-10"></i>{{ _l('back') }}
                            </a>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-12">
                            @if($extension == 'doc' || $extension == 'docx' || $extension == 'xls' || $extension == 'xlsx' || $extension == 'ppt' || $extension == 'pptx')
                            <iframe src='https://view.officeapps.live.com/op/embed.aspx?src={{ $link }}' width='100%' height='565px' frameborder='0'></iframe>
                            @elseif($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'jfif'|| $extension == 'pjpeg' || $extension == 'pjp' || $extension == 'apng'|| $extension == 'gif'|| $extension == 'ico' || $extension == 'cur'|| $extension == 'svg')
                            <img class="w-full" src="{{$link}}" alt="dokumen">
                            @elseif($extension == 'pdf')
                            <iframe src="{{ $link }}" width="100%" height="565px" frameborder="0">
                            </iframe>
                            @elseif($extension == 'mp4' || $extension == 'webm' || $extension == 'ogg')
                            <video autoplay="" loop="" controls="" width="100%">
                                <source src="{{ $link }}">
                            </video>
                            @else
                            <p class="m-b-0 padding-20 bg-master-lightest border text-center">
                                Tidak ada preview.
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(list_permit(['meeting_result.delete']))
<form action="{{ route('meeting-result.destroy', $data->id) }}" method="post" class="form-delete delete-meeting-result d-none">
    @csrf
    @method("delete")
</form>
@endif

@endsection

@push("scripts")
<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });
    })
</script>
@endpush