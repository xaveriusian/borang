@extends('layouts.master')

@section('content')
<div class="jumbotron p-t-20 p-b-20">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7">
                <h3 class="bold no-margin">Selamat datang di aplikasi Monitoring Borang</h3>
                <p></p>
            </div>
            <div class="col-md-5">
                <div class="sm-pull-reset pull-right">
                    <p class="hari"></p>
                    <h5 class="jam no-margin"></h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div id="preview-content"></div>
                    <p style="display:none;" class=" text-muted fs-12 m-b-0 m-t-20" id="preview-title"></p>
                </div>
            </div>
        </div> -->
    </div>
    <!-- <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                <li class="active"><a data-toggle="tab" href="#tab-1">(P) Penetapan</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2">(P) Pelaksanaan</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-3">(E) Evaluasi</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-4">(P) Pengendalian</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-5">(P) Peningkatan</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-6">(-) Lain-nya</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-7">Semua Data</a></li>
                            </ul>
                            <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                            <select class="form-control visible-xs" id="tab_selector">
                                <option value="0" selected>(P) Penetapan</option>
                                <option value="1" selected>(P) Pelaksanaan</option>
                                <option value="2" selected>(E) Evaluasi</option>
                                <option value="3" selected>(P) Pengendalian</option>
                                <option value="4" selected>(P) Peningkatan</option>
                                <option value="5" selected>(-) Lain-nya</option>
                                <option value="6" selected>Semua Data</option>
                            </select>
                        </div>
                        <div class="col-md-12 m-t-20">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            {!!$table_sermon!!} 
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_article!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_bulletin!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_pengendalian!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_resolution!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_meeting_result!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-7">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!$table_document!!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>

@endsection

@push('css')
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/datatables-responsive/css/datatables.responsive.css") }}" media="screen">
<link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-datepicker/css/datepicker.css") }}">
@endpush

@push("scripts")
<script type="text/javascript" src="{{ asset("assets/js/dashboard.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/datatables.responsive.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/lodash.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>

<script>
    $('#tab_selector').on('change', function(e) {
        $('.nav-tabs li a').eq($(this).val()).tab('show');
    });
</script>

<div class="padding-20 bg-light"></div>
<script>
    function preview_content(is_link = true, link_preview = null, title = null) {
        var html = `
            <div class="row">
                <div class="col-md-12">
                    <p class="m-b-0 padding-20 bg-master-lightest border text-center">
                        Tidak ada preview.
                    </p>
                </div>
            </div>
        `

        if (link_preview && is_link) {
            if (link_preview.indexOf('youtube.com/embed') > -1) {
                html = `
            <div class="row">
                <div class="col-md-3 col-lg-4"></div>
                <div class="col-md-6 col-lg-4">
                    <div class="youtube-ratio">
                        <iframe src="${link_preview} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            `
            } else if (link_preview.split('/').pop().indexOf('.') > -1) {
                var extension = link_preview.split('.')
                extension = extension[extension.length - 1]

                if (['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'].find(x => x == extension)) {
                    html = `<iframe src='https://view.officeapps.live.com/op/embed.aspx?src=${link_preview}' width='100%' style='height:250px;' frameborder='0'></iframe>`
                } else if (['png', 'jpg', 'jpeg', 'jfif', 'pjpeg', 'pjp', 'apng', 'gif', 'ico', 'cur', 'svg'].find(x => x == extension)) {
                    html = `
                    <div class="row">
                        <div class="col-md-12">
                            <div style='height:250px;' class='overflow-y-auto custom-scroll'>
                                <img class="w-full" src="${link_preview}" alt="image">
                            </div>
                        </div>
                    </div>
                    `
                } else if (['pdf'].find(x => x == extension)) {
                    html = `<iframe src="${link_preview}" width="100%" height="250px" frameborder="0"></iframe>`
                } else if (['mp4', 'webm', 'oog'].find(x => x == extension)) {
                    html = `<div class="row">
                    <div class="col-md-3 col-lg-4"></div>
                    <div class="col-md-6 col-lg-4">
                        <div class="youtube-ratio">
                            <video autoplay="" loop="" controls="">
                                <source src="${link_preview}">
                            </video>
                        </div>
                    </div>
                </div>`
                }
            } else {
                html = `<iframe src="${link_preview}" width="100%" height="250px" frameborder="0"></iframe>`
            }
        } else if (link_preview) {
            html = `
            <div class="row">
                <div class="col-md-12">
                    <div style='height:250px; border: 1px solid #d1d1d1' class='overflow-y-auto custom-scroll padding-20 bg-master-lightest'>
                        ${link_preview}
                    </div>
                </div>
            </div>
            `
        }

        $("#preview-content").html(html)

        $("#preview-title").html(`Preview: ${title}`)

        if (title) {
            $("#preview-title").slideDown()
        } else {
            $("#preview-title").slideUp()
        }
    }
    $(function() {
        preview_content()
        dja_datatable("{{ route('home.sermon.table') }}", false);
        dja_datatable("{{ route('home.article.table') }}", false, '#mytable-article');
        dja_datatable("{{ route('home.bulletin.table') }}", false, '#mytable-bulletin');
        dja_datatable("{{ route('home.meeting-result.table') }}", false, '#mytable-meeting-result');
        dja_datatable("{{ route('home.document.table') }}", false, '#mytable-document');
        dja_datatable("{{ route('home.resolution.table') }}", false, '#mytable-resolution');
        dja_datatable("{{ route('home.pengendalian.table') }}", false, '#mytable-pengendalian');
        dja_datatable("{{ route('home.peningkatan.table') }}", false, '#mytable-peningkatan');

        $(document).on("click", '[name="preview"]', function() {
            $.ajax({
                url: '{{route("home.preview")}}',
                data: {
                    type: $(this).data("type"),
                    id: $(this).val()
                },
                type: "POST",
                success: function(data) {
                    preview_content(data.data.is_link == 1 ? true : false, data.data.content, data.data.title)
                    toastr['success']('Preview changed.')
                }
            })
        })
    })
</script>
@endpush