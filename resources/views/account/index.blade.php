@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-body">
                    @if (!empty(auth()->user()->picture) && file_exists(public_path("uploads/users/" . auth()->user()->picture)))
                    <a href="javascript:void(0)" onclick="$('.delete-picture').submit()" class="badge badge-danger pull-right m-b-20"><i class="fa fa-trash m-r-10"></i>{{ _l('delete_picture') }}</a>
                    @endif
                    <?php
                    $image = asset("assets/img/user.jpg");
                    if (!empty(auth()->user()->picture) && file_exists(public_path("uploads/users/" . auth()->user()->picture))) {
                        $image = asset("uploads/users/" . auth()->user()->picture);
                    }
                    ?>
                    <div class="p-l-60 p-r-60 text-center">
                        <img src="{{ $image }}" alt="" class="img-responsive img-circle img-thumbnail">
                    </div>
                    <p class="text-center m-t-20 m-b-0 fs-24 bold">{{ auth()->user()->name }}</p>
                    <p class="text-center m-t-0 m-b-0 fs-14"></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                        <li class="active"><a data-toggle="tab" href="#profile">{{ _l('profile') }}</a></li>
                        <li><a data-toggle="tab" href="#change-password">{{ _l('change_password') }}</a></li>
                    </ul>
                    <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                    <select class="form-control visible-xs" id="tab_selector">
                        <option value="0" selected>{{ _l('profile') }}</option>
                        <option value="1">{{ _l('change_password') }}</option>
                    </select>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile">
                            <form method="post" action="{{ route('account.update') }}" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                                @method("put")
                                @csrf
                                {!! $profile !!}
                                <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l("save") }}</button>
                            </form>
                        </div>
                        <div class="tab-pane" id="change-password">
                            <form method="post" action="{{ route('account.password') }}" role="form" class="myform form-change-password" autocomplete="off" novalidate enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                {!! dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "new_pass1", "type" => "password", "label" => _l("new_password")]) !!}
                                {!! dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "new_pass2", "type" => "password", "label" => _l("confirm_new_password")]) !!}
                                {!! dja_form_input(["input_class" => "requied", "input_attr" => ["required" => "required", "autocomplete" => "off"], "name" => "password", "type" => "password", "label" => _l("current_password")]) !!}
                                <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l("save") }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (!empty(auth()->user()->picture) && file_exists(public_path("uploads/participants/" . auth()->user()->picture)))
<form action="{{ route('account.delete_picture') }}" method="post" class="form-delete delete-picture d-none">
    @csrf
    @method("delete")
</form>
@endif

@endsection

@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });

        $(".form-change-password").validate({
            rules: {
                password: "required",
                new_pass1: "required",
                new_pass2: {
                    equalTo: "#new_pass1"
                }
            },
        });
    })
</script>
@endpush