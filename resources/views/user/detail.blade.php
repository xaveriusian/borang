@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("user.index") => $title, "" => _l('detail')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <h4></h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="alert alert-success fs-16 padding-20">
                                Biodata has been marked as complete.
                            </div>
                        </div>
                       
                        <div class="col-md-12">
                            <div class="d-flex justify-content-end flex-wrap">
                               
                                <button class="btn btn-success m-l-10" type="button" onclick="$('.form-mark-biodata').submit()">
                                    <i class="fas fa-check m-r-10"></i>Mark biodata as complete
                                </button>
                               
                                @if(list_permit(['user.edit','user.delete']))
                                <div class="dropdown m-l-10">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        {{ _l("action") }}
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-action">
                                        @if(list_permit(['user.edit']))
                                       
                                        @endif
                                        @if(list_permit(['user.edit']) && !empty($data->picture) && file_exists(public_path("uploads/users/$data->picture")))
                                        <li><a href="javascript:void(0);" onclick="$('.delete-picture').submit()"><i class="fa fa-image m-r-10 text-danger"></i>{{ _l('delete_picture') }}</a></li>
                                        @endif
                                        @if(list_permit(['user.delete']))
                                        <li><a href="javascript:void(0);" onclick="$('.delete-user').submit()"><i class="fa fa-trash m-r-10 text-danger"></i>{{ _l('delete') }}</a></li>
                                        @endif
                                    </ul>
                                </div>
                                @endif
                                <a href="{{ route('user.index') }}" class="btn btn-primary m-l-10">
                                    <i class="fa fa-chevron-left m-r-10"></i>{{ _l('back') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Detail</a></li>
                                <li><a data-toggle="tab" href="#tab-2">Alamat</a></li>
                                <li><a data-toggle="tab" href="#tab-3">Pendidikan Formal</a></li>
                                <li><a data-toggle="tab" href="#tab-4">Pengalaman Organisasi</a></li>
                                <li><a data-toggle="tab" href="#tab-5">Pengalaman Kerja</a></li>
                                <li><a data-toggle="tab" href="#tab-6">Kegiatan</a></li>
                            </ul>
                            <div class="panel-group visible-xs" id="CvOhF-accordion"></div>
                            <select class="form-control visible-xs" id="tab_selector">
                                <option value="0" selected>Detail</option>
                                <option value="1" selected>Alamat</option>
                                <option value="2" selected>Pendidikan Formal</option>
                                <option value="3" selected>Pengalaman Organisasi</option>
                                <option value="4" selected>Pengalaman Kerja</option>
                                <option value="5" selected>Kegiatan</option>
                            </select>
                        </div>
                        <div class="col-md-12 m-t-20">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <?php
                                            $image = asset("assets/img/user.jpg");
                                            if (!empty($data->picture) && file_exists(public_path("uploads/users/$data->picture"))) {
                                                $image = asset("uploads/users/$data->picture");
                                            }
                                            ?>
      
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                        <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                        Data Diri
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table class="table table-bordered table-normal">
                                                        <tbody>
                                                            <tr>
                                                                <td>Nomor Urut</td>
                                                                <td>{{ $data->serial_number ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nomor Anggota</td>
                                                                <td>{{ $data->member_number ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nama Lengkap</td>
                                                                <td>{{ $data->name ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Usia</td>
                                                               

                                                            <tr>
                                                                <td>Jenis Kelamin</td>
             
                                                            </tr>
                                                            <tr>
                                                                <td>Pendidikan Terakhir</td>
                                                                <td>{{ $data->last_education ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nama Sesuai NIK</td>
                                                                <td>{{ $data->name_according_to_nik ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>NIK</td>
                                                                <td>{{ $data->nik_number ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tempat Lahir</td>
                                                                <td>{{ $data->birthplace ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tanggal Lahir</td>
                                           
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered table-normal">
                                                        <tbody>
                                                            <tr>
                                                                <td>Utusan Ormas</td>
                                                   
                                                            </tr>
                                                            <tr>
                                                                <td>Handphone (I)</td>
                                                                <td>{{ $data->phone_number ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Handphone (II)</td>
                                                                <td>{{ $data->phone_number_2 ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jabatan di MUI</td>
                                                                <td>{{ $data->mui_title->name ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bidang</td>
                                                                <td>{{ $data->field ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Peran</td>
                                                                <td>{{ $data->role->name ?? '-' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>{{ _l('email') }}</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>{{ _l('role') }}</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>{{ _l('active') }}</td>
                                                                <td>
                                                           
                                                                    <span class="badge badge-success">{{ _l('active') }}</span>
                                                                
                                                                    <span class="badge badge-danger">{{ _l('inactive') }}</span>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>{{ _l('admin') }}</td>
                                                                <td>
                                                             
                                                                    <span class="badge badge-success">{{ _l('yes') }}</span>
                                                                   
                                                                    <span class="badge badge-danger">{{ _l('no') }}</span>
                                                                 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                Alamat sesuai KTP
                                            </p>
                                            <table class="table table-bordered table-normal">
                                                <tbody>
                                                    <tr>
                                                        <td>Provinsi</td>
                                                        <td>{{ $data->ktp_province->name ?? '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kabupaten/Kota</td>
                                                        <td>{{ $data->ktp_regency->name ?? '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kecamatan</td>
                                                        <td>{{ $data->ktp_district->name ?? '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kelurahan</td>
                                                        <td>{{ $data->ktp_village->name ?? '-' }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>RT/RW</td>
                                                        <td>{{ $data->rt_as_ktp ?? '-' }}/{{ $data->rw_as_ktp ?? '-' }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Alamat Domisili (Bila tidak sesuai ktp)</td>
                                                        <td>{{ $data->residence_address ?? '-' }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6">
                                            <p class="text-blue-mandiri bold text-uppercase m-b-20">
                                                <i class="fas fa-bookmark text-yellow-mandiri"></i>
                                                Wilayah Kerja
                                            </p>
                                            <table class="table table-bordered table-normal">
                                                <tbody>
                                                    <tr>
                                                        <td>Provinsi</td>
                                                        <td>{{ $data->working_area_province->name ?? '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kabupaten/Kota</td>
                                                        <td>{{ $data->working_area_regency->name ?? '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kecamatan</td>
                                                        <td>{{ $data->working_area_district->name ?? '-' }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(list_permit(['user.add']))
                            
                                                <i class="fa fa-plus m-r-10"></i> Address
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            {!!$table_address!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(list_permit(['user.add']))
                                           
                                                <i class="fa fa-plus m-r-10"></i> Education
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            {!!$table_education!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(list_permit(['user.add']))
                                     
                                                <i class="fa fa-plus m-r-10"></i> Organization Experience
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            {!!$table_organization_experience!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(list_permit(['user.add']))
                        
                                                <i class="fa fa-plus m-r-10"></i> Work Experience
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            {!!$table_work_experience!!}
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="tab-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(list_permit(['user.add']))
                                    
                                                <i class="fa fa-plus m-r-10"></i> Experience
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-12">
                                            {!!$table_experience!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    @csrf
    @method("delete")
</form>


@if(list_permit(['user.edit']) && !empty($data->picture) && file_exists(public_path("uploads/users/$data->picture")))
<form action="{{ route('user.delete_picture', $data->id) }}" method="post" class="form-delete delete-picture d-none">
    @csrf
    @method("delete")
</form>
@endif




@endsection

@push('css')
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/datatables-responsive/css/datatables.responsive.css") }}" media="screen">
<link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-datepicker/css/datepicker.css") }}">
@endpush

@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/datatables.responsive.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/lodash.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>

<script>
    $(function() {
        $('#tab_selector').on('change', function(e) {
            $('.nav-tabs li a').eq($(this).val()).tab('show');
        });

        
    })
</script>
@endpush