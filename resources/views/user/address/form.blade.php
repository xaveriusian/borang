@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("user.show", $user->id) => $title, "" => _l('form')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="{{ isset($data) ? route('user.address.update', [$user->id, $data->id])  : route('user.address.store', $user->id)  }}" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($data))
                        @method("put")
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> {{ _l('form') }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                {!! dja_form_select(['name' => 'province_id','label' => _l('province'), 'options' => $list_provinces, 'value' => isset($data) ? $data->province_id : '','input_attr' => ['data-init-plugin' => 'select2']]) !!}
                                {!! dja_form_select(['name' => 'regency_id','label' => _l('regency'), 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]) !!}
                                {!! dja_form_select(['name' => 'district_id','label' => _l('district'), 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]) !!}
                                {!! dja_form_select(['name' => 'village_id','label' => _l('village'), 'options' => [], 'value' => '','input_attr' => ['data-init-plugin' => 'select2']]) !!}

                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'postal_code', 'label' => _l("postal code"), 'value' => isset($data) ? $data->postal_code : ""]) !!}

                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'address', 'label' => _l("address"), 'value' => isset($data) ? $data->address : ""]) !!}

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex justify-content-between m-t-20">
                                    @if(isset($data))
                                    <button type="button" class="btn btn-danger" onclick="$('.form-delete').submit()"><i class="fa fa-trash m-r-10"></i>{{ _l('delete') }}</button>
                                    @endif
                                    <div class="m-l-auto">
                                        <a href="{{ route('user.show', $user->id) }}" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i>{{ _l('cancel') }}</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l('save') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($data) && list_permit(['user.delete']))
<form action="{{ route('user.address.destroy', [$user->id, $data->id]) }}" method="post" class="form-delete d-none">
    @csrf
    @method("delete")
</form>
@endif
@endsection

@push("css")
<link rel="stylesheet" href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}">
<link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-datepicker/css/datepicker.css") }}">
@endpush


@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/switchery/js/switchery.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>

<script>
    var reg_val = parseInt('{{isset($data) ? $data->regency_id : 0}}')
    var dis_val = parseInt('{{isset($data) ? $data->district_id : 0}}')
    var vil_val = parseInt('{{isset($data) ? $data->village_id : 0}}')

    async function get_regency() {
        pro_id = $("#province_id").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/regencies/${pro_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${reg_val && reg_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            reg_val = 0
            $("#regency_id").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }

    async function get_district() {
        reg_id = $("#regency_id").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/districts/${reg_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${dis_val && dis_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            dis_val = 0
            $("#district_id").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }


    async function get_village() {
        vil_id = $("#district_id").val()

        try {
            const send = await $.ajax({
                url: `${base_url}/region/villages/${vil_id}`,
                type: 'GET'
            })

            var html = ''
            send.data.forEach(val => {
                html += `<option value="${val.id}" ${vil_val && vil_val == val.id ? 'selected' : ''}>${val.name}</option>`
            })
            vil_val = 0
            $("#village_id").html(html).select2().trigger("change")
        } catch (error) {
            return false
        }
    }
    $(function() {
        $(document).on("change", "#province_id", function() {
            get_regency()
        })

        $(document).on("change", "#regency_id", function() {
            get_district()
        })

        $(document).on("change", "#district_id", function() {
            get_village()
        })

        $("#province_id").trigger("change")
    });
</script>
@endpush