@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("user.show", $user->id) => $title, "" => _l('form')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="{{ isset($data) ? route('user.work-experience.update', [$user->id, $data->id])  : route('user.work-experience.store', $user->id)  }}" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($data))
                        @method("put")
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> {{ _l('form') }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'company', 'label' => _l("company"), 'value' => isset($data) ? $data->company : ""]) !!}
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'title', 'label' => 'Jabatan', 'value' => isset($data) ? $data->title : ""]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'input_class' => "datep", 'name' => 'start_date', 'label' => _l("start date"), 'value' => isset($data) ? tgl_indo($data->start_date) : ""]) !!}
                            </div>
                            <div class="col-md-6">
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'input_class' => "datep", 'name' => 'end_date', 'label' => _l("end date"), 'value' => isset($data) ? tgl_indo($data->end_date) : ""]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex justify-content-between m-t-20">
                                    @if(isset($data))
                                    <button type="button" class="btn btn-danger" onclick="$('.form-delete').submit()"><i class="fa fa-trash m-r-10"></i>{{ _l('delete') }}</button>
                                    @endif
                                    <div class="m-l-auto">
                                        <a href="{{ route('user.show', $user->id) }}" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i>{{ _l('cancel') }}</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l('save') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($data) && list_permit(['user.delete']))
<form action="{{ route('user.work-experience.destroy', [$user->id, $data->id]) }}" method="post" class="form-delete d-none">
    @csrf
    @method("delete")
</form>
@endif
@endsection

@push("css")
<link rel="stylesheet" href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}">
<link rel="stylesheet" href="{{ asset("assets/plugins/bootstrap-datepicker/css/datepicker.css") }}">
@endpush


@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/switchery/js/switchery.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>
@endpush