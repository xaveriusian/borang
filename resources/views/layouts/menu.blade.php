<head>
<script src="https://kit.fontawesome.com/11dd8dbdc4.js" crossorigin="anonymous"></script>
</head>
<ul class="menu-items">

    <li class="@if($menu ?? ''  == 'berkas'){{ 'active' }}@endif">
        <a href="berkas" class="detailed">Visitasi Asesor</a>
        <span class="icon-thumbnail"><i class="fa-regular fa-newspaper fs-16"></i>
        </span>
    </li>

    <li class="@if($menu ?? '' == 'instrumen'){{ 'active' }}@endif">
        <a href="https://uptmutu.bakrie.ac.id/downloads/60-spme/6-instrumen-akreditasi" class="detailed">Penjaminan Mutu</a>
        <span class="icon-thumbnail"><i class="fa-solid fa-arrow-up-right-dots fs-16"></i>
        </span>
    </li>
    
    <li>
        <a href="javascript:void(0)" onclick="$('#logout-form').submit()">Logout</a>
        <span class="icon-thumbnail"><i class="fas fa-power-off fs-18"></i></span>
    </li>

    
    <li class="@if(in_array($menu ?? '', ['organization', 'user', 'role', 'mui_title'])){{ 'active open' }}@endif">
        <a href="javascript:;" class="detailed">
            <span class="title">{{ _l("administrasi") }}</span>
            <span class="arrow @if(in_array($menu ?? '', ['organization', 'role', 'user',  'mui_title'])){{ 'active open' }}@endif"></span>
        </a>
        <span class="icon-thumbnail"><i class="fas fa-database fs-18"></i></span>
        <ul class="sub-menu">
            
            @if(list_permit(['user.view']))
            <li class="@if($menu ?? '' == 'user'){{ 'active' }}@endif">
                <a href="{{ route("user.index") }}">User</a>
                <span class="icon-thumbnail"><i class="fas fa-user-tie fs-18"></i></span>
            </li>
            @endif 

            @if(list_permit(['role.view']))
            <li class="@if($menu ?? '' == 'role'){{ 'active' }}@endif">
                <a href="{{ route("role.index") }}">{{ _l("role") }}</a>
                <span class="icon-thumbnail"><i class="fas fa-user-shield fs-18"></i></span>
            </li>
            @endif 
        </ul>
    </li>
    
</ul>
