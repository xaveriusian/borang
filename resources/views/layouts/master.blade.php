<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">

    <title> Borang Monitoring - Universitas Bakrie</title>
    <link rel="icon" type="image/x-icon" href="/assets/img/favicon.png">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('asset/img/icons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('asset/img/icons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('asset/img/icons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('asset/img/icons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('asset/img/icons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('asset/img/icons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('asset/img/icons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('asset/img/icons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('asset/img/icons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('asset/img/icons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('asset/img/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('asset/img/icons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('asset/img/icons/favicon-16x16.png')}}">
    
    <!-- <link rel="manifest" href="{{asset('manifest.json')}}"> -->
    <meta name="msapplication-TileColor" content="#4b72fe">
    <meta name="msapplication-TileImage" content="{{asset('asset/img/i')}}">
    <meta name="theme-color" content="#4b72fe">

    <meta name="robots" content="index, follow" />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <link rel="canonical" href="{{url('/')}}" />

    <link rel="stylesheet" href="{{ asset("assets/css/megonesia.min.css")}} ">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script type="text/javascript">
        var base_url = '{{ URL::to("/") }}';
    </script>
    <script src="https://use.fontawesome.com/3d14d504ca.js"></script>
    <!--[if lte IE 9]>
    <link href="{{ asset('assets/plugins/codrops-dialogFx/dialog.ie.css') }}" rel="stylesheet" media="screen"/>
    <![endif]-->

    @stack('css')
</head>

<body class="fixed-header my-layout">

    <nav class="page-sidebar" data-pages="sidebar">
        <div class="sidebar-header">
            <div class="logo">
                <img src="{{asset('assets/img/logo-uptmutu.png')}}">
            </div>

            <div class="sidebar-header-controls">
                <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
                    <i class="fas fa-circle"></i>
                </button>
            </div>
        </div>
        <div class="sidebar-menu">
            @include("layouts.menu")
            <div class="clearfix"></div>
        </div>
    </nav>
    <div class="page-container">
        <div class="header">
            <div class="container-fluid relative">
                <div class="pull-left full-height visible-sm visible-xs">
                    <div class="header-inner">
                        <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                            <i class="fas fa-bars"></i>
                        </a>
                    </div>
                </div>

                <div class="pull-left hidden-md hidden-lg">
                    <div class="header-inner">
                        <div class="brand inline">
                            <img src="{{asset('assets/img/favicon.png')}}" alt="{{config('app.name')}}">
                        </div>
                    </div>
                </div>
                <!-- <div class="pull-right hidden-md hidden-lg">
                    <ul class="notification-list no-margin no-style p-t-10">
                        <li class="p-r-15 inline">
                            <div class="dropdown" style="position:static;">
                                <a href="" class="notification-center" data-toggle="dropdown">
                                    <i class="typcn typcn-bell"></i>
                                    @if(count(get_my_unread_notification()))
                                    <span class="badge badge-danger">
                                       
                                    </span>
                                    @endif
                                </a>
                                <div class="dropdown-menu notification-toggle pull-right" style="top:45px;" role="menu">
                                    <div class="notification-panel">
                                        <div class="notification-body scrollable">
                                            @foreach(get_my_unread_notification() as $n)
                                            <div class="notification-item learfix">
                                                <div class="heading">
                                                    <a href="{{ url($n->link ? $n->link : '') }}" class="d-block">
                                                        <span class="">{{ $n->message }}</span>
                                                        <br>
                                                        <span class="time text-lowercase">{{ time_ago($n->created_at) }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="notification-footer text-center">
                                            <a href="{{ route('notification.index') }}" class="">Baca Semua Notifikasi</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> -->
            </div>
            <div class="pull-left sm-table hidden-xs hidden-sm">
                <div class="header-inner">
                    <div class="brand inline">
                        <img src="{{asset('assets/img/logo-uptmutu.png')}}" alt="{{config('app.name')}}" style="width:120px;height:60px;">
                    </div>

                    <!-- <ul class="notification-list no-margin b-grey b-l b-r no-style p-l-30 p-r-20">
                        <li class="p-r-15 inline">
                            <div class="dropdown">
                                <a href="" class="notification-center" data-toggle="dropdown">
                                    <i class="typcn typcn-bell"></i>
                                    @if(count(get_my_unread_notification()))
                                    <span class="badge badge-danger">
                                        <?php
                                        if (count(get_my_unread_notification()) <= 9) {
                                            echo count(get_my_unread_notification());
                                        } else {
                                            echo "9+";
                                        }
                                        ?>
                                    </span>
                                    @endif
                                </a>
                                <div class="dropdown-menu notification-toggle" role="menu">
                                    <div class="notification-panel">
                                        <div class="notification-body scrollable">
                                            @foreach(get_my_unread_notification() as $n)
                                            <div class="notification-item learfix">
                                                <div class="heading">
                                                    <a href="{{ url($n->link ? $n->link : '') }}" class="d-block">
                                                        <span class="">{{ $n->message }}</span>
                                                        <br>
                                                        <span class="time text-lowercase">{{ time_ago($n->created_at) }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="notification-footer text-center">
                                            <a href="{{ route('notification.index') }}" class="">Baca Semua Notifikasi</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul> -->

                </div>
            </div>
            <div class="pull-right">
                <div class="visible-lg visible-md m-t-10">
                    <div class="dropdown pull-right">
                        <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="semi-bold pull-left p-r-10 p-t-10 fs-16 font-heading">
                                {{Auth::user()->name}}
                                <i class="fas fa-feather-alt p-l-10"></i>
                            </span>
                        </button>
                        <ul class="dropdown-menu profile-dropdown" role="menu">
                            <li><a href="{{ route("account.index") }}"><i class="fas fa-user-circle fs-18"></i><span class="m-l-5">{{ _l('account') }}</span></a>
                            </li>
                            <li class="bg-master-lighter">
                                <a href="javascript:void(0);" class="clearfix" onclick="$('#logout-form').submit()">
                                    <span class="pull-left">Logout</span>
                                    <span class="pull-right"><i class="fas fa-power-off fs-18"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="content">
                @if(View::hasSection("breadcrumb"))
                <div class="jumbotron" data-pages="parallax">
                    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                        <div class="inner">
                            <h2 class="clearfix"><span class="icon-thumbnail pull-left"><i class="{{ $icon ? $icon : 'typcn typcn-home' }}"></i></span>{{ $title ? $title : "Dashboard" }}</h2>
                            @yield("breadcrumb")
                        </div>
                    </div>
                </div>
                @endif
                @yield("content")
            </div>
            <!-- <div class="container-fluid container-fixed-lg footer">
                <div class="copyright sm-text-center">
                    <p class="small no-margin pull-left sm-pull-reset">
                        <span class="hint-text">Copyright &copy; {{date('Y')}}</span>
                        <span class="font-montserrat"></span>
                        Created by
                        <a href="https://megonesia.com/" target="_blank" class="font-montserrat">MEGONESIA</a>.
                    </p>
                </div>
            </div> -->
        </div>

    </div>

    <form id="logout-form" action="{{ route("logout") }}" method="post" class="d-none">
        @csrf
    </form>

    <script type="text/javascript" src="{{ asset("assets/plugins/pace/pace.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-1.11.1.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/plugins/modernizr.custom.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/boostrapv3/js/bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery/jquery-easy.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-unveil/jquery.unveil.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-bez/jquery.bez.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-ios-list/jquery.ioslist.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-actual/jquery.actual.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/classie/classie.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/toastr/toastr.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/plugins/sweetalert.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/js/dja_admin.js") }}"></script>
    @stack('scripts')

    <script type="text/javascript" src="{{ asset("assets/js/pages.min.js") }}"></script>


    <script>
        $(function() {
            toastr.options.progressBar = true;
            toastr.options.timeOut = 10000;

            <?php if ($alert_message = Session::get("alert-success")) { ?>
                toastr.success('{{ $alert_message }}');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-warning")) { ?>
                toastr.warning('{{ $alert_message }}');
            <?php } ?>
            <?php if ($alert_message = Session::get("alert-danger")) { ?>
                toastr.error('{{ $alert_message }}');
            <?php } ?>

            <?php if (count($errors) > 0) {
                foreach ($errors->all() as $e) {
            ?>
                    toastr.error('{{ $e }}');
            <?php }
            } ?>
        });
    </script>


</body>

</html>