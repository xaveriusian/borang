@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), "" => $title]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover" id="mytable" width="100%">
                                <thead>
                                    <tr>
                                        <th>Notifikasi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $d)
                                    <tr>
                                        <td>{{ $d->message }}</td>
                                        <td class="text-lowercase">{{ time_ago($d->created_at) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("assets/plugins/datatables-responsive/css/datatables.responsive.css") }}" media="screen">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.print.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/JSZip/jszip.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/pdfmake.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-datatable/extensions/pdfmake/vfs_fonts.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/datatables.responsive.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/datatables-responsive/js/lodash.min.js") }}"></script>


<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>
<script>
    $(function() {
        var datatable = $('#mytable').DataTable({
            "order": [
                [0, "desc"]
            ],
            "columns": [{
                    "width": "80%"
                },
                {
                    "width": "20%"
                }
            ],
        });

    });
</script>
@endpush