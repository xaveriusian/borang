@extends('layouts.master')

@section("breadcrumb")
{!! dja_breadcrumb([route("home") => _l("dashboard"), route("subscription.index") => $title, "" => _l('form')]) !!}
@endsection

@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="post" action="@if(isset($data)){{ route('subscription.update', $data->id) }}@else{{ route('subscription.store') }}@endif" role="form" class="myform" autocomplete="off" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($data))
                        @method("put")
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-blue-mandiri bold text-uppercase m-b-20"><i class="fas fa-bookmark text-yellow-mandiri"></i> {{ _l('form') }}</p>
                            </div>
                            <div class="col-md-12">
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'name' => 'name', 'label' => _l("name"), 'value' => isset($data) ? $data->name : ""]) !!}

                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'input_class' => 'angka', 'name' => 'price', 'label' => _l("price"), 'value' => isset($data) ? $data->price : ""]) !!}
                                {!! dja_form_input(['type' => 'text', 'input_attr' => ['required' => 'required'], 'input_class' => 'angka', 'name' => 'duration', 'label' => _l("duration") . " (" . _l('days') . ")", 'value' => isset($data) ? $data->duration : ""]) !!}
                            </div>
                            <div class="col-md-12">
                                @if(isset($data))
                                <button type="button" onclick="$('.form-delete').submit()" class="btn btn-danger m-t-20">
                                    <i class="fas fa-trash m-r-10"></i>Delete
                                </button>
                                @endif
                                <div class="pull-right m-t-20">
                                    <a href="{{ route('subscription.index') }}" class="btn btn-default m-r-10"><i class="fa fa-times m-r-10"></i>{{ _l('cancel') }}</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check m-r-10"></i>{{ _l('save') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($data))
<form class="form-delete" action="{{route('subscription.destroy', $data->id)}}" method="post">
    @method("DELETE")
    @csrf
</form>
@endif
@endsection

@push("css")
<link rel="stylesheet" href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}">
@endpush


@push("scripts")
<script type="text/javascript" src="{{ asset("assets/plugins/bootstrap-select2/select2.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation/js/jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/jquery.inputmask.bundle.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/inputmask/dist/inputmask/inputmask.numeric.extensions.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/switchery/js/switchery.min.js") }}"></script>

<script type="text/javascript" src="{{ asset("assets/js/form.js") }}"></script>
@endpush