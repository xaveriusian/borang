<?php

use App\Http\Middleware\SubscriptedMiddleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/access-denied', function () {
    return abort(403);
})->name("access-denied");
Route::get('/register', 'RegisterController@index')->name("register.index");
Route::get('/register/teologian', 'RegisterController@teologian')->name("register.teologian");
Route::post('/register/teologian', 'RegisterController@teologian_store')->name("register.teologian.store");
Route::get('/register/user', 'RegisterController@user')->name("register.user");
Route::post('/register/user', 'RegisterController@user_store')->name("register.user.store");
Route::post('/user/email/check', 'UserController@emailCheck')->name("user.email.check");
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/home/sermon/table', 'HomeController@sermon_table')->name('home.sermon.table');
    Route::post('/home/article/table', 'HomeController@article_table')->name('home.article.table');
    Route::post('/home/pengendalian/table', 'HomeController@pengendalian_table')->name('home.pengendalian.table');
   
    Route::post('/home/lainnya/table', 'HomeController@lainnya_table')->name('home.lainnya.table');
    Route::post('/home/peningkatan/table', 'HomeController@peningkatan')->name('home.peningkatan.table');
    Route::post('/home/bulletin/table', 'HomeController@bulletin_table')->name('home.bulletin.table');
    Route::post('/home/meeting-result/table', 'HomeController@meeting_result_table')->name('home.meeting-result.table');
    Route::post('/home/document/table', 'HomeController@document_table')->name('home.document.table');
    Route::post('/home/resolution/table', 'HomeController@resolution_table')->name('home.resolution.table');
    Route::post('/home/preview', 'HomeController@preview')->name('home.preview');

    Route::get('/account', 'AccountController@index')->name('account.index');
    Route::put('/account/update', 'AccountController@update')->name('account.update');
    Route::put('/account/password', 'AccountController@password')->name('account.password');
    Route::delete('/account/delete-picture', 'AccountController@delete_picture')->name('account.delete_picture');

    Route::resource('/user', 'UserController');
    Route::post('/user/table', 'UserController@table')->name('user.table');
    Route::post('/user/{id}/mark-biodata', 'UserController@mark_biodata')->name('user.mark-biodata');
    Route::delete('/user/delete_picture/{id}', 'UserController@delete_picture')->name('user.delete_picture');

    Route::post('/user/{id}/address/table', 'UserController@address_table')->name("user.address.table");
    Route::get("user/{id}/address/create", "UserController@address_create")->name("user.address.create");
    Route::post('/user/{id}/address/store', 'UserController@address_store')->name("user.address.store");
    Route::get("user/{id}/address/{address_id}/edit", "UserController@address_edit")->name("user.address.edit");
    Route::put('/user/{id}/address/{address_id}', 'UserController@address_update')->name("user.address.update");
    Route::delete('/user/{id}/address/{address_id}', 'UserController@address_destroy')->name("user.address.destroy");

    Route::post('/user/{id}/education/table', 'UserController@education_table')->name("user.education.table");
    Route::get("user/{id}/education/create", "UserController@education_create")->name("user.education.create");
    Route::post('/user/{id}/education/store', 'UserController@education_store')->name("user.education.store");
    Route::get("user/{id}/education/{education_id}/edit", "UserController@education_edit")->name("user.education.edit");
    Route::put('/user/{id}/education/{education_id}', 'UserController@education_update')->name("user.education.update");
    Route::delete('/user/{id}/education/{education_id}', 'UserController@education_destroy')->name("user.education.destroy");

    Route::post('/user/{id}/organization-experience/table', 'UserController@organization_experience_table')->name("user.organization-experience.table");
    Route::get("user/{id}/organization-experience/create", "UserController@organization_experience_create")->name("user.organization-experience.create");
    Route::post('/user/{id}/organization-experience/store', 'UserController@organization_experience_store')->name("user.organization-experience.store");
    Route::get("user/{id}/organization-experience/{organization_experience_id}/edit", "UserController@organization_experience_edit")->name("user.organization-experience.edit");
    Route::put('/user/{id}/organization-experience/{organization_experience_id}', 'UserController@organization_experience_update')->name("user.organization-experience.update");
    Route::delete('/user/{id}/organization-experience/{organization_experience_id}', 'UserController@organization_experience_destroy')->name("user.organization-experience.destroy");

    Route::post('/user/{id}/work-experience/table', 'UserController@work_experience_table')->name("user.work-experience.table");
    Route::get("user/{id}/work-experience/create", "UserController@work_experience_create")->name("user.work-experience.create");
    Route::post('/user/{id}/work-experience/store', 'UserController@work_experience_store')->name("user.work-experience.store");
    Route::get("user/{id}/work-experience/{work_experience_id}/edit", "UserController@work_experience_edit")->name("user.work-experience.edit");
    Route::put('/user/{id}/work-experience/{work_experience_id}', 'UserController@work_experience_update')->name("user.work-experience.update");
    Route::delete('/user/{id}/work-experience/{work_experience_id}', 'UserController@work_experience_destroy')->name("user.work-experience.destroy");

    Route::post('/user/{id}/experience/table', 'UserController@experience_table')->name("user.experience.table");
    Route::get("user/{id}/experience/create", "UserController@experience_create")->name("user.experience.create");
    Route::post('/user/{id}/experience/store', 'UserController@experience_store')->name("user.experience.store");
    Route::get("user/{id}/experience/{experience_id}/edit", "UserController@experience_edit")->name("user.experience.edit");
    Route::put('/user/{id}/experience/{experience_id}', 'UserController@experience_update')->name("user.experience.update");
    Route::delete('/user/{id}/experience/{experience_id}', 'UserController@experience_destroy')->name("user.experience.destroy");

    Route::post('/notification/read', 'NotificationController@read')->name("notification.read");
    Route::get('/notification', 'NotificationController@index')->name("notification.index");

    Route::resource('/role', 'RoleController');
    Route::post('/role/table', 'RoleController@table')->name("role.table");

    Route::resource('/language', 'LanguageController')->except(["show", "create"]);
    Route::post('/language/table', 'LanguageController@table')->name("language.table");


    Route::get('/file-manager', 'FileManagerController@index')->name('file-manager.index');

    Route::resource('/document', 'DocumentController');
    Route::post('/document/table', 'DocumentController@table')->name("document.table");

    Route::resource('/sermon', 'SermonController');
    Route::post('/sermon/table', 'SermonController@table')->name("sermon.table");

    Route::resource('/article', 'ArticleController');
    Route::post('/article/table', 'ArticleController@table')->name("article.table");

    Route::resource('/lainnya', 'LainnyaController');
    Route::post('/lainnya/table', 'LainnyaController@table')->name("lainnya.table");

    Route::resource('/pengendalian', 'PengendalianController');
    Route::post('/pengendalian/table', 'PengendalianController@table')->name("pengendalian.table");

    Route::resource('/peningkatan', 'PeningkatanController');
    Route::post('/peningkatan/table', 'PeningkatanController@table')->name("peningkatan.table");

    Route::resource('/bulletin', 'BulletinController');
    Route::post('/bulletin/table', 'BulletinController@table')->name("bulletin.table");

    Route::resource('/meeting-result', 'MeetingResultController');
    Route::post('/meeting-result/table', 'MeetingResultController@table')->name("meeting-result.table");

    Route::resource('/organization', 'OrganizationController')->except(["show", "create"]);
    Route::post('/organization/table', 'OrganizationController@table')->name("organization.table");

    Route::resource('/mui-title', 'MuiTitleController')->except(["show", "create"]);
    Route::post('/mui-title/table', 'MuiTitleController@table')->name("mui-title.table");

    Route::resource('/resolution', 'ResolutionController');
    Route::post('/resolution/table', 'ResolutionController@table')->name("resolution.table");

    Route::get('/region/regencies/{id}', 'RegionController@get_regencies')->name('region.regencies');
    Route::get('/region/districts/{id}', 'RegionController@get_districts')->name('region.districts');
    Route::get('/region/villages/{id}', 'RegionController@get_villages')->name('region.villages');
});

Route::get('instrumen', function(){
     return Redirect::to('https://uptmutu.bakrie.ac.id/downloads/60-spme/6-instrumen-akreditasi');
});

Route::get('/berkas', function () {
    return view('berkas.index');
});

Route::get('/kriteria1', function () {
    return view('berkas.kriteria1');
});

Route::get('/kriteria2', function () {
    return view('berkas.kriteria2');
});

Route::get('/kriteria3', function () {
    return view('berkas.kriteria3');
});

Route::get('/kriteria4', function () {
    return view('berkas.kriteria4');
});

Route::get('/kriteria5', function () {
    return view('berkas.kriteria5');
});

Route::get('/kriteria6', function () {
    return view('berkas.kriteria6');
});

Route::get('/kriteria7', function () {
    return view('berkas.kriteria7');
});

Route::get('/kriteria8', function () {
    return view('berkas.kriteria8');
});

Route::get('/kriteria9', function () {
    return view('berkas.kriteria9');
});

Route::get('/kriteria0', function () {
    return view('berkas.kriteria0');
});