// const staticCache = 'static-cache';
// const dynamicCache = 'dynamic-cache';
// const assets = [
//     '/',
//     '/error/no-connection',
//     '/assets/css/megonesia.css',
//     '/assets/js/jquery-3.5.1.min.js',
//     '/assets/js/bootstrap.bundle.min.js',
//     '/assets/js/megonesia.js',
//     '/assets/font/Poppins-Regular.ttf',
//     '/assets/font/Poppins-Bold.ttf',
//     '/assets/font/Poppins-Light.ttf',
//     '/assets/font/remixicon.woff2?t=1590207869815',
//     '/assets/img/budaya-tv-logo.png',
//     '/assets/plugins/owl-carousel-2/owl.carousel.min.js',
//     '/assets/plugins/owl-carousel-2/owl.carousel.min.css',
//     '/assets/plugins/owl-carousel-2/owl.theme.default.min.css'

// ];

self.addEventListener('install', e => {
    // e.waitUntil(
    //     caches
    //         .open(staticCache)
    //         .then(cache => {
    //             cache.addAll(assets)
    //         })
    // )
})

self.addEventListener('activate', e => {
})

self.addEventListener('fetch', e => {
    // e.respondWith(
    //     caches.match(e.request).then(res => {
    //         return res || fetch(e.request)
    //     })
    //     .catch(() => caches.match('/error/no-connection'))
    // )
})

// dynamic offline pages (problem in /article page, causes by ajax i think)
// self.addEventListener('fetch', e => {
//     e.respondWith(
//         caches.match(e.request).then(staticRes => {
//             return staticRes || fetch(e.request).then(dynamicRes => {
//                 return caches.open(dynamicCache).then(cache => {
//                     cache.put(e.request.url, dynamicRes.clone())
//                     return dynamicRes
//                 })
//             })
//         })
//     )
// })