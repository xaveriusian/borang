CKEDITOR.editorConfig = function (config) {
    config.toolbarGroups = [{
            name: 'insert',
            groups: ['insert']
        },
        {
            name: 'document',
            groups: ['mode', 'document', 'doctools']
        },
        {
            name: 'clipboard',
            groups: ['clipboard', 'undo']
        },
        {
            name: 'editing',
            groups: ['find', 'selection', 'spellchecker', 'editing']
        },
        {
            name: 'forms',
            groups: ['forms']
        },
        '/',
        {
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup']
        },
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']
        },
        {
            name: 'links',
            groups: ['links']
        },
        '/',
        {
            name: 'styles',
            groups: ['styles']
        },
        {
            name: 'colors',
            groups: ['colors']
        },
        {
            name: 'tools',
            groups: ['tools']
        },
        {
            name: 'others',
            groups: ['others']
        },
        {
            name: 'about',
            groups: ['about']
        }
    ];

    // config.removeButtons = 'Templates,Save,Preview,ExportPdf,Print,PasteText,PasteFromWord,Source,About,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Styles,Format,Font,FontSize,Paste,CreateDiv,Scayt';
    // config.removeDialogTabs = 'link:target;link:advanced;image:Link;image:advanced;';
    config.basicEntities = false;
    config.entities_greek = false;
    config.entities_latin = false;
    config.entities_additional = '';
};
