if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/sw.js");
}

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

function dja_datatable(ajax_link, show_buttons = true, elId = "#mytable") {
  if (show_buttons === true) {
    var buttons_list = [
      "csv",
      "excel",
      {
        extend: "pdfHtml5",
        orientation: "landscape",
        pageSize: "LEGAL",
      },
      "print",
    ];
  } else {
    var buttons_list = [];
  }
  var dtable = $(elId).DataTable({
    processing: true,
    serverSide: true,
    buttons: buttons_list,
    sDom: "lB<'table-responsive't><'row'<p i>>",
    destroy: true,
    scrollCollapse: true,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "All"],
    ],
    ajax: {
      url: ajax_link,
      type: "post",
      error: function() {
        $(".table-error").html("");
        $(elId).append(
          '<tbody class="table-error"><tr><td colspan="7">No data found in the server</td></tr></tbody>'
        );
      },
      data: function(data) {
        $(`${elId} tfoot input, ${elId} tfoot select`).each(function(index) {
          var n = $(this).attr("name");
          data[n] = $("[name=" + n + "]").val();
        });
      },
    },
    drawCallback: function() {
      $("[rel='tooltip']").tooltip({
        html: true,
        container: "body",
      });
    },
  });

  $(document).on(
    "change keyup",
    `${elId} tfoot input, ${elId} tfoot select`,
    function() {
      dtable.ajax.reload(null, false);
    }
  );

  return dtable;
}

function matchYoutubeUrl(url) {
  var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  var matches = url.match(p);
  if (matches) {
    return matches[1];
  }
  return false;
}

function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}

function tgl_dt(tgl) {
  var arr_tgl = tgl.split(" ");
  var arr_tgl_1 = tgl.split("-");

  var output = `${arr_tgl_1[2]}-${arr_tgl_1[1]}-${arr_tgl_1[0]}`;

  if (arr_tgl[1]) {
    output += ` ${arr_tgl[1]}`;
  }

  return output;
}

$(function() {
  $("body").on("click", " .full-view-image", function() {
    $(".full-view-image").removeClass("active");
    $(".full-view-image")
      .find("img")
      .attr("src", "");
  });

  $('[data-toggle="full-view-image"]').css({
    cursor: "pointer",
  });

  $("body").on("click", '[data-toggle="full-view-image"]', function() {
    $(".full-view-image").addClass("active");
    $(".full-view-image")
      .find("img")
      .attr("src", $(this).attr("src"));
  });

  $(".notification-center").on("click", function(e) {
    e.preventDefault();

    $.ajax({
      type: "post",
      url: "notification/read",
      success: function() {
        $(".notification-center .badge").remove();
      },
    });
  });

  $(".form-delete").on("submit", function(e) {
    e.preventDefault();
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes!",
    }).then((result) => {
      if (result.value) {
        $(this).off("submit");
        $(this).submit();
      }
    });
  });

  $("body").on("click", "[data-toggle='file-preview']", function() {
    var URL = $(this).data("url");
    var strWindowFeatures =
      "location=0,height=" +
      screen.availHeight +
      ",width=" +
      screen.availWidth +
      ",scrollbars=yes,status=no,menubar=no,toolbar=no,centerscreen=yes";

    window.open(URL, "_blank", strWindowFeatures, true);
  });
});
