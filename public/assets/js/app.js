$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
    if ($(window).scrollTop() < 1) {
        $(".navbar").removeClass("active")
    } else {
        $(".navbar").addClass("active")
    }

    $(window).on("scroll", function () {
        if ($(window).scrollTop() < 1) {
            $(".navbar").removeClass("active")
        } else {
            $(".navbar").addClass("active")
        }
    })

    $("body").on("click", ".navbar-toggle, .navbar-menu, .navbar-close", () => {
        if ($(".navbar-toggle").hasClass("active")) {
            $(".navbar-toggle").removeClass("active")
            $(".navbar-menu").removeClass("active")
            $(".navbar-close").removeClass("active")
        } else {
            $(".navbar-toggle").addClass("active")
            $(".navbar-menu").addClass("active")
            $(".navbar-close").addClass("active")
        }
    })
})

$(window).on("load", function () {

});

$(function () {
})