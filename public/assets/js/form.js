$(function() {
  if ($(".datetimep").length) {
    $(".datetimep").each(function() {
      $(this).flatpickr({
        enableTime: true,
        dateFormat: "d-m-Y H:i",
        disableMobile: "true",
      });
    });
  }

  if ($(".yearp").length) {
    $(".yearp").each(function() {
      $(this).datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
      });
    });
  }

  if ($(".datep").length) {
    $(".datep").each(function() {
      $(this).datepicker({
        format: "dd-mm-yyyy",
      });
    });
  }
  if ($(".angka").length) {
    $(".angka").each(function() {
      $(this).inputmask("decimal", {
        radixPoint: ".",
        groupSeparator: ",",
        clearMaskOnLostFocus: false,
        rightAlign: false,
      });
    });
  }

  if ($(".myform").length) {
    $(".myform").each(function() {
      $(this).validate();
    });
  }

  if ($(".yearp").length) {
    $(".yearp").each(function() {
      $(this).datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
      });
    });
  }

  if ($(".telp").length) {
    $(".telp").each(function() {
      $(this).inputmask({
        mask: "(+62) 999-9999-99999",
      });
    });
  }

  if ($('[data-init-plugin="inputmask-whatsapp"]').length) {
    $('[data-init-plugin="inputmask-whatsapp"]').inputmask({
      mask: [
        "629999999",
        "6299999999",
        "62999999999",
        "629999999999",
        "6299999999999",
        "62999999999999",
        "629999999999999",
      ],
      keepStatic: true,
    });
  }

  if ($('[data-init-plugin="inputmask-number"]').length) {
    $('[data-init-plugin="inputmask-number"]').inputmask({
      mask: "9",
      repeat: 14,
      greedy: false,
    });
  }
});
