$(function () {
    function jam() {
        setInterval(renderTime, 40);
    }

    function renderTime() {
        var nama_bulan = [
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni", "Juli",
            "Agustus", "September", "Oktober",
            "November", "Desember"
        ];

        var nama_hari = [
            "Minggu", "Senin", "Selasa",
            "Rabu", "Kamis", "Jumat", "Sabtu"
        ];

        var now = new Date();
        var today = now.toDateString(now);
        var time = now.toLocaleTimeString(now);
        var tgl = now.getDate(now);
        var bulan = now.getMonth(now);
        var thn = now.getFullYear(now);
        var hari = now.getDay(now);

        $(".hari").html(nama_hari[hari] + ', ' + tgl + ' ' + nama_bulan[bulan] + ' ' + thn);
        $(".jam").html(time);
    }

    jam();
});
