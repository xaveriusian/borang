<?php

namespace App\Models;

use App\Traits\ModelUuids;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingResult extends Model
{
    use SoftDeletes, ModelUuids;

    public function author()
    {
        return $this->belongsTo(User::class, "created_by", "id");
    }
}
