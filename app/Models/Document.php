<?php

namespace App\Models;

use App\Traits\ModelUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes, ModelUuids;
}
