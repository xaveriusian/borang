<?php

namespace App;

use App\Models\District;
use App\Models\MuiTitle;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Role;
use App\Models\Village;
use App\Models\Whatsapp;
use App\Traits\ModelUuids;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, ModelUuids;
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ktp_province()
    {
        return $this->belongsTo(Province::class, "province_as_ktp", "id");
    }


    public function ktp_regency()
    {
        return $this->belongsTo(Regency::class, "regency_as_ktp", "id");
    }


    public function ktp_district()
    {
        return $this->belongsTo(District::class, "district_as_ktp", "id");
    }


    public function ktp_village()
    {
        return $this->belongsTo(Village::class, "village_as_ktp", "id");
    }

    public function working_area_province()
    {
        return $this->belongsTo(Province::class, "province_working_area", "id");
    }


    public function working_area_regency()
    {
        return $this->belongsTo(Regency::class, "regency_working_area", "id");
    }


    public function working_area_district()
    {
        return $this->belongsTo(District::class, "district_working_area", "id");
    }

    public function mui_title()
    {
        return $this->belongsTo(MuiTitle::class, "mui_title_id", "id");
    }

    public function role()
    {
        return $this->belongsTo(Role::class, "role_id", "id");
    }
}
