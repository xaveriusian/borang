<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

function dja_table($data, $class, $id)
{

    $output = "<table class='$class' id='$id' width='100%'>";
    $output .= dja_table_thead($data);
    $output .= dja_table_tfoot($data);
    $output .= "</table>";
    return $output;
}

function dja_table_thead($array)
{
    $output = '<thead><tr>';
    foreach ($array as $key => $value) {
        if (!in_array('no-table', $value)) {
            if (array_key_exists('title', $value)) {
                $output .= "<th>" . _l($value['title']) . "</th>";
            } else {
                $judul = _l(get_judul($key));
                $output .= "<th>$judul</th>";
            }
        }
    }

    $output .= "</tr></thead>";
    return $output;
}

function dja_table_tfoot($column_search)
{
    $output = '<tfoot><tr>';
    foreach ($column_search as $key => $value) {
        if (!in_array('no-table', $value)) {
            if (array_key_exists('type', $value)) {
                if ($value['type'] == 'like' || $value['type'] == 'file') {
                    $output .= "<th>";
                    if (array_key_exists('sub-type', $value)) {
                        if ($value['sub-type'] == 'date' || $value['sub-type'] == 'datetime') {
                            $output .= dja_input([
                                "name" => $key,
                                "class" => "datep",
                                "attr" => ["placeholder" => "Tanggal"],
                            ]);
                        } elseif ($value['sub-type'] == 'year') {
                            $output .= dja_input([
                                "name" => $key,
                                "class" => "yearp",
                                "attr" => ["placeholder" => "Tahun"]
                            ]);
                        } else {
                            $output .= dja_input([
                                "name" => $key
                            ]);
                        }
                    } else {
                        $output .= dja_input([
                            "name" => $key
                        ]);
                    }
                    $output .= "</th>";
                } elseif ($value['type'] == 'from') {
                    $output .= "<th>";
                    if (array_key_exists('sub-type', $value)) {
                        if ($value['sub-type'] == 'number') {
                            if (array_key_exists('field', $value)) {
                                $output .= dja_input([
                                    "name" => $value['field'][0],
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $value['field'][1],
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            } else {
                                $output .= dja_input([
                                    "name" => $key . '_from',
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $key . '_to',
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            }
                        } elseif ($value['sub-type'] == 'date' || $value['sub-type'] == 'datetime') {
                            if (array_key_exists('field', $value)) {
                                $output .= dja_input([
                                    "name" => $value['field'][0],
                                    "class" => "datep",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $value['field'][1],
                                    "class" => "datep",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            } else {
                                $output .= dja_input([
                                    "name" => $key . '_from',
                                    "class" => "datep",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $key . '_to',
                                    "class" => "datep",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            }
                        } elseif ($value['sub-type'] == 'year') {
                            if (array_key_exists('field', $value)) {
                                $output .= dja_input([
                                    "name" => $value['field'][0],
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $value['field'][1],
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            } else {
                                $output .= dja_input([
                                    "name" => $key . '_from',
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "From"]
                                ]);
                                $output .= dja_input([
                                    "name" => $key . '_to',
                                    "class" => "angka",
                                    "attr" => ["placeholder" => "To"]
                                ]);
                            }
                        } else {
                            $output .= dja_input([
                                "name" => $key . '_from',
                                "class" => "angka",
                                "attr" => ["placeholder" => "From"]
                            ]);
                            $output .= dja_input([
                                "name" => $key . '_to',
                                "class" => "angka",
                                "attr" => ["placeholder" => "To"]
                            ]);
                        }
                    } else {
                        $output .= dja_input([
                            "name" => $key . '_from',
                            "class" => "angka",
                            "attr" => ["placeholder" => "From"]
                        ]);
                        $output .= dja_input([
                            "name" => $key . '_to',
                            "class" => "angka",
                            "attr" => ["placeholder" => "To"]
                        ]);
                    }
                    $output .= "</th>";
                } elseif ($value['type'] == 'date' || $value['type'] == 'datetime') {
                    $output .= "<th>";

                    $output .= dja_input($key, '', 'datep', "placeholder='Tanggal'");
                    $output .= dja_input([
                        "name" => $key,
                        "class" => "datep",
                        "attr" => ["placeholder" => "Tanggal"]
                    ]);

                    $output .= "</th>";
                } elseif ($value['type'] == 'option') {
                    $output .= "<th>" . dja_select([
                        'name' => $key,
                        'options' => $value['data'],
                        'first' => ['' => "All"],
                        'attr' => ["data-init-plugin" => "select2"]
                    ]) . "</th>";
                } else {
                    $output .= "<th></th>";
                }
            }
        }
    }
    $output .= '</tr></tfoot>';
    return $output;
}

function dt_get($table_name, $query = [])
{
    // $join = [], $column_search = [], $where = [], $like = [], $group_by = [], $or_like = [], $or_where = [], $order = '', $urut = 'desc'
    $table = dt_query($table_name, $query);
    if ($_POST['length'] != -1) {
        $table->skip($_POST['start'])
            ->take($_POST['length']);
    }

    return $table->get();
}

function dt_query($table_name, $query = [])
{
    $table = DB::table($table_name);

    if (Schema::hasColumn($table_name, 'deleted_at')) {
        if (!isset($query['mode'])) {
            $table->where('deleted_at', '=', null);
        } elseif ($query['mode'] == 'trash') {
            $table->where('deleted_at', '!=', null);
        }
    }

    if (array_key_exists("where", $query)) {
        $table->where($query["where"]);
    }

    if (array_key_exists('join', $query)) {
        foreach ($query['join'] as $jk => $jv) {
            $jd = explode(' ', $jv);
            $table->leftJoin($jk, $jd[0], $jd[1], $jd[2]);
        }
    }

    if (array_key_exists("group_by", $query)) {
        $table->groupBy($query["group_by"]);
    }

    if (array_key_exists("or_where", $query)) {
        $table->orWhere($query["or_where"]);
    }
    if (array_key_exists("order", $query)) {
        $table->orderBy(array_keys(($query["order"])), array_values($query["order"]));
    }

    if (array_key_exists("where_in", $query)) {
        $table->whereIn(array_keys($query['where_in'])[0], array_values($query['where_in'])[0]);
    }

    if (array_key_exists("column_search", $query)) {
        foreach ($query['column_search'] as $c => $k) {
            if (!in_array("no-table", $k)) {
                if ($k['type'] == 'like') {
                    if ($_POST[$c] != '') {
                        $table->where($c, 'like', '%' . $_POST[$c] . '%');
                    }
                } elseif ($k['type'] == 'option') {
                    if ($_POST[$c] != '') {
                        if (array_key_exists('join', $k)) {
                            $table->where($k['join'], $_POST[$c]);
                        } else {
                            if ($_POST[$c] != '') {
                                $table->where($c, $_POST[$c]);
                            }
                        }
                    }
                } elseif ($k['type'] == 'date' || $k['type'] == 'datetime') {
                    if ($_POST[$c] != '') {
                        $table->where($c, tgl_dt($_POST[$c]));
                    }
                } elseif ($k['type'] == 'from') {
                    if (array_key_exists('sub-type', $k)) {
                        if ($k['sub-type'] == 'number') {
                            if (array_key_exists('field', $k)) {
                                if ($_POST[$k['field'][0]] != '') {
                                    $table->where($c, '>=', $_POST[$k['field'][0]]);
                                }

                                if ($_POST[$k['field'][1]] != '') {
                                    $table->where($c, '<=', $_POST[$k['field'][1]]);
                                }
                            } else {
                                if ($_POST[$c . '_from'] != '') {
                                    $table->where($c, '>=', $_POST[$c . '_from']);
                                }

                                if ($_POST[$c . '_to'] != '') {
                                    $table->where($c, '<=', $_POST[$c . '_to']);
                                }
                            }
                        } elseif ($k['sub-type'] == 'date' || $k['sub-type'] == 'datetime') {
                            if (array_key_exists('field', $k)) {
                                if ($_POST[$k['field'][0]] != '') {
                                    $table->where($c . '>=', tgl_dt($_POST[$k['field'][0]]));
                                }

                                if ($_POST[$k['field'][1]] != '') {
                                    $table->where($c, '<=', tgl_dt($_POST[$k['field'][1]]));
                                }
                            } else {
                                if ($_POST[$c . '_from'] != '') {
                                    $table->where($c, '>=', tgl_dt($_POST[$c . '_from']));
                                }

                                if ($_POST[$c . '_to'] != '') {
                                    $table->where($c, '<=', tgl_dt($_POST[$c . '_to']));
                                }
                            }
                        }
                    } else {
                        if ($_POST[$c . '_from'] != '') {
                            $table->where($c, '>=', $_POST[$c . '_from']);
                        }

                        if ($_POST[$c . '_to'] != '') {
                            $table->where($c, '<=', $_POST[$c . '_to']);
                        }
                    }
                }
            }
        }
    }

    $column_order = get_clean_array($query['column_search']);

    if (isset($_POST['order'])) {
        $table->orderBy($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }

    return $table;
}

function dt_count_all($table_name, $query = [])
{
    $table = DB::table($table_name);

    if (Schema::hasColumn($table_name, 'deleted_at')) {
        if (!isset($query['mode'])) {
            $table->where('deleted_at', '=', null);
        } elseif ($query['mode'] == 'trash') {
            $table->where('deleted_at', '!=', null);
        }
    }

    if (array_key_exists("where", $query)) {
        $table->where($query["where"]);
    }

    if (array_key_exists("where_in", $query)) {
        $table->whereIn(array_keys($query['where_in'])[0], array_values($query['where_in'])[0]);
    }

    if (array_key_exists('where_not_in', $query)) {
        $table->whereNotIn(array_keys($query['where_not_in'])[0], array_values($query['where_not_in'])[0]);
    }

    if (array_key_exists('join', $query)) {
        foreach ($query['join'] as $jk => $jv) {
            $jd = explode(' ', $jv);
            $table->leftJoin($jk, $jd[0], $jd[1], $jd[2]);
        }
    }

    return $table->count();
}

function dt_count_filtered($table_name, $query = [])
{
    $table = dt_query($table_name, $query);
    return $table->count();
}

function dja_row($col, $list)
{
    $data = [];

    foreach ($list as $ls) {
        $row = [];

        foreach ($col as $key => $val) {
            if (!in_array('no-table', $val)) {
                if (array_key_exists('display', $val)) {
                    $attr = '';
                    if (array_key_exists('attr', $val['display'])) {
                        if (is_array($val['display']['attr'])) {
                            foreach ($val['display']['attr'] as $d) {
                                $attr .= $d . " ";
                            }
                        } else {
                            $attr = $val['display']['attr'] . " ";
                        }
                    }

                    $link = '';
                    if (array_key_exists('id', $val['display'])) {
                        if (is_array($val['display']['id'])) {
                            $last = end($val['display']['id']);
                            foreach ($val['display']['id'] as $d) {
                                if (in_array('hashids', $val['display'])) {
                                    $link .= hashids_encode($ls->$d);
                                } else {
                                    $link .= $ls->$d;
                                }

                                if ($last != $d) {
                                    $link .= "/";
                                }
                            }
                        } else {
                            $id_data = $val['display']['id'];
                            if (in_array('hashids', $val['display'])) {
                                $link .= hashids_encode($ls->$id_data);
                            } else {
                                $link .= $ls->$id_data;
                            }
                        }
                    }

                    if (array_key_exists('end_link', $val['display'])) {
                        if (is_array($val['display']['end_link'])) {
                            foreach ($val['display']['end_link'] as $el) {
                                $link .= "/";
                                $link .= $el;
                            }
                        } else {
                            $link .= "/";
                            $link .= $val['display']['end_link'];
                        }
                    }

                    if (in_array("my-modal", $val['display'])) {
                        $attr .= "class='but-modal'";
                    }

                    if (array_key_exists('type', $val['display'])) {

                        // #LINK
                        if ($val['display']['type'] == 'link') {
                            if (in_array("specialchars", $val)) {
                                $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr>" . $ls->$key . "</a>";
                            } else {
                                $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr>" . htmlspecialchars($ls->$key) . "</a>";
                            }
                        } elseif ($val['display']['type'] == 'link_image') {
                            $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr class='d-flex flex-wrap align-items-center'><img height='35px' class='m-r-10 m-t-5 m-b-5' src='" . $ls->$key . "'>" . $ls->$key . "</a>";
                            // #LINK OPTION
                        } elseif ($val['display']['type'] == 'link_option') {
                            // if ($ls->$key == 0) {
                                // $row[] = '';
                            // } else {
                                $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr>" . $val['data'][$ls->$key] . "</a>";
                            // }
                        } elseif ($val['display']['type'] == 'link_date') {
                            if ($ls->$key == '0000-00-00') {
                                $row[] = '';
                            } else {
                                $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr>" .  tgl_indo($ls->$key) . "</a>";
                            }
                        // #OPTION
                        }
                        elseif ($val['display']['type'] == 'option') {
                            // if ($ls->$key == 0) {
                                // $row[] = '';
                            // } else {
                                $row[] = $val['data'][$ls->$key];
                            // }

                            // #DATE
                        } elseif ($val['display']['type'] == 'date') {
                            if ($ls->$key != '0000-00-00') {
                                $row[] = tgl_indo($ls->$key);
                            } else {
                                $row[] = '';
                            }
                            // #CURRENCY
                        } elseif ($val['display']['type'] == 'currency') {
                            $row[] = number_format($ls->$key, 0, '.', '.');

                            // CLOCK
                        } elseif ($val['display']['type'] == 'jam') {
                            $row[] = substr($ls->$key, 0, -3);

                            // COLOR
                        } elseif ($val['display']['type'] == 'color') {
                            $row[] = "<div style='background-color: " . $val['display']['color'] . "; padding: 10px; color: #000;'>" . htmlspecialchars($ls->$key) . "</div>";

                            // RUPIAH
                        } elseif ($val['display']['type'] == 'rupiah') {
                            $row[] = nominal_rupiah($ls->$key);
                        } elseif ($val['display']['type'] == 'link_rupiah') {
                            $row[] = "<a href='" . url($val['display']['link'] . $link) . "' $attr>" .  nominal_rupiah($ls->$key) . "</a>";
                        }
                    }
                } else {
                    if ($val['type'] == 'option') {
                        $row[] = $val['data'][$ls->$key];
                    } elseif (isset($val['sub-type']) && $val['sub-type'] == 'date') {
                        if ($ls->$key != '0000-00-00') {
                            $row[] = tgl_indo($ls->$key) . 'xx';
                        } else {
                            $row[] = '';
                        }
                    } elseif ($val['type'] == 'like') {
                        if (in_array("specialchars", $val)) {
                            $row[] = $ls->$key;
                        } else {
                            $row[] = htmlspecialchars($ls->$key);
                        }
                    } else {
                        $row[] = htmlspecialchars($ls->$key);
                    }
                }
            }
        }
        $data[] = $row;
    }

    return $data;
}
