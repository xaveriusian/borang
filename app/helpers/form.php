<?php

function dja_form_input($data)
{
    /**
     * name
     * ? type
     * ? value
     * ? label
     * ? hint
     * ? group_attr
     * ? input_attr
     * ? group_class
     * ? input_class
     */

    $output       = '';

    $_group_attr  = '';
    $_input_attr  = '';
    $_group_class = '';
    $_input_class = '';

    $_value = '';
    $_type  = 'text';

    if (array_key_exists("value", $data)) {
        $_value = $data["value"];
    }

    if (array_key_exists("type", $data)) {
        $_type = $data["type"];
    }

    if (array_key_exists("group_attr", $data)) {
        foreach ($data['group_attr'] as $key => $val) {
            $_group_attr .= $key . '=' . '"' . $val . '"';
        }
    }

    if (array_key_exists("input_attr", $data)) {
        foreach ($data["input_attr"] as $key => $val) {
            $_input_attr .= $key . '=' . '"' . $val . '"';
        }

        if (array_key_exists("required", $data['input_attr'])) {
            $_group_class .= ' required';
            $_input_class .= ' required';
        }
    }

    if (array_key_exists("group_class", $data)) {
        $_group_class .= ' ' . $data["group_class"];
    }

    if (array_key_exists("input_class", $data)) {
        $_input_class .= ' ' . $data["input_class"];
    }

    $output .= '<div class="form-group form-group-default' . $_group_class . '" ' . $_group_attr . '>';

    if (array_key_exists("label", $data)) {
        $output .= '<label for="' . $data['name'] . '" class="control-label">' . $data["label"] . '</label>';
    }

    if ($data['type'] == "password") {
        $output .= '<input type="' . $_type . '" id="' . $data["name"] . '" name="' . $data["name"] . '" class="form-control' . $_input_class . '" ' . $_input_attr . ' value="">';
    } elseif ($data['type'] == 'textarea') {
        $output .= "<textarea id='" . $data['name'] . "' name='" . $data['name'] . "' class='form-control $_input_class' $_input_attr>$_value</textarea>";
    } else {
        $output .= '<input type="' . $_type . '" id="' . $data["name"] . '" name="' . $data["name"] . '" class="form-control' . $_input_class . '" ' . $_input_attr . ' value="' . $_value . '">';
    }

    if (array_key_exists("hint", $data)) {
        $output .= "<small class='form-text text-muted'>" . $data["hint"] . "</small>";
    }

    $output .= '</div>';

    return $output;
}

function dja_form_select($data)
{
    /**
     * name
     * options
     * ? value
     * ? label
     * ? hint
     * ? first
     * ? group_attr
     * ? input_attr
     * ? group_class
     * ? input_class
     */

    $output       = '';

    $_value       = '';
    $_group_attr  = '';
    $_input_attr  = '';
    $_group_class = '';
    $_input_class = '';
    $_style       = '';

    if (array_key_exists("value", $data)) {
        $_value = $data["value"];
    }

    if (array_key_exists("group_attr", $data)) {
        foreach ($data['group_attr'] as $key => $val) {
            $_group_attr .= $key . '=' . '"' . $val . '"';
        }
    }

    if (array_key_exists("input_attr", $data)) {
        foreach ($data["input_attr"] as $key => $val) {
            $_input_attr .= $key . '=' . '"' . $val . '"';
        }

        if (array_key_exists("required", $data['input_attr'])) {
            $_group_class .= ' required';
            $_input_class .= ' required';
        }

        if (array_key_exists("multiple", $data['input_attr'])) {
            $_style = 'style="height:auto;"';
        }
    }

    if (array_key_exists("group_class", $data)) {
        $_group_class .= ' ' . $data["group_class"];
    }

    if (array_key_exists("input_class", $data)) {
        $_input_class .= ' ' . $data["input_class"];
    }

    $output .= "<div class='form-group form-group-default $_group_class' $_group_attr>";

    if (array_key_exists("label", $data)) {
        $output .= '<label for="' . $data['name'] . '" class="control-label">' . $data["label"] . '</label>';
    }

    $output .= "<select name='" . $data['name'] . "' id='" . $data['name'] . "' class='form-control full-width dja-select $_input_class' $_style $_input_attr>";

    if (array_key_exists("first", $data)) {
        foreach ($data['first'] as $key => $val) {
            $_selected = '';
            if ($_value != '' && $_value == $key) {
                $_selected = ' selected';
            }

            $output .= '<option value="' . $key . '"' . $_selected . '>' . $val . '</option>';
        }
    }

    foreach ($data['options'] as $key => $val) {
        $_selected = '';
        if ($_value != '') {
            if ($_value == $key) {
                $_selected = ' selected';
            }
        }

        $output .= '<option value="' . $key . '"' . $_selected . '>' . $val . '</option>';
    }

    $output .= "</select>";

    if (array_key_exists("hint", $data)) {
        $output .= "<small class='form-text text-muted'>" . $data["hint"] . "</small>";
    }

    $output .= '</div>';

    return $output;
}

function dja_input($data)
{
    /**
     * name
     * ? type
     * ? value
     * ? attr
     * ? class
     */

    $_name  = '';
    $_type  = 'text';
    $_value = '';
    $_class = '';
    $_attr  = '';

    if (array_key_exists("name", $data)) {
        $_name = $data['name'];
    }
    if (array_key_exists("type", $data)) {
        $_type = $data['type'];
    }
    if (array_key_exists("value", $data)) {
        $_value = $data['value'];
    }
    if (array_key_exists("class", $data)) {
        $_class = $data['class'];
    }

    if (array_key_exists("attr", $data)) {
        foreach ($data["attr"] as $key => $val) {
            $_attr .= $key . '=' . '"' . $val . '"';
        }
    }

    $output = "<input name='$_name' type='$_type' class='$_class' value='$_value' $_attr>";
    return $output;
}

function dja_select($data)
{
    /**
     * name
     * options
     * ? value
     * ? attr
     * ? class
     * ? first
     */

    $_value = '';
    $_class = '';
    $_attr  = '';

    if (array_key_exists("value", $data)) {
        $_value = $data['value'];
    }

    if (array_key_exists("class", $data)) {
        $_class = $data['class'];
    }

    if (array_key_exists("attr", $data)) {
        foreach ($data["attr"] as $key => $val) {
            $_attr .= $key . '=' . '"' . $val . '"';
        }

        if (array_key_exists("required", $data['attr'])) {
            $_class .= ' required';
        }
    }

    $output = "<select id='" . $data['name'] . "' name='" . $data['name'] . "' class='$_class' $_attr>";

    if (array_key_exists("first", $data)) {
        foreach ($data['first'] as $key => $val) {
            $_selected = '';
            if ($_value != '') {
                if ($_value == $key) {
                    $_selected = ' selected';
                }
            }

            $output .= '<option value="' . $key . '"' . $_selected . '>' . $val . '</option>';
        }
    }

    foreach ($data['options'] as $opt_key => $val) {
        $_selected = '';
        if ($_value != '') {
            if ($_value == $opt_key) {
                $_selected = ' selected';
            }
        }

        $output .= '<option value="' . $opt_key . '"' . $_selected . '>' . $val . '</option>';
    }
    $output .= '</select>';

    return $output;
}

function dja_form($col, $data = '')
{
    $output = '';

    foreach ($col as $key => $value) {
        $input = [];
        $input['name']  = $key;
        $input['value'] = '';
        $input['input_class'] = '';
        $input['group_class'] = '';
        $input['input_attr']  = [];

        if ($data != '') {
            $input['value'] = $data->$key;
        }

        if (in_array('required', $value)) {
            $input['input_class'] .= "$key ";
            $input['input_attr']['required'] = "required";
        }

        if (in_array('form-hidden', $value)) {
            $input['type'] = "hidden";
            $output .= dja_input($input);
        }

        if (!in_array('no-form', $value)) {
            if ($value['type'] == 'like') {
                if ($data != '') {
                    $input['value'] = htmlspecialchars($data->$key);
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }

                if (isset($value['sub-type']) && $value['sub-type'] == 'year') {
                    $input['input_class'] .= ' yearp';
                }

                $input['type'] = "text";

                if (isset($value['sub-type']) && $value['sub-type'] == 'textarea') {
                    $output .= dja_form_textarea($input);
                } elseif (isset($value['sub-type']) && $value['sub-type'] == 'lfm') {
                    $output .= dja_form_lfm($input);
                } else {
                    $output .= dja_form_input($input);
                }
            } elseif ($value['type'] == 'password') {
                $input['value'] = '';

                if ($data != '') {
                    $input['input_class'] .= "$key ";
                    $input['group_class'] .= '';
                    $input['hint'] = "Kosongkan jika tidak ingin mengubah";
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }
                $input['type'] = "password";

                $output .= dja_form_input($input);
            } elseif ($value['type'] == 'from') {
                if ($data != '') {
                    if (array_key_exists('sub-type', $value)) {
                        if ($value['sub-type'] == 'date') {
                            $input['value'] = tgl_indo($data->$key, 'angka');
                        } elseif ($value['sub-type'] == 'datetime') {
                            $input['value'] = tgl_indo($data->$key, 'angka');
                        } else {
                            $input['value'] = $data->$key;
                        }
                    } else {
                        $input['value'] = $data->$key;
                    }
                } else {
                    $input['value'] = '';
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }

                if (array_key_exists('sub-type', $value)) {
                    if ($value['sub-type'] == 'date') {
                        $input['input_class'] .= ' datep';
                    } elseif ($value['sub-type'] == 'year') {
                        $input['input_class'] .= ' yearp';
                    } elseif ($value['sub-type'] == 'datetime') {
                        $input['input_class'] .= ' datetimep';
                    } else {
                        $input['input_class'] .= ' angka';

                        if (array_key_exists('min_value', $value)) {
                            $input['input_attr']['min'] = $value['min_value'];
                        }

                        if (array_key_exists('max_value', $value)) {
                            $input['input_attr']['max'] = $value['max_value'];
                        }
                    }
                } else {
                    $input['input_class'] .= ' angka';

                    if (array_key_exists('min_value', $value)) {
                        $input['input_attr']['min'] = $value['min_value'];
                    }

                    if (array_key_exists('max_value', $value)) {
                        $input['input_attr']['max'] = $value['max_value'];
                    }
                }
                $input['type'] = "text";

                $output .= dja_form_input($input);
            } elseif ($value['type'] == 'date') {
                if ($data != '') {
                    $input['value'] = tgl_indo($data->$key, 'angka');
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }
                $input['input_class'] .= ' datep';
                $input['type']   = "text";

                $output .= dja_form_input($input);
            } elseif ($value['type'] == 'option') {
                if ($data != '') {
                    $input['value'] = $data->$key;
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }

                $input['options'] = $value['data'];

                if (in_array('select2', $value)) {
                    // $input['input_class'] .= ' select2';
                    $input['input_attr']['data-init-plugin'] = 'select2';
                }

                $output .= dja_form_select($input);
            } elseif ($value['type'] == 'file') {
                if ($data != '') {
                    $input['value'] = htmlspecialchars($data->$key);
                    $input['hint']  = "Kosongkan jika tidak ingin mengubah";
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }

                if (array_key_exists("accept", $value)) {
                    $input['input_attr']['accept'] = $value['accept'];
                }

                $input['type'] = "file";

                $output .= dja_form_input($input);
            } elseif ($value['type'] == 'textarea') {
                if ($data != '') {
                    $input['value'] = $data->$key;
                }

                if (array_key_exists('title', $value)) {
                    $input['label'] = _l($value['title']);
                } else {
                    $input['label'] = _l(get_judul($key));
                }
                $input['type']   = "textarea";

                $output .= dja_form_input($input);
            }
        }
    }

    return $output;
}

function dja_form_textarea($data = [])
{
    /**
     * name
     * ? type
     * ? value
     * ? label
     * ? hint
     * ? group_attr
     * ? input_attr
     * ? group_class
     * ? input_class
     */

    $output       = '';

    $_group_attr  = '';
    $_input_attr  = '';
    $_group_class = '';
    $_input_class = '';

    $_value = '';
    $_style = 'style="resize:vertical; height: auto;"';

    if (array_key_exists("value", $data)) {
        $_value = $data["value"];
    }

    if (array_key_exists("group_attr", $data)) {
        foreach ($data['group_attr'] as $key => $val) {
            $_group_attr .= $key . '=' . '"' . $val . '"';
        }
    }

    if (array_key_exists("input_attr", $data)) {
        foreach ($data["input_attr"] as $key => $val) {
            $_input_attr .= $key . '=' . '"' . $val . '"';
        }

        if (array_key_exists("required", $data['input_attr'])) {
            $_group_class .= ' required';
            $_input_class .= ' required';
        }
    }

    if (array_key_exists("group_class", $data)) {
        $_group_class .= ' ' . $data["group_class"];
    }

    if (array_key_exists("input_class", $data)) {
        $_input_class .= ' ' . $data["input_class"];
    }

    $output .= '<div class="form-group form-group-default' . $_group_class . '" ' . $_group_attr . '>';

    if (array_key_exists("label", $data)) {
        $output .= '<label for="' . $data['name'] . '" class="control-label">' . $data["label"] . '</label>';
    }

    $output .= '<textarea rows="3" id="' . $data["name"] . '" name="' . $data["name"] . '" class="form-control' . $_input_class . '" ' . $_input_attr . ' ' . $_style . '>' . $_value . '</textarea>';

    if (array_key_exists("hint", $data)) {
        $output .= "<small class='form-text text-muted'>" . $data["hint"] . "</small>";
    }

    $output .= '</div>';

    return $output;
}

function dja_form_switchery($data = [])
{
    /**
     * name
     * label
     * ? value
     * ? checked
     * ? size
     * ? color
     */

    $_name    = isset($data['name']) ? $data['name'] : "";
    $_label   = isset($data['label']) ? $data['label'] : "";
    $_value   = isset($data['value']) ? $data['value'] : "true";
    $_checked = isset($data['checked']) && $data['checked'] == true ? "checked" : "";
    $_size    = isset($data['size']) ? $data['size'] : "small";
    $_color   = isset($data['color']) ? $data['color'] : 'success';

    $output = "<div class='form-group form-group-default p-b-10 p-t-10'>";
    $output .= "<label for='$_name'><input id='$_name' type='checkbox' name='$_name' data-init-plugin='switchery' data-size='$_size' data-color='$_color' value='$_value' $_checked/><span class='cursor m-l-10'>$_label</span></label>";
    if (array_key_exists("hint", $data)) {
        $output .= "<small class='form-text text-muted'>" . $data["hint"] . "</small>";
    }
    $output .= "</div>";

    return $output;
}

function dja_switchery($data = [])
{
    /**
     * name
     * label
     * ? value
     * ? checked
     * ? size
     * ? color
     */

    $_name    = isset($data['name']) ? $data['name'] : "";
    $_label   = isset($data['label']) ? $data['label'] : "";
    $_value   = isset($data['value']) ? $data['value'] : "true";
    $_checked = isset($data['checked']) && $data['checked'] == true ? "checked" : "";
    $_size    = isset($data['size']) ? $data['size'] : "small";
    $_color   = isset($data['color']) ? $data['color'] : 'success';

    $output = "<label for='$_name'><input id='$_name' type='checkbox' name='$_name' data-init-plugin='switchery' data-size='$_size' data-color='$_color' value='$_value' $_checked/><span class='cursor m-l-10'>$_label</span></label>";

    return $output;
}

function dja_role($data)
{
    $c_view = '';
    $c_add = '';
    $c_edit = '';
    $c_del = '';
    $c_detail = '';

    if ($data['role_permission']) {
        foreach ($data['role_permission'] as $p) {
            if ($data['name'] == $p->permission) {
                if ($p->view_rp == 1) $c_view = 'checked';
                if ($p->add_rp == 1) $c_add = 'checked';
                if ($p->edit_rp == 1) $c_edit = 'checked';
                if ($p->delete_rp == 1) $c_del = 'checked';
                if ($p->detail_rp == 1) $c_detail = 'checked';
            }
        }
    }

    $class = 'p-l-20';
    if (array_key_exists('class', $data)) {
        $class = $data['class'];
    }

    $output = "<tr>";
    $output .= "<td class='$class'>$data[title]</td>";
    $output .= "<td><input type='checkbox'  name='pmission[$data[name]-view]' data-init-plugin='switchery' data-size='small' data-color='success' $c_view /></td>";
    $output .= "<td><input type='checkbox'  name='pmission[$data[name]-add]' data-init-plugin='switchery' data-size='small' data-color='success' $c_add /></td>";
    $output .= "<td><input type='checkbox'  name='pmission[$data[name]-edit]' data-init-plugin='switchery' data-size='small' data-color='success' $c_edit /></td>";
    $output .= "<td><input type='checkbox'  name='pmission[$data[name]-delete]' data-init-plugin='switchery' data-size='small' data-color='success' $c_del /></td>";
    $output .= "<td><input type='checkbox'  name='pmission[$data[name]-detail]' data-init-plugin='switchery' data-size='small' data-color='success' $c_detail /></td>";
    $output .= "</tr>";

    return $output;
}

function dja_form_lfm($data)
{
    /**
     * name
     * label
     * ?value
     */
    $_value = $data['value'] ?? '';
    $_id = $data['id'] ?? 'lfm';

    $output = '<div class="form-group form-group-default">';
    $output .= '<label for="" class="control-label">' . _l($data['label']) . '</label>';
    $output .= '<div class="input-group">';
    $output .= '<span class="input-group-btn">';
    $output .= '<a id="' . $_id . '" data-input="thumbnail-' . $_id . '" data-preview="holder" class="btn btn-default">';
    $output .= '<i class="fas fa-picture-o m-r-10"></i>' . _l('choose');
    $output .= '</a>';
    $output .= '</span>';
    $output .= '<input id="thumbnail-' . $_id . '" class="form-control form-control-group" type="text" name="' . $data['name'] . '" readonly value="' . $_value . '">';
    $output .= '</div>';
    $output .= '</div>';

    return $output;
}
