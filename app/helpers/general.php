<?php

use App\Models\MemberSubscription;
use App\Models\Notification;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\Auth;

function _l($code)
{
    $set = Auth::user()->language;
    $code = strtolower(str_replace(" ", "_", $code));
    $single = DB::table("languages")->where(['code' => $code, 'deleted_at' => NULL])->first();

    // return $code;
    if ($single) {
        if ($set == 1) {
            return $single->bahasa;
        } else {
            return $single->english;
        }
    }

    // return "translate_not_found_" . $code;
    // return $code;
    return ucwords(str_replace("_", " ", $code));
}

function get_clean_array($array)
{
    $output = [];
    foreach ($array as $c => $k) {
        if (!is_array($k)) {
            if (cek_word($k, 'like')) {
                $nc = get_like_word($k);
                $output[] = $nc;
            } elseif (cek_word($k, 'from')) {
                $f = get_from_word($k);
                $output[] = $f[1];
            } elseif (cek_word($k, 'no_search')) {
                $nc = get_like_word($k);
                $output[] = $nc;
            }
        } else {
            $output[] = $c;
        }
    }

    $output;

    return $data[] = $output;
}

function cek_word($str, $word)
{
    $c = explode(" ", $str);
    if ($c[0] == $word) {
        return true;
    } else {
        return false;
    }
}

function get_from_word($str)
{
    $c = explode(" ", $str);
    return [$c[1], $c[2], $c[3], $c[4]];
}

function get_like_word($str)
{
    $c = explode(" ", $str);
    return $c[1];
}

function get_judul($data)
{
    $df = explode('_', $data);
    $dg = array_pop($df);
    return implode(' ', $df);
}

function mego_bulan($gt, $pj = 0)
{
    if (Auth::user()->language == 1) {
        if ($pj == 0) {
            $bln = array('01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Ags', '09' => 'Sept', '10' => 'Okt', '11' => 'Nov', '12' => 'Des');
        } else {
            $bln = array('01' => 'Januari', '02' => 'Febuari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
        }
    } else {
        if ($pj == 0) {
            $bln = array('01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'Aug', '09' => 'Sept', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
        } else {
            $bln = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        }
    }

    if (array_key_exists($gt, $bln)) {
        return $bln[$gt];
    } else {
        return "Error: mego_bulan() at first parameter";
    }
}




function tgl_dt($tgl)
{
    $cv = ['', '0000-00-00', '1970-01-01'];

    $arr_tgl = explode(" ", $tgl);

    if (!in_array($tgl, $cv)) {
        $kirim = date("Y-m-d", strtotime($tgl));
        if (isset($arr_tgl[1])) {
            $kirim .= date(" H:i:s", strtotime($tgl));
        }

        return $kirim;
    } else {
        return "";
    }
}

function tgl_indo($tgl, $bln = 'angka', $pj = 0)
{
    $cv = ['', ' ', '0000-00-00', '1970-01-01'];
    $arr_tgl = explode(" ", $tgl);

    if (!in_array($tgl, $cv)) {
        if ($bln == 'angka') {
            $kirim = date("d-m-Y", strtotime($tgl));
        } else {
            $kirim = date("d ", strtotime($tgl));
            $kirim .= mego_bulan(date("m", strtotime($tgl)), $pj);
            $kirim .= date(" Y", strtotime($tgl));
        }
        if (isset($arr_tgl[1])) {
            $kirim .= date(" H:i:s", strtotime($tgl));
        }

        return $kirim;
    } else {
        return "";
    }
}

function dja_breadcrumb($breadscrumbs)
{
    $output = '<ul class="breadcrumb">';
    foreach ($breadscrumbs as $key => $val) {
        if ($key != '') {
            $output .= "<li>
               <a href='$key'>$val</a>
               </li>";
        } else {
            $output .= "<li class='bold'>$val</li>";
        }
    }
    $output .= "</ul>";

    return $output;
}

function dja_detail($array)
{
    if (array_key_exists('col', $array)) {
        $col = $array['col'];
    } else {
        $col = [];
    }
    if (array_key_exists('data', $array)) {
        $data = $array['data'];
    } else {
        $data = '';
    }

    $output = '';
    foreach ($col as $key => $value) {
        if (!in_array('no-detail', $value)) {
            if (array_key_exists('title', $value)) {
                $title = _l($value['title']);
            } else {
                $title = _l(get_judul($key));
            }

            $_unit = '';
            if ($data->$key) {
                if (array_key_exists('unit', $value)) {
                    $_unit = $value['unit'];
                }
            }


            $_align_value = '';
            if (array_key_exists('align_value', $value)) {
                $_align_value = $value['align_value'];
            }

            $_extra = '';
            if (array_key_exists('extra', $value)) {
                foreach ($value['extra'] as $ke => $kv) {

                    $_v_extra = '';
                    $_t_extra = '';

                    if (array_key_exists('value', $value['extra'])) {
                        $_v_extra = $data->$value['extra']['value'];

                        if ($data->$value['extra']['value'] != '') {
                            if (array_key_exists('title', $value['extra'])) {
                                $_t_extra = " | " . $value['extra']['title'] . " : ";
                            }
                        }
                    }

                    $_u_extra = '';
                    if (array_key_exists('unit', $value['extra'])) {
                        $_u_extra = $value['extra']['unit'];
                    }

                    $_extra = "$_t_extra $_v_extra $_u_extra";

                    if (array_key_exists('link', $value['extra'])) {
                        $_src = $value['extra']['link']['src'] . $data->$value['extra']['link']['id'];
                        $_extra = "<a href='$_src' target='_blank'>$_t_extra $_v_extra $_u_extra</a>";
                    }
                }
            }


            if ($value['type'] == 'like') {
                if ($data != '') {
                    $data_value = htmlspecialchars($data->$key);
                } else {
                    $data_value = '';
                }

                $output .= "<tr><td>$title</td><td class='$_align_value'>$data_value $_unit $_extra</td></tr>";
            } elseif ($value['type'] == 'date') {
                if (!in_array('no-format', $value)) {
                    if ($data->$key != '') {
                        $data_value = tgl_indo($data->$key, 'xx');
                    } else {
                        $data_value = '';
                    }
                } else {
                    if ($data->$key != '') {
                        $data_value = $data->$key;
                    } else {
                        $data_value = '';
                    }
                }


                $output .= "<tr><td>$title</td><td class='$_align_value'>$data_value $_unit $_extra</td></tr>";
            } elseif ($value['type'] == 'link_date') {
                if (!in_array('no-format', $value)) {
                    if ($data->$key != '') {
                        $data_value = tgl_indo($data->$key, 'xx');
                    } else {
                        $data_value = '';
                    }
                } else {
                    if ($data->$key != '') {
                        $data_value = $data->$key;
                    } else {
                        $data_value = '';
                    }
                }

                $_src = '';
                if (array_key_exists('link', $value)) {
                    $_src = $value['link']['src'] . $data->$value['link']['id'];
                }

                $output .= "<tr><td>$title</td><td class='$_align_value'><a href='$_src' target='_blank'>$data_value</a></td></tr>";
            } elseif ($value['type'] == 'from') {
                if ($data != '') {
                    if (array_key_exists('sub-type', $value)) {
                        if ($value['sub-type'] == 'date') {
                            $data_value = tgl_indo($data->$key, 'angka');
                        } else {
                            $data_value = $data->$key;
                        }
                    } else {
                        $data_value = $data->$key;
                    }
                } else {
                    $data_value = '';
                }

                if (isset($value['display']) && isset($value['display']['type'])) {
                    if ($value['display']['type'] == 'link_rupiah') {
                        $data_value = nominal_rupiah($data_value);
                    }
                }

                $output .= "<tr><td>$title</td><td class='$_align_value'>$data_value $_unit $_extra</td></tr>";
            } elseif ($value['type'] == 'option') {
                if ($data->$key != 0) {
                    $data_value = $value['data'][$data->$key];
                } else {
                    $data_value = '';
                }

                $output .= "<tr><td>$title</td><td class='$_align_value'>$data_value $_unit $_extra</td></tr>";
            }
        }
    }
    return $output;
}

function dja_get_all_list($array)
{
    $key = $array['key'];
    $val = $array['val'];

    $data = DB::table($array['table']);

    if (array_key_exists("where", $array)) {
        $data->where($array['where']);
    }

    if (array_key_exists("whereIn", $array)) {
        $data->whereIn($array['whereIn'][0], $array['whereIn'][1]);
    }
    if (array_key_exists("whereNotIn", $array)) {
        $data->whereNotIn($array['whereNotIn'][0], $array['whereNotIn'][1]);
    }

    $output = [];
    foreach ($data->get() as $d) {
        $value = "";
        $value .= $d->$val;

        if (array_key_exists("dash", $array)) {
            $dash = $array['dash'];
            $value  .= " - " . $d->$dash;
        }

        $output[$d->$key] = $value;
    }

    return $output;
}

function dja_get_all_item_category_list()
{
    $data = DB::table("item_categories")->get()->toArray();
    $data = json_decode(json_encode($data), true);

    $output = [];
    foreach ($data as $d) {
        $name = '';

        $arrParent = [];
        $parent = $d['parent'];
        while ($parent != '') {
            $parentIndex = array_search($parent, array_column($data, 'id'));
            $dataparent = $data[$parentIndex];
            $arrParent[] = $dataparent['name'];
            $parent = $dataparent['parent'];
        }
        $arrParent = array_reverse($arrParent, true);
        foreach ($arrParent as $key => $val) {
            $name .= $val;
            $name .= " / ";
        }
        $name .= $d['name'];

        $output[$d['id']] = $name;
    }

    asort($output);

    return $output;
}


function hashids_encode($id)
{
    return Hashids::encode($id);
}

function hashids_decode($id)
{
    return Hashids::decode($id)[0];
}

function get_extension($fileName)
{
    $ext = explode('.', $fileName);
    $ext = end($ext);

    return $ext;
}

function get_extension_icon($ext)
{
    $ext = explode('.', $ext);
    $ext = end($ext);

    if ($ext == 'doc' || $ext == 'docx') {
        $file_icon = '<i class="fa fa-file-word-o text-primary"></i>';
    } elseif ($ext == 'xls' || $ext == 'xlsx') {
        $file_icon = '<i class="fa fa-file-excel-o text-success"></i>';
    } elseif ($ext == 'pdf') {
        $file_icon = '<i class="fa fa-file-pdf-o text-danger"></i>';
    } elseif ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'gif') {
        $file_icon = '<i class="fa fa-file-photo-o text-warning-dark"></i>';
    } elseif ($ext == 'ppt' || $ext == 'pptx') {
        $file_icon = '<i class="fa fa-file-powerpoint-o text-warning"></i>';
    } else {
        $file_icon = '<i class="fa fa-file-o text-black"></i>';
    }

    return $file_icon;
}

function permit($permission)
{
    $permit = false;

    $p = explode('.', $permission);

    if (Auth::user()->is_admin == 1) {
        $permit = true;
    } else {
        $role = DB::table('role_permissions')
            ->where([
                'role_id' => Auth::user()->role_id,
                'permission' => $p[0],
                $p[1] . '_rp' => 1
            ])
            ->get()
            ->count();

        if ($role) {
            $permit = true;
        }
    }

    return $permit;
}

function have_permit($permission)
{
    if (!permit($permission)) {
        echo redirect()->route('access-denied');
        die();
    }
}

function list_permit($permission)
{
    foreach ($permission as $p) {
        if (permit($p)) {
            return true;
        }
    }

    return false;
}

function nominal_rupiah($nominalRupiah, $rp = 1)
{
    $nominalRupiah = number_format($nominalRupiah, 0, ',', '.');

    if ($rp == 1) {
        $nominalRupiah = "Rp$nominalRupiah";
    }

    return $nominalRupiah;
}


function filename_remove_time($fileName)
{
    // 1598179417_filename -> filename
    $arrTime = explode('_', $fileName);

    $fileName = '';
    for ($i = 1; $i < count($arrTime); $i++) {
        $fileName .= $arrTime[$i];
    }

    return $fileName;
}

function filename_truncate($fileName, $length = 10, $cutTime = false)
{
    if ($cutTime) {
        $fileName = filename_remove_time($fileName);
    }

    if (strlen($fileName) < $length) {
        return $fileName;
    } else {
        $extension = explode('.', $fileName);
        $extension = end($extension);

        return substr($fileName, 0, $length) . '... .' . $extension;
    }
}

function seconds_to_readable($second)
{
    $output = "";

    $days    = floor($second / (3600 * 24));
    $hours   = floor(($second / 3600) % 24);
    $minutes = floor(($second / 60) % 60);
    $seconds = $second % 60;

    if ($days == 1) {
        $output .= $days . " " . _l("day");
    } elseif ($days > 1) {
        $output .= $days . " " . _l("days");
    }


    if ($days > 0 && $hours > 0) {
        $output .= " ";
    }

    if ($hours == 1) {
        $output .= $hours . " " . _l("hour");
    } elseif ($hours > 1) {
        $output .= $hours . " " . _l("hours");
    }

    if ($hours > 0 && $minutes > 0) {
        $output .= " ";
    }

    if ($minutes == 1) {
        $output .= $minutes . " " . _l("minute");
    } elseif ($minutes > 1) {
        $output .= $minutes . " " . _l("minutes");
    }

    if ($minutes > 0 && $seconds > 0) {
        $output .= " ";
    }


    if ($seconds <= 1) {
        $output .= $seconds . " " . _l("second");
    } elseif ($seconds > 1) {
        $output .= $seconds . " " . _l("seconds");
    }

    return $output;
}

function get_gender_icon($genderId, $color = true)
{
    // $this->_list_gender = [
    //     0 => "Tidak diketahui",
    //     1 => "Laki - laki",
    //     2 => "Perempuan",
    // ];

    $output = '';
    $class  = '';

    if ($genderId == 1) {
        if ($color) {
            $class = 'text-primary';
        }
        $output = "<i class='fa fa-mars $class'></i>";
    } elseif ($genderId == 2) {
        if ($color) {
            $class = 'text-danger';
        }
        $output = "<i class='fa fa-venus $class'></i>";
    } else {
        $output = "<i class='fa fa-question-circle-o'></i>";
    }

    return $output;
}

function terbilang($x)
{
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = terbilang($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = terbilang($x / 10) . " puluh" . terbilang($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . terbilang($x - 100);
    } else if ($x < 1000) {
        $temp = terbilang($x / 100) . " ratus" . terbilang($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . terbilang($x - 1000);
    } else if ($x < 1000000) {
        $temp = terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    } else if ($x < 1000000000) {
        $temp = terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = terbilang($x / 1000000000) . " milyar" . terbilang(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = terbilang($x / 1000000000000) . " trilyun" . terbilang(fmod($x, 1000000000000));
    }
    return ucfirst($temp);
}

function limit_word($str, $limit = 10)
{
    if (str_word_count($str) < $limit) {
        $output = $str;
    } else {
        $str_arr = explode(' ', $str);
        $output = '';

        for ($i = 0; $i < $limit; $i++) {
            if ($i != 0) {
                $output .= ' ';
            }
            $output .= $str_arr[$i];
        }
    }
    return $output;
}

function get_my_unread_notification()
{
    return Notification::where(['read' => 0, 'user' => 'u-' . Auth::user()->id])->get();
}

function time_ago($time_ago)
{
    $time_ago     = strtotime($time_ago);
    $cur_time     = time();
    $time_elapsed = $cur_time - $time_ago;
    $seconds      = $time_elapsed;
    $minutes      = round($time_elapsed / 60);
    $hours        = round($time_elapsed / 3600);
    $days         = round($time_elapsed / 86400);
    $weeks        = round($time_elapsed / 604800);
    $months       = round($time_elapsed / 2600640);
    $years        = round($time_elapsed / 31207680);
    // Seconds
    if ($seconds <= 60) {
        return _l("just_now");
    }
    //Minutes
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            return _l('a_minute_ago');
        } else {
            return $minutes . ' ' . _l('minutes_ago');
        }
    }
    //Hours
    else if ($hours <= 24) {
        if ($hours == 1) {
            return _l("an_hour_ago");
        } else {
            return $hours . ' ' . _l('hours ago');
        }
    }
    //Days
    else if ($days <= 7) {
        if ($days == 1) {
            return _l("yesterday");
        } else {
            return $days . ' ' . _l('days_ago');
        }
    }
    //Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            return _l('last_week');
        } else {
            return $weeks . ' ' . _l('weeks_ago');
        }
    }
    //Months
    else if ($months <= 12) {
        if ($months == 1) {
            return _l('last_month');
        } else {
            return $months . ' ' . _l('months_ago');
        }
    }
    //Years
    else {
        if ($years == 1) {
            return _l("a_year_ago");
        } elseif ($years > 1 && $years <= 5) {
            return $years . ' ' . _l('years_ago');
        } else {
            return _l("never");
        }
    }
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function check_url($url)
{
    $headers = @get_headers($url);
    return $headers;
    if ($headers && strpos($headers[0], '200') || $headers && strpos($headers[0], '302') || $headers && strpos($headers[8], '302')) {
        return true;
    }
    return false;
}

function send_api($url, $method, $data = [])
{

    $curl = curl_init();

    $url .= "?_token=" . csrf_token();
    if (count($data)) {
        $url .= '&' . http_build_query($data);
    }

    $option = [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_CUSTOMREQUEST => $method,
    ];

    curl_setopt_array($curl, $option);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
        return json_decode($response, true);
    }
}

function mego_rand($long = 5, $sm = 0)
{
    if ($sm == 0) {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    } else {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }


    $string = '';
    $max = strlen($characters) - 1;
    for ($i = 0; $i < $long; $i++) {
        $string .= $characters[mt_rand(0, $max)];
    }

    return $string;
}

function subscription_items()
{
    return [
        "Unlimited Message",
        "Auto Reply",
        "File Message",
        "Image Message",
        "Video Message",
        "Music Message",
        "Unlimited Blasting",
        "Scheduled Blasting",
        "Scheduled Message",
        "Full Access to API",
        "Webhook Up To 3 Endpoints"
    ];
}

function generate_order_number()
{
    $now = date("YmdHis");
    $last = MemberSubscription::where('order_number', 'LIKE', "%$now%")->get();
    if (count($last)) {
        $now .= count($last) + 1;
    } else {
        $now .= 1;
    }

    return "SCP-$now";
}
