<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\RolePermission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "role",
            "title" => "role",
            "icon"  => "fas fa-user-shield",
        ];

        $this->_table = 'roles';
        $this->_query = [];

        $this->_col = [
            'name' => [
                'type'  => 'like',
                'title' => "name",
                'display' => [
                    'type' => 'link',
                    'link' => 'role/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }
    public function index()
    {
        have_permit('role.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('role.table', $data);
    }

    public function create()
    {
        have_permit('role.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['form'] = dja_form($this->_col);
        $data['rolep'] = '';

        return view('role.form', $data);
    }

    public function store(Request $request)
    {
        have_permit('role.add');

        $request->validate([
            "name" => 'required|unique:roles,name',
        ]);

        $role = new Role;
        $role->name = $request->name;
        $role->guard_name = "web";
        $role->save();

        if ($request->pmission) {
            foreach ($request->pmission as $key => $value) {
                $field = array_filter(explode('-', $key));

                $cek = RolePermission::where(['role_id' => $role->id, 'permission' => $field[0]])->first();

                if ($cek) {
                    $simpan = [];
                    $simpan[$field[1] . "_rp"] = 1;
                    RolePermission::where(['role_id' => $role->id, 'permission' => $field[0]])->update($simpan);
                } else {
                    $f = $field[1] . "_rp";
                    $new = new RolePermission();
                    $new->permission = $field[0];
                    $new->role_id = $role->id;
                    $new->$f = 1;

                    $new->save();
                }
            }
        }

        return redirect(route("role.index"))->with("alert-success", _l("data_saved_successfully"));
    }

    public function edit($id)
    {
        have_permit('role.edit');

        $value = Role::find($id);
        $rolep = RolePermission::where(['role_id' => $id])->get();

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['form']  = dja_form($this->_col, $value);
        $data['data']  = $value;
        $data['rolep'] = $rolep;

        return view('role.form', $data);
    }

    public function update($id, Request $request)
    {
        have_permit('role.edit');

        $request->validate([
            "name" => "required|unique:roles,name,{$id}",
        ]);

        RolePermission::where(['role_id' => $id])->delete();

        if ($request->pmission) {
            foreach ($request->pmission as $key => $value) {
                $field = array_filter(explode('-', $key));

                $cek = RolePermission::where(['role_id' => $id, 'permission' => $field[0]])->first();

                if ($cek) {
                    $simpan = [];
                    $simpan[$field[1] . "_rp"] = 1;
                    RolePermission::where(['role_id' => $id, 'permission' => $field[0]])->update($simpan);
                } else {
                    $f = $field[1] . "_rp";
                    $new = new RolePermission();
                    $new->permission = $field[0];
                    $new->role_id = $id;
                    $new->$f = 1;

                    $new->save();
                }
            }
        }

        return redirect(route("role.index"))->with("alert-success", _l("data_successfully_changed"));
    }
}
