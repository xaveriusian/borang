<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "document",
            "title" => "Semua Data",
            "icon"  => "fas fa-folder-open",
        ];

        $this->_table = 'documents';
        $this->_list_categories = [
            "sermon" => "Penetapan",
            "article" => "Pelaksanaan",
            "bulletin" => "Evaluasi",
            "pengendalian" => "Pengendalian",
            "peningkatan" => "Peningaktan",
            "meeting_result" => "Lainnya",
            "document" => "Semua Data",
            
        ];
        $this->_list_users = dja_get_all_list(["table" => "users", "key" => "id", "val" => "name"]);
        $this->_col = [
            'category' => [
                'type'  => 'option',
                'title' => "Category",
                'data' => $this->_list_categories,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'name' => [
                'type'  => 'like',
                'title' => "Name",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('document.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('document.table', $data);
    }

    public function show($id)
    {
        have_permit('document.detail');

        $value = Document::find($id);

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_categories'] = $this->_list_categories;

        $data['data']   = $value;

        $extension = explode(".", $value->link);
        $extension = end($extension);
        $data['extension'] = $extension;
        $data['link'] = $value->link;

        return view('document.detail', $data);
    }
}
