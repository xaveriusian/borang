<?php

namespace App\Http\Controllers;

use App\Models\MuiTitle;
use Illuminate\Http\Request;

class MuiTitleController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "mui_title",
            "title" => "Jabatan MUI",
            "icon"  => "fas fa-user-cog",
        ];

        $this->_table = 'mui_titles';
        $this->_query = [];

        $this->_col = [
            'name' => [
                'type'  => 'like',
                'title' => "name",
                'display' => [
                    'type' => 'link',
                    'link' => 'mui-title/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('mui_title.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = $this->_var["title"];
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');
        $data['form'] = dja_form($this->_col);

        return view('mui-title.table', $data);
    }

    public function store(Request $request)
    {
        have_permit('mui_title.add');

        $request->validate([
            "name"    => 'required|unique:mui_titles,name',
        ]);

        $target = new MuiTitle;

        $target->name = $request->name;
        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();
        return $target;
    }

    public function edit($id)
    {
        have_permit('mui_title.edit');

        $data = MuiTitle::find($id);
        $data->nid = (string) $id;
        return $data;
    }

    public function update(Request $request, $id)
    {
        have_permit('mui_title.edit');

        $request->validate([
            "name" => "required|unique:mui_titles,name,{$id}",
        ]);

        $target = MuiTitle::findOrFail($id);

        $target->name = $request->name;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return $target;
    }

    public function destroy($id)
    {
        have_permit('mui_title.delete');

        $target = MuiTitle::findOrFail($id);
        $target->deleted_by = auth()->user()->id;
        $target->save();

        return $target->delete();
    }
}
