<?php

namespace App\Http\Controllers;

use App\Models\Pengendalian;
use App\Models\Document;
use Illuminate\Http\Request;

class PengendalianController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "pengendalian",
            "title" => "Pengendalian",
            "icon"  => "fas fa-newspaper",
        ];

        $this->_table = 'pengendalian';
        $this->_list_users = dja_get_all_list(["table" => "users", "key" => "id", "val" => "name"]);
        $this->_list_content_types = [
            "file" => "File",
            "link" => "Link",
            "papers" => "Karya Tulis",
        ];

        $this->_col = [
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('pengendalian.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('pengendalian.table', $data);
    }


    public function create()
    {
        have_permit('pengendalian.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        return view('pengendalian.form', $data);
    }

    public function store(Request $request)
    {
        have_permit('pengendalian.add');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = new Pengendalian;
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file") {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = new Document;
            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "pengendalian";
            $doc->relation_id = $target->id;
            $doc->created_by = auth()->user()->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();

        return redirect(route("pengendalian.index"))->with("alert-success", _l("data_saved_successfully"));
    }

    public function show($id)
    {
        have_permit('pengendalian.detail');

        $value = Pengendalian::find($id);

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        $data['data']   = $value;

        if ($value->content_type == "file") {
            $data['document'] = Document::where(['category' => "pengendalian", "relation_id" => $value->id])->first();
        }

        return view('pengendalian.detail', $data);
    }

    public function edit($id)
    {
        have_permit('pengendalian.edit');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['data'] = Pengendalian::findOrFail($id);

        $data['list_content_types'] = $this->_list_content_types;

        return view('pengendalian.form', $data);
    }

    public function update($id, Request $request)
    {
        have_permit('pengendalian.edit');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = Pengendalian::findOrFail($id);
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file" && $request->file) {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = Document::where(['relation_id' => $id, 'category' => 'pengendalian'])->first();

            if (!$doc) {
                $doc = new Document;
                $doc->created_by = auth()->user()->id;
            }

            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "pengendalian";
            $doc->relation_id = $target->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();
        return redirect(route("pengendalian.show", $target->id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function destroy($id)
    {
        have_permit('pengendalian.edit');

        $value = Pengendalian::findOrFail($id);
        $value->deleted_by = auth()->user()->id;
        $value->save();

        $value->delete($id);

        return redirect()->route('pengendalian.index')->with("alert-success",  _l("data_successfully_deleted"));
    }
}
