<?php

namespace App\Http\Controllers;

use App\Models\UserAddress;
use App\Models\UserEducation;
use App\Models\UserExperience;
use App\Models\UserOrganizationExperience;
use App\Models\UserWorkExperience;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "user",
            "title" => "user",
            "icon"  => "fas fa-user-tie",
        ];

        $this->_table = 'users';
        $this->_query = [];

        $this->_list_gender = [
            0 => "Tidak diketahui",
            1 => "Laki - laki",
            2 => "Perempuan",
        ];

        $this->_list_language = [
            1 => "Indonesia",
            2 => "Inggris"
        ];

        $this->_list_religion = [
            0 => 'Tidak diketahui',
            1 => 'Islam',
            2 => 'Kristen',
            3 => 'Katolik',
            4 => 'Hindu',
            5 => 'Buddha',
            6 => 'Kong hu cu',
            7 => 'Kristen advent',
        ];

        $this->_list_roles = dja_get_all_list(["table" => "roles", "key" => "id", "val" => "name"]);
        $this->_list_mui_titles = dja_get_all_list(["table" => "mui_titles", "key" => "id", "val" => "name"]);
        $this->_list_provinces = dja_get_all_list(["table" => "provinces", "key" => "id", "val" => "name"]);
        $this->_list_organizations = dja_get_all_list(["table" => "organizations", "key" => "id", "val" => "name"]);
        $this->_list_organizations = array_merge(["general" => "Umum"], $this->_list_organizations);
        $this->_list_is_biodata_done = [
            0 => "Need to be filled",
            1 => "Done",
        ];

        $this->_col = [
            'name' => [
                'type'  => 'like',
                'title' => "Nama Lengkap",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            
            'email' => [
                'type'  => 'like',
                'title' => "Email",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            
            'role_id' => [
                'type'  => 'option',
                'title' => 'Peran',
                'data' => $this->_list_roles,
                'select2',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            
        ];

        $this->_col_address = [
            'province' => [
                'type'  => 'like',
                'title' => "Province",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            'regency' => [
                'type'  => 'like',
                'title' => "Regency",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            'district' => [
                'type'  => 'like',
                'title' => "District",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            'village' => [
                'type'  => 'like',
                'title' => "Village",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            'postal_code' => [
                'type'  => 'like',
                'title' => "Postal Code",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
            'long_address' => [
                'type'  => 'like',
                'title' => "Address",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'user/',
                    'id'   => 'id'
                ],
            ],
        ];

        $this->_col_education = [
            'education_name' => [
                'type'  => 'like',
                'title' => "Name",
            ],
            'start_date' => [
                'type'  => 'from',
                'sub-type' => "datetime",
                'title' => "From",
            ],
            'end_date' => [
                'type'  => 'from',
                'sub-type' => "datetime",
                'title' => "To",
            ],
        ];

        $this->_col_organization_experience = [
            'date' => [
                'type'  => 'from',
                'sub-type' => "date",
                'title' => "Date",
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
            ],
            'experience_description' => [
                'type'  => 'like',
                'title' => "Description",
            ],
        ];


        $this->_col_work_experience = [
            'company' => [
                'type'  => 'like',
                'title' => "Company",
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
            ],
            'start_date' => [
                'type'  => 'from',
                'sub-type' => "date",
                'title' => "From",
            ],
            'end_date' => [
                'type'  => 'from',
                'sub-type' => "date",
                'title' => "To",
            ],
        ];

        $this->_col_experience = [

            'date' => [
                'type'  => 'from',
                'sub-type' => "date",
                'title' => "Date",
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
            ],
            'role' => [
                'type'  => 'like',
                'title' => "Peran",
            ],
            'location' => [
                'type'  => 'like',
                'title' => "Lokasi",
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('user.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('user.table', $data);
    }

    public function create()
    {
        have_permit('user.add');

        if (Auth::user()->language == 1) {
            $active_hint   = "Jika anda tidak mengaktifkannya, pengguna ini tidak akan bisa login";
            $change_hint   = "Kosongkan jika anda tidak ingin mengubahnya";
            $admin_hint    = "Jika anda mengaktifkannya, pengguna ini akan mempunyai hak akses tanpa batas";
            $password_hint = "Kosongkan jika anda ingin menggunakan kata sandi default (demo1234)";
        } else {
            $active_hint   = "If you dont activate it, this user will not be able to login";
            $change_hint   = "Leave it blank if you don't want to change it";
            $admin_hint    = "if you activate it, this user will have unlimited permission";
            $password_hint = "Leave it blank if you want to use default password (demo1234)";
        }

        $data['active_hint']   = $active_hint;
        $data['change_hint']   = $change_hint;
        $data['admin_hint']    = $admin_hint;
        $data['password_hint'] = $password_hint;

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_gender'] = $this->_list_gender;
        $data['list_religion'] = $this->_list_religion;
        $data['list_role'] = $this->_list_roles;
        $data['list_organization'] = $this->_list_organizations;
        $data['list_mui_titles'] = $this->_list_mui_titles;
        $data['list_provinces'] = $this->_list_provinces;

        return view('user.form', $data);
    }

    public function store(Request $request)
    {
        have_permit('user.add');

        $request->validate([
            "email"    => 'required|unique:users,email',
            "picture"  => "mimes:png,jpg,jpeg",
        ]);

        $new = new User();

        $new->password = $request->password  ? Hash::make($request->password) : Hash::make("demo1234");
        $new->is_active = isset($request->is_active) ? $request->is_active : 0;
        $new->is_admin = isset($request->is_admin)  ? $request->is_admin : 0;
        $new->role_id = $request->role_id;
        $new->email = $request->email;

        $new->serial_number = $request->serial_number;
        $new->member_number = $request->member_number;
        $new->name = $request->name;
        $new->gender = $request->gender;
        $new->last_education = $request->last_education;
        $new->name_according_to_nik = $request->name_according_to_nik;
        $new->nik_number = $request->nik_number;
        $new->birthplace = $request->birthplace;
        $new->birthdate = tgl_dt($request->birthdate);
        $new->organization_id = $request->organization_id;
        $new->phone_number = $request->phone_number;
        $new->phone_number_2 = $request->phone_number_2;
        $new->mui_title_id = $request->mui_title_id;
        $new->field = $request->field;
        $new->province_as_ktp = $request->province_as_ktp;
        $new->regency_as_ktp = $request->regency_as_ktp;
        $new->district_as_ktp = $request->district_as_ktp;
        $new->village_as_ktp = $request->village_as_ktp;
        $new->rt_as_ktp = $request->rt_as_ktp;
        $new->rw_as_ktp = $request->rw_as_ktp;
        $new->province_working_area = $request->province_working_area;
        $new->regency_working_area = $request->regency_working_area;
        $new->district_working_area = $request->district_working_area;
        $new->residence_address = $request->residence_address;

        if ($request->picture) {
            $file_name = time() . '_' . $request->picture->getClientOriginalName();
            $request->picture->move(public_path("uploads/users"), $file_name);
            $new->picture = $file_name;
        }

        $new->save();

        return redirect(route("user.index"))->with("alert-success", _l("data_saved_successfully"));
    }

    public function show($id)
    {
        have_permit('user.detail');

        $value = User::find($id);

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];


        $data['list_gender'] = $this->_list_gender;
        $data['list_religion'] = $this->_list_religion;
        $data['list_role'] = $this->_list_roles;
        $data['list_organization'] = $this->_list_organizations;

        $data['data']   = $value;

        $data['table_address'] = dja_table($this->_col_address, 'table table-hover table-striped my-table', 'datatable-address');
        $data['table_education'] = dja_table($this->_col_education, 'table table-hover table-striped my-table', 'datatable-education');
        $data['table_organization_experience'] = dja_table($this->_col_organization_experience, 'table table-hover table-striped my-table', 'datatable-organization_experience');
        $data['table_work_experience'] = dja_table($this->_col_work_experience, 'table table-hover table-striped my-table', 'datatable-work_experience');
        $data['table_experience'] = dja_table($this->_col_experience, 'table table-hover table-striped my-table', 'datatable-experience');

        return view('user.detail', $data);
    }

    public function edit($id)
    {
        have_permit('user.edit');

        $value = User::find($id);

        if (Auth::user()->language == 1) {
            $active_hint   = "Jika anda tidak mengaktifkannya, pengguna ini tidak akan bisa login";
            $change_hint   = "Kosongkan jika anda tidak ingin mengubahnya";
            $admin_hint    = "Jika anda mengaktifkannya, pengguna ini akan mempunyai hak akses tanpa batas";
            $password_hint = "Kosongkan jika anda tidak ingin mengubahnya";
        } else {
            $active_hint   = "If you dont activate it, this user will not be able to login";
            $change_hint   = "Leave it blank if you don't want to change it";
            $admin_hint    = "if you activate it, this user will have unlimited permission";
            $password_hint = "Leave it blank if you don't want to change it";
        }

        $data['active_hint']   = $active_hint;
        $data['change_hint']   = $change_hint;
        $data['admin_hint']    = $admin_hint;
        $data['password_hint'] = $password_hint;

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['data'] = $value;

        $data['list_gender'] = $this->_list_gender;
        $data['list_religion'] = $this->_list_religion;
        $data['list_role'] = $this->_list_roles;
        $data['list_organization'] = $this->_list_organizations;
        $data['list_mui_titles'] = $this->_list_mui_titles;
        $data['list_provinces'] = $this->_list_provinces;

        return view('user.form', $data);
    }

    public function update(Request $request, $id)
    {
        have_permit('user.edit');


        $request->validate([
            "email"    => "required|unique:users,email,{$id}",
            "picture"  => "mimes:png,jpg,jpeg",
        ]);

        $new = User::findOrFail($id);

        $new->password = $request->password  ? Hash::make($request->password) : $new->password;
        $new->is_active = isset($request->is_active) ? $request->is_active : 0;
        $new->is_admin = isset($request->is_admin)  ? $request->is_admin : 0;
        $new->role_id = $request->role_id;
        $new->email = $request->email;

        $new->serial_number = $request->serial_number;
        $new->member_number = $request->member_number;
        $new->name = $request->name;
        $new->gender = $request->gender;
        $new->last_education = $request->last_education;
        $new->name_according_to_nik = $request->name_according_to_nik;
        $new->nik_number = $request->nik_number;
        $new->birthplace = $request->birthplace;
        $new->birthdate = tgl_dt($request->birthdate);
        $new->organization_id = $request->organization_id;
        $new->phone_number = $request->phone_number;
        $new->phone_number_2 = $request->phone_number_2;
        $new->mui_title_id = $request->mui_title_id;
        $new->field = $request->field;
        $new->province_as_ktp = $request->province_as_ktp;
        $new->regency_as_ktp = $request->regency_as_ktp;
        $new->district_as_ktp = $request->district_as_ktp;
        $new->village_as_ktp = $request->village_as_ktp;
        $new->rt_as_ktp = $request->rt_as_ktp;
        $new->rw_as_ktp = $request->rw_as_ktp;
        $new->province_working_area = $request->province_working_area;
        $new->regency_working_area = $request->regency_working_area;
        $new->district_working_area = $request->district_working_area;
        $new->residence_address = $request->residence_address;

        if ($request->picture) {
            $file_name = time() . '_' . $request->picture->getClientOriginalName();
            $request->picture->move(public_path("uploads/users"), $file_name);
            $save['picture'] = $file_name;

            if (!empty($new->picture) && file_exists(public_path("uploads/users/$new->picture"))) {
                unlink(public_path("uploads/users/$new->picture"));
            }
        }

        $new->save();

        return redirect(route("user.show", $id))->with("alert-success", _l("data_successfully_changed"));
    }

    public function destroy($id)
    {
        have_permit('user.delete');

        $value = User::find($id);

        if (!empty($value->picture) && file_exists(public_path("uploads/users/$value->picture"))) {
            unlink(public_path("uploads/users/$value->picture"));
        }
        $value->deleted_by = auth()->user()->id;
        $value->save();
        $value->delete();

        return redirect()->route('user.index')->with("alert-success",  _l("data_successfully_deleted"));
    }

    public function delete_picture($id)
    {
        have_permit('user.edit');

        $value = User::find($id);

        if (!empty($value->picture) && file_exists(public_path("uploads/users/$value->picture"))) {
            unlink(public_path("uploads/users/$value->picture"));
        }

        $value->update(['picture' => '']);

        return redirect()->back()->with("alert-success", _l("picture_successfully_deleted"));
    }

    public function emailCheck(Request $request)
    {
        $data = User::where(['email' => $request->email])->first();
        $output = [];

        $output['status'] = 1;
        $output['message'] = "Email tersedia";

        if ($data) {
            $output['status'] = 0;
            $output['message'] = "Email sudah digunakan";
        }

        return response()->json($output);
    }

    public function mark_biodata($id)
    {
        $user = User::find($id);
        $user->is_biodata_done = 1;
        $user->save();

        return redirect(route("user.show", $id))->with("alert-success", "User marked as completed biodata.");
    }

    public function address_table(Request $request, $id)
    {
        $sql = "SELECT
                    user_addresses.id AS id,
                    user_addresses.user_id AS id_user,
                    user_addresses.address AS long_address,
                    user_addresses.postal_code AS postal_code,
                    provinces.name AS province,
                    regencies.name AS regency,
                    districts.name AS district,
                    villages.name AS village
                FROM user_addresses
                    LEFT JOIN provinces ON user_addresses.province_id = provinces.id
                    LEFT JOIN regencies ON user_addresses.regency_id = regencies.id
                    LEFT JOIN districts ON user_addresses.district_id = districts.id
                    LEFT JOIN villages ON user_addresses.village_id = villages.id
                WHERE user_addresses.deleted_at IS NULL
                    AND user_addresses.user_id = '$id'
        ";

        $having = " HAVING province IS NOT NULL";
        $sql_all = "$sql $having";

        foreach ($this->_col_address as $key => $val) {
            if ($val['type'] == 'option' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " = '" . $_POST[$key] . "'";
            }

            if ($val['type'] == 'like' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " LIKE '%" . $_POST[$key] . "%'";
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_from"]) && $_POST[$key . "_from"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key >= '" . tgl_dt($_POST[$key . "_from"]) . "'";
                } else {
                    $having .= " AND " . $key . " >= " . (int) filter_var($_POST[$key . "_from"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_to"]) && $_POST[$key . "_to"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key <= '" . tgl_dt($_POST[$key . "_to"]) . "'";
                } else {
                    $having .= " AND $key <= " . (int) filter_var($_POST[$key . "_to"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }
        }

        $sql_count = "$sql $having";
        $sql .= " $having";

        if (isset($_POST["order"][0]["column"])) {
            $list_columns = array_keys($this->_col_address);
            $sql .= " ORDER BY " . $list_columns[$_POST["order"][0]["column"]] . " " . $_POST["order"][0]["dir"];
        }

        if ($_POST['length'] != -1) {
            $sql .= " LIMIT " . $_POST['length'] . " OFFSET " .  $_POST['start'];
        }

        $list = DB::select($sql);

        $data = [];
        foreach ($list as $ls) {
            $row = [];

            $route = route("user.address.edit", [$ls->id_user, $ls->id]);

            $row[] = "<a href='$route'>$ls->province</a>";
            $row[] = "<a href='$route'>$ls->regency</a>";
            $row[] = "<a href='$route'>$ls->district</a>";
            $row[] = "<a href='$route'>$ls->village</a>";
            $row[] = "<a href='$route'>$ls->postal_code</a>";
            $row[] = "<a href='$route'>$ls->long_address</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => count(DB::select($sql_all)),
            "recordsFiltered" => count(DB::select($sql_count)),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function address_create($user_id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);

        $data['list_provinces'] = $this->_list_provinces;

        return view('berkas.index', $data);
    }

    public function address_store($user_id, Request $request)
    {
        $request->validate([
            "province_id" => "required",
            "regency_id" => "required",
            "district_id" => "required",
            "village_id" => "required",
            "address" => "required",
            "postal_code" => "required",
        ]);

        $target = new UserAddress;
        $target->user_id = $user_id;
        $target->province_id = $request->province_id;
        $target->regency_id = $request->regency_id;
        $target->district_id = $request->district_id;
        $target->village_id = $request->village_id;
        $target->address = $request->address;
        $target->postal_code = $request->postal_code;

        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_saved_successfully"));
    }

    public function address_edit($user_id, $id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);
        $data['data'] = UserAddress::find($id);

        $data['list_provinces'] = $this->_list_provinces;

        return view('user.address.form', $data);
    }

    public function address_update($user_id, $id, Request $request)
    {
        $request->validate([
            "province_id" => "required",
            "regency_id" => "required",
            "district_id" => "required",
            "village_id" => "required",
            "address" => "required",
            "postal_code" => "required",
        ]);

        $target = UserAddress::find($id);
        $target->user_id = $user_id;
        $target->province_id = $request->province_id;
        $target->regency_id = $request->regency_id;
        $target->district_id = $request->district_id;
        $target->village_id = $request->village_id;
        $target->address = $request->address;
        $target->postal_code = $request->postal_code;

        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function address_destroy($user_id, $address_id)
    {
        $target = UserAddress::find($address_id);

        $target->deleted_by = auth()->user()->id;
        $target->save();

        $target->delete();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_deleted_successfully"));
    }

    public function education_table(Request $request, $id)
    {
        $sql = "SELECT
                user_educations.id AS id,
                user_educations.user_id AS id_user,
                user_educations.name AS education_name,
                user_educations.start_date AS start_date,
                user_educations.end_date AS end_date
            FROM user_educations
            WHERE user_educations.deleted_at IS NULL
                AND user_educations.user_id = '$id'
        ";

        $having = " HAVING education_name IS NOT NULL";
        $sql_all = "$sql $having";

        foreach ($this->_col_education as $key => $val) {
            if ($val['type'] == 'option' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " = '" . $_POST[$key] . "'";
            }

            if ($val['type'] == 'like' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " LIKE '%" . $_POST[$key] . "%'";
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_from"]) && $_POST[$key . "_from"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key >= '" . tgl_dt($_POST[$key . "_from"]) . "'";
                } else {
                    $having .= " AND " . $key . " >= " . (int) filter_var($_POST[$key . "_from"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_to"]) && $_POST[$key . "_to"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key <= '" . tgl_dt($_POST[$key . "_to"]) . "'";
                } else {
                    $having .= " AND $key <= " . (int) filter_var($_POST[$key . "_to"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }
        }

        $sql_count = "$sql $having";
        $sql .= " $having";

        if (isset($_POST["order"][0]["column"])) {
            $list_columns = array_keys($this->_col_education);
            $sql .= " ORDER BY " . $list_columns[$_POST["order"][0]["column"]] . " " . $_POST["order"][0]["dir"];
        }

        if ($_POST['length'] != -1) {
            $sql .= " LIMIT " . $_POST['length'] . " OFFSET " .  $_POST['start'];
        }

        $list = DB::select($sql);

        $data = [];
        foreach ($list as $ls) {
            $row = [];

            $route = route("user.education.edit", [$ls->id_user, $ls->id]);

            $row[] = "<a href='$route'>$ls->education_name</a>";
            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->start_date) . "</a>";
            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->end_date) . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => count(DB::select($sql_all)),
            "recordsFiltered" => count(DB::select($sql_count)),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function education_create($user_id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);

        return view('user.education.form', $data);
    }

    public function education_store($user_id, Request $request)
    {
        $request->validate([
            "name" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ]);

        $target = new UserEducation();
        $target->user_id = $user_id;
        $target->name = $request->name;
        $target->start_date = tgl_dt($request->start_date);
        $target->end_date = tgl_dt($request->end_date);

        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_saved_successfully"));
    }

    public function education_edit($user_id, $id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);
        $data['data'] = UserEducation::find($id);

        return view('user.education.form', $data);
    }

    public function education_update($user_id, $id, Request $request)
    {
        $request->validate([
            "name" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ]);

        $target = UserEducation::find($id);
        $target->user_id = $user_id;
        $target->name = $request->name;
        $target->start_date = tgl_dt($request->start_date);
        $target->end_date = tgl_dt($request->end_date);

        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function education_destroy($user_id, $address_id)
    {
        $target = UserEducation::find($address_id);

        $target->deleted_by = auth()->user()->id;
        $target->save();

        $target->delete();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_deleted_successfully"));
    }

    public function organization_experience_table(Request $request, $id)
    {
        $sql = "SELECT
                user_organization_experiences.id AS id,
                user_organization_experiences.user_id AS id_user,
                user_organization_experiences.date AS date,
                user_organization_experiences.title AS title,
                user_organization_experiences.description AS experience_description
            FROM user_organization_experiences
            WHERE user_organization_experiences.deleted_at IS NULL
                AND user_organization_experiences.user_id = '$id'
        ";

        $having = " HAVING experience_description IS NOT NULL";
        $sql_all = "$sql $having";

        foreach ($this->_col_organization_experience as $key => $val) {
            if ($val['type'] == 'option' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " = '" . $_POST[$key] . "'";
            }

            if ($val['type'] == 'like' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " LIKE '%" . $_POST[$key] . "%'";
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_from"]) && $_POST[$key . "_from"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key >= '" . tgl_dt($_POST[$key . "_from"]) . "'";
                } else {
                    $having .= " AND " . $key . " >= " . (int) filter_var($_POST[$key . "_from"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_to"]) && $_POST[$key . "_to"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key <= '" . tgl_dt($_POST[$key . "_to"]) . "'";
                } else {
                    $having .= " AND $key <= " . (int) filter_var($_POST[$key . "_to"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }
        }

        $sql_count = "$sql $having";
        $sql .= " $having";

        if (isset($_POST["order"][0]["column"])) {
            $list_columns = array_keys($this->_col_organization_experience);
            $sql .= " ORDER BY " . $list_columns[$_POST["order"][0]["column"]] . " " . $_POST["order"][0]["dir"];
        }

        if ($_POST['length'] != -1) {
            $sql .= " LIMIT " . $_POST['length'] . " OFFSET " .  $_POST['start'];
        }

        $list = DB::select($sql);

        $data = [];
        foreach ($list as $ls) {
            $row = [];

            $route = route("user.organization-experience.edit", [$ls->id_user, $ls->id]);

            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->date) . "</a>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>$ls->experience_description</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => count(DB::select($sql_all)),
            "recordsFiltered" => count(DB::select($sql_count)),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function organization_experience_create($user_id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);

        return view('user.organization-experience.form', $data);
    }

    public function organization_experience_store($user_id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "description" => "required",
            "date" => "required",
        ]);

        $target = new UserOrganizationExperience();
        $target->user_id = $user_id;
        $target->title = $request->title;
        $target->description = $request->description;
        $target->date = tgl_dt($request->date);

        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_saved_successfully"));
    }

    public function organization_experience_edit($user_id, $id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);
        $data['data'] = UserOrganizationExperience::find($id);

        return view('user.organization-experience.form', $data);
    }

    public function organization_experience_update($user_id, $id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "description" => "required",
            "date" => "required",
        ]);

        $target = UserOrganizationExperience::find($id);
        $target->user_id = $user_id;
        $target->title = $request->title;
        $target->description = $request->description;
        $target->date = tgl_dt($request->date);

        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function organization_experience_destroy($user_id, $address_id)
    {
        $target = UserOrganizationExperience::find($address_id);

        $target->deleted_by = auth()->user()->id;
        $target->save();

        $target->delete();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_deleted_successfully"));
    }

    public function work_experience_table(Request $request, $id)
    {
        $sql = "SELECT
                user_work_experiences.id AS id,
                user_work_experiences.user_id AS id_user,
                user_work_experiences.start_date AS start_date,
                user_work_experiences.end_date AS end_date,
                user_work_experiences.title AS title,
                user_work_experiences.company AS company
            FROM user_work_experiences
            WHERE user_work_experiences.deleted_at IS NULL
                AND user_work_experiences.user_id = '$id'
        ";

        $having = " HAVING company IS NOT NULL";
        $sql_all = "$sql $having";

        foreach ($this->_col_work_experience as $key => $val) {
            if ($val['type'] == 'option' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " = '" . $_POST[$key] . "'";
            }

            if ($val['type'] == 'like' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " LIKE '%" . $_POST[$key] . "%'";
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_from"]) && $_POST[$key . "_from"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key >= '" . tgl_dt($_POST[$key . "_from"]) . "'";
                } else {
                    $having .= " AND " . $key . " >= " . (int) filter_var($_POST[$key . "_from"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_to"]) && $_POST[$key . "_to"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key <= '" . tgl_dt($_POST[$key . "_to"]) . "'";
                } else {
                    $having .= " AND $key <= " . (int) filter_var($_POST[$key . "_to"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }
        }

        $sql_count = "$sql $having";
        $sql .= " $having";

        if (isset($_POST["order"][0]["column"])) {
            $list_columns = array_keys($this->_col_work_experience);
            $sql .= " ORDER BY " . $list_columns[$_POST["order"][0]["column"]] . " " . $_POST["order"][0]["dir"];
        }

        if ($_POST['length'] != -1) {
            $sql .= " LIMIT " . $_POST['length'] . " OFFSET " .  $_POST['start'];
        }

        $list = DB::select($sql);

        $data = [];
        foreach ($list as $ls) {
            $row = [];

            $route = route("user.work-experience.edit", [$ls->id_user, $ls->id]);

            $row[] = "<a href='$route'>$ls->company</a>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->start_date) . "</a>";
            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->end_date) . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => count(DB::select($sql_all)),
            "recordsFiltered" => count(DB::select($sql_count)),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function work_experience_create($user_id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);

        return view('user.work-experience.form', $data);
    }

    public function work_experience_store($user_id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "company" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ]);

        $target = new UserWorkExperience;
        $target->user_id = $user_id;
        $target->company = $request->company;
        $target->title = $request->title;
        $target->start_date = tgl_dt($request->start_date);
        $target->end_date = tgl_dt($request->end_date);

        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_saved_successfully"));
    }

    public function work_experience_edit($user_id, $id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);
        $data['data'] = UserWorkExperience::find($id);

        return view('user.work-experience.form', $data);
    }

    public function work_experience_update($user_id, $id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "description" => "required",
            "date" => "required",
        ]);

        $target = UserWorkExperience::find($id);
        $target->user_id = $user_id;
        $target->company = $request->company;
        $target->title = $request->title;
        $target->start_date = tgl_dt($request->start_date);
        $target->end_date = tgl_dt($request->end_date);

        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function work_experience_destroy($user_id, $address_id)
    {
        $target = UserWorkExperience::find($address_id);

        $target->deleted_by = auth()->user()->id;
        $target->save();

        $target->delete();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_deleted_successfully"));
    }


    public function experience_table(Request $request, $id)
    {
        $sql = "SELECT
                user_experiences.id AS id,
                user_experiences.user_id AS id_user,
                user_experiences.date AS date,
                user_experiences.title AS title,
                user_experiences.role AS role,
                user_experiences.location AS location
            FROM user_experiences
            WHERE user_experiences.deleted_at IS NULL
                AND user_experiences.user_id = '$id'
        ";

        $having = " HAVING date IS NOT NULL";
        $sql_all = "$sql $having";

        foreach ($this->_col_experience as $key => $val) {
            if ($val['type'] == 'option' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " = '" . $_POST[$key] . "'";
            }

            if ($val['type'] == 'like' && isset($_POST[$key]) && $_POST[$key]) {
                $having .= " AND " . $key . " LIKE '%" . $_POST[$key] . "%'";
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_from"]) && $_POST[$key . "_from"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key >= '" . tgl_dt($_POST[$key . "_from"]) . "'";
                } else {
                    $having .= " AND " . $key . " >= " . (int) filter_var($_POST[$key . "_from"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }

            if ($val['type'] == 'from' && isset($_POST[$key . "_to"]) && $_POST[$key . "_to"]) {
                if (isset($val['sub-type']) && $val['sub-type'] == 'datetime') {
                    $having .= " AND $key <= '" . tgl_dt($_POST[$key . "_to"]) . "'";
                } else {
                    $having .= " AND $key <= " . (int) filter_var($_POST[$key . "_to"], FILTER_SANITIZE_NUMBER_INT) . "";
                }
            }
        }

        $sql_count = "$sql $having";
        $sql .= " $having";

        if (isset($_POST["order"][0]["column"])) {
            $list_columns = array_keys($this->_col_experience);
            $sql .= " ORDER BY " . $list_columns[$_POST["order"][0]["column"]] . " " . $_POST["order"][0]["dir"];
        }

        if ($_POST['length'] != -1) {
            $sql .= " LIMIT " . $_POST['length'] . " OFFSET " .  $_POST['start'];
        }

        $list = DB::select($sql);

        $data = [];
        foreach ($list as $ls) {
            $row = [];

            $route = route("user.experience.edit", [$ls->id_user, $ls->id]);

            $row[] = "<a href='$route' class='text-right'>" . tgl_indo($ls->date) . "</a>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>$ls->role</a>";
            $row[] = "<a href='$route'>$ls->location</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => count(DB::select($sql_all)),
            "recordsFiltered" => count(DB::select($sql_count)),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function experience_create($user_id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);

        return view('user.experience.form', $data);
    }

    public function experience_store($user_id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "role" => "required",
            "date" => "required",
            "location" => "required",
        ]);

        $target = new UserExperience;
        $target->user_id = $user_id;
        $target->title = $request->title;
        $target->role = $request->role;
        $target->location = $request->location;
        $target->date = tgl_dt($request->date);

        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_saved_successfully"));
    }

    public function experience_edit($user_id, $id)
    {
        have_permit('user.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['user'] = User::find($user_id);
        $data['data'] = UserExperience::find($id);

        return view('user.experience.form', $data);
    }

    public function experience_update($user_id, $id, Request $request)
    {
        $request->validate([
            "title" => "required",
            "role" => "required",
            "date" => "required",
            "location" => "required",
        ]);

        $target = UserExperience::find($id);
        $target->user_id = $user_id;
        $target->title = $request->title;
        $target->role = $request->role;
        $target->location = $request->location;
        $target->date = tgl_dt($request->date);


        $target->updated_by = auth()->user()->id;

        $target->save();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function experience_destroy($user_id, $address_id)
    {
        $target = UserExperience::find($address_id);

        $target->deleted_by = auth()->user()->id;
        $target->save();

        $target->delete();

        return redirect(route("user.show", $user_id))->with("alert-success", _l("data_deleted_successfully"));
    }
}
