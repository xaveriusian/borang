<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{


    public function __construct()
    {
        $this->_var = [
            "menu"  => "meeting_result",
            "title" => "meeting result",
        ];

        $this->_list_organizations = dja_get_all_list(["table" => "organizations", "key" => "id", "val" => "name"]);
        $this->_list_organizations = array_merge(["general" => "Umum"], $this->_list_organizations);
    }

    public function index()
    {
        $data['menu']  = $this->_var['menu'];
        $data['title'] = $this->_var["title"];

        return view('auth.register', $data);
    }

    public function teologian()
    {
        $data['menu']  = $this->_var['menu'];
        $data['title'] = $this->_var["title"];

        $data['list_organizations'] = $this->_list_organizations;

        $roles = Role::where('name', 'LIKE', '%ulama%')->orWhere('name', 'LIKE', '%zuama%')->orWhere('name', 'LIKE', '%cendekiawan%')->get();
        $list_roles = [];

        foreach ($roles as $rls) {
            $list_roles[$rls->id] = $rls->name;
        }

        $data['list_roles'] = $list_roles;

        return view('auth.register-teologian', $data);
    }

    public function teologian_store(Request $request)
    {
        $request->validate([
            "email"    => 'required|unique:users,email',
            "name" => "required",
            "phone_number" => "required",
            "role_id" => "required",
            "organization_id" => "required",
            "email" => "required",
            "password" => "required"
        ]);

        if ($request->password != $request->password_confirmation) {
            return redirect()->back()->with("alert-danger", "Konfirmasi password tidak sama")->withInput();
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->role_id = $request->role_id;
        $user->organization_id = $request->organization_id;
        $user->password = bcrypt($request->password);
        $user->is_active = 1;
        $user->save();

        return redirect(route('login'))->with("alert-success", "Registrasi berhasil, silahkan login menggunakan akun baru anda.");
    }

    public function user()
    {
        $data['menu']  = $this->_var['menu'];
        $data['title'] = $this->_var["title"];
        $data['list_organizations'] = $this->_list_organizations;

        return view('auth.register-user', $data);
    }

    public function user_store(Request $request)
    {
        $request->validate([
            "email"    => 'required|unique:users,email',
            "name" => "required",
            "phone_number" => "required",
            "organization_id" => "required",
            "email" => "required",
            "password" => "required"
        ]);

        if ($request->password != $request->password_confirmation) {
            return redirect()->back()->with("alert-danger", "Konfirmasi password tidak sama")->withInput();
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->organization_id = $request->organization_id;
        $user->password = bcrypt($request->password);
        $user->is_active = 1;

        $role = Role::where('name', 'LIKE', '%user%')->first();

        $user->role_id = $role->id;

        $user->save();

        return redirect(route('login'))->with("alert-success", "Registrasi berhasil, silahkan login menggunakan akun baru anda.");
    }
}
