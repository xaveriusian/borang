<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function get_regencies($prov_id)
    {
        return response()->json([
            "message" => "OK",
            "data" => Regency::where(['province_id' => $prov_id])->get()
        ]);
    }

    public function get_districts($reg_id)
    {
        return response()->json([
            "message" => "OK",
            "data" => District::where(['regency_id' => $reg_id])->get()
        ]);
    }

    public function get_villages($dis_id)
    {
        return response()->json([
            "message" => "OK",
            "data" => Village::where(['district_id' => $dis_id])->get()
        ]);
    }
}
