<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileManagerController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "file",
            "title" => "file manager",
            "icon"  => "fas fa-folder-open",
        ];
    }

    public function index()
    {
        have_permit('file.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        return view('file.index', $data);
    }
}
