<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "organization",
            "title" => "organisasi",
            "icon"  => "fas fa-users",
        ];

        $this->_table = 'organizations';
        $this->_query = [];

        $this->_col = [
            'name' => [
                'type'  => 'like',
                'title' => "name",
                'display' => [
                    'type' => 'link',
                    'link' => 'organization/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('organization.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');
        $data['form'] = dja_form($this->_col);

        return view('organization.table', $data);
    }

    public function store(Request $request)
    {
        have_permit('organization.add');

        $request->validate([
            "name"    => 'required|unique:organizations,name',
        ]);

        $target = new Organization;

        $target->name = $request->name;
        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;

        $target->save();
        return $target;
    }

    public function edit($id)
    {
        have_permit('organization.edit');

        $data = Organization::find($id);
        $data->nid = (string) $id;
        return $data;
    }

    public function update(Request $request, $id)
    {
        have_permit('organization.edit');

        $request->validate([
            "name" => "required|unique:organizations,name,{$id}",
        ]);

        $target = Organization::findOrFail($id);

        $target->name = $request->name;
        $target->updated_by = auth()->user()->id;

        $target->save();

        return $target;
    }

    public function destroy($id)
    {
        have_permit('organization.delete');

        $target = Organization::findOrFail($id);
        $target->deleted_by = auth()->user()->id;
        $target->save();

        return $target->delete();
    }
}
