<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "language",
            "title" => "language",
            "icon"  => "fas fa-language",
        ];

        $this->_table = 'languages';
        $this->_query = [];

        $this->_col = [
            'code' => [
                'type'  => 'like',
                'title' => "code",
                'display' => [
                    'type' => 'link',
                    'link' => 'language/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
            'english' => [
                'type'  => 'like',
                'title' => "english",
                'display' => [
                    'type' => 'link',
                    'link' => 'language/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
            'bahasa' => [
                'type'  => 'like',
                'title' => "bahasa",
                'display' => [
                    'type' => 'link',
                    'link' => 'language/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('language.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');
        $data['form'] = dja_form($this->_col);

        return view('language.table', $data);
    }

    public function store(Request $request)
    {
        have_permit('language.add');

        $request->validate([
            "code"    => 'required|unique:languages,code',
            "english" => 'required',
            "bahasa"  => "required",
        ]);

        $language = new Language();

        $language->code    = strtolower(str_replace(' ', '_', $request->code));

        $language->english = ucwords($request->english);
        $language->bahasa  = ucwords($request->bahasa);

        $language->save();
        return $language;
    }

    public function edit($id)
    {
        have_permit('language.edit');

        $data = Language::find($id);
        $data->nid = (string) $id;
        return $data;
    }

    public function update(Request $request, $id)
    {
        have_permit('language.edit');

        $request->validate([
            "code" => "required|unique:languages,code,{$id}",
            "english" => 'required',
            "bahasa" => "required",
        ]);

        $simpan['code']    = strtolower(str_replace(' ', '_', $request->code));
        $simpan['english'] = ucwords($request->english);
        $simpan['bahasa']  = ucwords($request->bahasa);

        Language::find($id)
            ->update($simpan);
    }

    public function destroy($id)
    {
        have_permit('language.delete');

        Language::destroy($id);
    }
}
