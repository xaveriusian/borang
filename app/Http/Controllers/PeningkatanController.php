<?php

namespace App\Http\Controllers;

use App\Models\Peningkatan;
use App\Models\Document;
use Illuminate\Http\Request;

class PeningkatanController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "peningkatan",
            "title" => "Peningkatan",
            "icon"  => "fas fa-newspaper",
        ];

        $this->_table = 'peningkatans';
        $this->_list_users = dja_get_all_list(["table" => "users", "key" => "id", "val" => "name"]);
        $this->_list_content_types = [
            "file" => "File",
            "link" => "Link",
            "papers" => "Karya Tulis",
        ];

        $this->_col = [
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('peningkatan.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('peningkatan.table', $data);
    }


    public function create()
    {
        have_permit('peningkatan.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        return view('peningkatan.form', $data);
    }

    public function store(Request $request)
    {
        have_permit('peningkatan.add');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = new Peningkatan;
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file") {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = new Document;
            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "peningkatan";
            $doc->relation_id = $target->id;
            $doc->created_by = auth()->user()->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();

        return redirect(route("peningkatan.index"))->with("alert-success", _l("data_saved_successfully"));
    }

    public function show($id)
    {
        have_permit('peningkatan.detail');

        $value = Peningkatan::find($id);

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        $data['data']   = $value;

        if ($value->content_type == "file") {
            $data['document'] = Document::where(['category' => "peningkatan", "relation_id" => $value->id])->first();
        }

        return view('peningkatan.detail', $data);
    }

    public function edit($id)
    {
        have_permit('peningkatan.edit');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['data'] = Peningkatan::findOrFail($id);

        $data['list_content_types'] = $this->_list_content_types;

        return view('peningkatan.form', $data);
    }

    public function update($id, Request $request)
    {
        have_permit('peningkatan.edit');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = Peningkatan::findOrFail($id);
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file" && $request->file) {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = Document::where(['relation_id' => $id, 'category' => 'peningkatan'])->first();

            if (!$doc) {
                $doc = new Document;
                $doc->created_by = auth()->user()->id;
            }

            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "peningkatan";
            $doc->relation_id = $target->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();
        return redirect(route("peningkatan.show", $target->id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function destroy($id)
    {
        have_permit('peningkatan.edit');

        $value = Peningkatan::findOrFail($id);
        $value->deleted_by = auth()->user()->id;
        $value->save();

        $value->delete($id);

        return redirect()->route('peningkatan.index')->with("alert-success",  _l("data_successfully_deleted"));
    }
}
