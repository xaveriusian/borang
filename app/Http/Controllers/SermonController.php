<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Sermon;
use Illuminate\Http\Request;

class SermonController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "sermon",
            "title" => "Penetapan",
            "icon"  => "fas fa-mosque",
        ];

        $this->_table = 'sermons';
        $this->_list_users = dja_get_all_list(["table" => "users", "key" => "id", "val" => "name"]);
        $this->_list_content_types = [
            "file" => "File",
            "link" => "Link",
            "papers" => "Karya Tulis",
        ];

        $this->_col = [
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        have_permit('sermon.view');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['table'] = dja_table($this->_col, 'table table-hover table-striped my-table', 'mytable');

        return view('sermon.table', $data);
    }


    public function create()
    {
        have_permit('sermon.add');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        return view('sermon.form', $data);
    }

    public function store(Request $request)
    {
        have_permit('sermon.add');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = new Sermon;
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->created_by = auth()->user()->id;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file") {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = new Document;
            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "sermon";
            $doc->relation_id = $target->id;
            $doc->created_by = auth()->user()->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();

        return redirect(route("sermon.index"))->with("alert-success", _l("data_saved_successfully"));
    }

    public function show($id)
    {
        have_permit('sermon.detail');

        $value = Sermon::find($id);

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['list_content_types'] = $this->_list_content_types;

        $data['data']   = $value;

        if ($value->content_type == "file") {
            $data['document'] = Document::where(['category' => "sermon", "relation_id" => $value->id])->first();
        }

        return view('sermon.detail', $data);
    }

    public function edit($id)
    {
        have_permit('sermon.edit');

        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];
        $data['data'] = Sermon::findOrFail($id);

        $data['list_content_types'] = $this->_list_content_types;

        return view('sermon.form', $data);
    }

    public function update($id, Request $request)
    {
        have_permit('sermon.edit');

        $request->validate([
            "title"  => 'required',
            "content_type" => 'required',
        ]);

        $target = Sermon::findOrFail($id);
        $target->title = $request->title;
        $target->content_type = $request->content_type;
        $target->updated_by = auth()->user()->id;
        $target->save();

        if ($request->content_type == 'papers') {
            $target->content = $request->content;
        } else if ($request->content_type == "link") {
            $target->link = $request->link;
        } else if ($request->content_type == "file" && $request->file) {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $request->file->move(public_path("uploads/documents"), $file_name);

            $doc = Document::where(['relation_id' => $id, 'category' => 'sermon'])->first();

            if (!$doc) {
                $doc = new Document;
                $doc->created_by = auth()->user()->id;
            }

            $doc->name = $request->title;
            $doc->link = url("/uploads/documents/$file_name");
            $doc->category = "sermon";
            $doc->relation_id = $target->id;
            $doc->updated_by = auth()->user()->id;

            $doc->save();
        }

        $target->save();
        return redirect(route("sermon.show", $target->id))->with("alert-success", _l("data_changed_successfully"));
    }

    public function destroy($id)
    {
        have_permit('sermon.edit');

        $value = Sermon::findOrFail($id);
        $value->deleted_by = auth()->user()->id;
        $value->save();

        $value->delete($id);

        return redirect()->route('sermon.index')->with("alert-success",  _l("data_successfully_deleted"));
    }
}
