<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Pengendalian;
use App\Models\Peningkatan;
use App\Models\Bulletin;
use App\Models\Document;
use App\Models\MeetingResult;
use App\Models\Project;
use App\Models\Resolution;
use App\Models\Sermon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "dashboard",
            "title" => "Dashboard",
            "icon" => "fas fa-home",
        ];

        $this->_list_users = dja_get_all_list(["table" => "users", "key" => "id", "val" => "name"]);
        $this->_list_content_types = [
            "file" => "File",
            "link" => "Link",
            "papers" => "Karya Tulis",
        ];

        $this->_list_document_categories = [
            "sermon" => "Penetapan",
            "article" => "Pelaksanaan",
            "bulletin" => "Evaluasi",
            "pengendalian" => "Pengendalian",
            "resolution" => "Peningkatan",
            "meeting_result" => "lainnya",
            "document" => "Semua Data"
        ];

        $this->_col_sermon = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'sermon/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_article = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'article/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'article/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'article/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'article/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_peningkatan = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'peningkatan/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_pengendalian = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'pengendalian/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_bulletin = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'bulletin/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'bulletin/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'bulletin/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'bulletin/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_meeting_result = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'meeting-result/',
                    'id'   => 'id',
                ],
            ],
            'meeting_date' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Meeting Date",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'meeting-result/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'meeting-result/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'meeting-result/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'meeting-result/',
                    'id'   => 'id',
                ],
            ],
        ];

        $this->_col_document = [
            'id' => [
                'type'  => '',
                'title' => "Preview",
                'required',
            ],
            'category' => [
                'type'  => 'option',
                'title' => "Category",
                'data' => $this->_list_document_categories,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'name' => [
                'type'  => 'like',
                'title' => "Name",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'document/',
                    'id'   => 'id',
                ],
            ],
        ];


        $this->_col_resolution = [
            'title' => [
                'type'  => 'like',
                'title' => "Title",
                'required',
                'display' => [
                    'type' => 'link',
                    'link' => 'resolution/',
                    'id'   => 'id',
                ],
            ],
            'date' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Declaration Date",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'resolution/',
                    'id'   => 'id',
                ],
            ],
            'created_by' => [
                'type'  => 'option',
                'title' => "Created by",
                'data' => $this->_list_users,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'resolution/',
                    'id'   => 'id',
                ],
            ],
            'created_at' => [
                'type'  => 'from',
                'sub-type' => 'datetime',
                'title' => "Created at",
                'required',
                'display' => [
                    'type' => 'link_date',
                    'link' => 'resolution/',
                    'id'   => 'id',
                ],
            ],
            'content_type' => [
                'type'  => 'option',
                'title' => "Content Type",
                'data' => $this->_list_content_types,
                'required',
                'display' => [
                    'type' => 'link_option',
                    'link' => 'resolution/',
                    'id'   => 'id',
                ],
            ],
        ];
    }

    public function index()
    {
        $data['menu']  = $this->_var["menu"];
        $data['title'] = $this->_var["title"];

        $data['table_sermon'] = dja_table($this->_col_sermon, 'table table-hover table-striped my-table', 'mytable');
        $data['table_article'] = dja_table($this->_col_article, 'table table-hover table-striped my-table', 'mytable-article');
        $data['table_bulletin'] = dja_table($this->_col_bulletin, 'table table-hover table-striped my-table', 'mytable-bulletin');
        $data['table_meeting_result'] = dja_table($this->_col_meeting_result, 'table table-hover table-striped my-table', 'mytable-meeting-result');
        $data['table_document'] = dja_table($this->_col_document, 'table table-hover table-striped my-table', 'mytable-document');
        $data['table_resolution'] = dja_table($this->_col_resolution, 'table table-hover table-striped my-table', 'mytable-resolution');
        $data['table_pengendalian'] = dja_table($this->_col_pengendalian, 'table table-hover table-striped my-table', 'mytable-pengendalian');
        $data['table_peningkatan'] = dja_table($this->_col_peningkatan, 'table table-hover table-striped my-table', 'mytable-peningkatan');

        return view('home.index', $data);
    }

    public function sermon_table(Request $request)
    {
        $this->_table = "sermons";
        $this->_query['column_search'] = $this->_col_sermon;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('sermon.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='sermon' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function article_table(Request $request)
    {
        $this->_table = "articles";
        $this->_query['column_search'] = $this->_col_article;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('article.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='article' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function pengendalian_table(Request $request)
    {
        $this->_table = "pengendalians";
        $this->_query['column_search'] = $this->_col_pengendalian;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('pengendalian.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='pengendalian' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }

    // public function lainnya_table(Request $request)
    // {
    //     $this->_table = "lainnyas";
    //     $this->_query['column_search'] = $this->_col_pengendalian;

    //     $list = dt_get($this->_table, $this->_query ?? []);
    //     $data = [];
    //     foreach ($list as $ls) {
    //         $route = route('lainnya.show', $ls->id) . "?ref=home";
    //         $row = [];
    //         $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='lainnya' value='$ls->id'></label>";
    //         $row[] = "<a href='$route'>$ls->title</a>";
    //         $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
    //         $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
    //         $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

    //         $data[] = $row;
    //     }

    //     $output = array(
    //         "draw"            => $request->draw,
    //         "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
    //         "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
    //         "data"            => $data,
    //     );

    //     return response()->json($output);
    // }

    public function peningkatan_table(Request $request)
    {
        $this->_table = "peningkatans";
        $this->_query['column_search'] = $this->_col_peningkatan;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('peningkatan.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='peningkatans' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function bulletin_table(Request $request)
    {
        $this->_table = "bulletins";
        $this->_query['column_search'] = $this->_col_bulletin;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('article.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='bulletin' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }

    public function meeting_result_table(Request $request)
    {
        $this->_table = "meeting_results";
        $this->_query['column_search'] = $this->_col_meeting_result;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('article.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='meeting_result' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->meeting_date) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }


    public function document_table(Request $request)
    {
        $this->_table = "documents";
        $this->_query['column_search'] = $this->_col_document;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('document.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='document' value='$ls->id'></label>";
            $row[] = "<a href='$route'>" . $this->_list_document_categories[$ls->category] . "</a>";
            $row[] = "<a href='$route'>$ls->name</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }


    public function resolution_table(Request $request)
    {
        $this->_table = "resolutions";
        $this->_query['column_search'] = $this->_col_resolution;

        $list = dt_get($this->_table, $this->_query ?? []);
        $data = [];
        foreach ($list as $ls) {
            $route = route('resolution.show', $ls->id) . "?ref=home";
            $row = [];
            $row[] = "<label class='text-center d-block'><input type='radio' name='preview' data-type='resolution' value='$ls->id'></label>";
            $row[] = "<a href='$route'>$ls->title</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->date) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_users[$ls->created_by] . "</a>";
            $row[] = "<a href='$route'>" . tgl_indo($ls->created_at) . "</a>";
            $row[] = "<a href='$route'>" . $this->_list_content_types[$ls->content_type] . "</a>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $request->draw,
            "recordsTotal"    => dt_count_all($this->_table, $this->_query ?? []),
            "recordsFiltered" => dt_count_filtered($this->_table, $this->_query ?? []),
            "data"            => $data,
        );

        return response()->json($output);
    }


    public function preview(Request $request)
    {
        $is_link = 1;
        $content = null;
        $title = null;

        if ($request->type == "sermon") {
            $data = Sermon::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "sermon", "relation_id" => $data->id])->first()->link;
            }
        } else if ($request->type == "article") {
            $data = Article::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "article", "relation_id" => $data->id])->first()->link;
            }
        }
        else if ($request->type == "pengendalian") {
                $data = Pengendalian::find($request->id);
                $title = $data->title;
    
                if ($data->content_type == "papers") {
                    $content = $data->content;
                    $is_link = 0;
                } else if ($data->content_type == "link") {
                    $content = $data->link;
                } else if ($data->content_type == "file") {
                    $content = Document::where(['category' => "pengendalian", "relation_id" => $data->id])->first()->link;
                }
        
        } else if ($request->type == "peningkatan") {
            $data = Peningkatan::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "peningkatan", "relation_id" => $data->id])->first()->link;
            }
        } else if ($request->type == "bulletin") {
            $data = Bulletin::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "bulletin", "relation_id" => $data->id])->first()->link;
            }
        }else if ($request->type == "lainnya") {
            $data = Lainnya::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "bulletin", "relation_id" => $data->id])->first()->link;
            }
        }
         else if ($request->type == "meeting_result") {
            $data = MeetingResult::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "meeting_result", "relation_id" => $data->id])->first()->link;
            }
        } else if ($request->type == "resolution") {
            $data = Resolution::find($request->id);
            $title = $data->title;

            if ($data->content_type == "papers") {
                $content = $data->content;
                $is_link = 0;
            } else if ($data->content_type == "link") {
                $content = $data->link;
            } else if ($data->content_type == "file") {
                $content = Document::where(['category' => "resolution", "relation_id" => $data->id])->first()->link;
            }
        } else if ($request->type == "document") {
            $data = Document::find($request->id);
            $title = $data->title;

            $content = $data->link;
        }

        return response()->json([
            "data" => [
                "is_link" => $is_link ? 1 : 0,
                "content" => $content,
                "title" => $title
            ]
        ]);
    }
}
