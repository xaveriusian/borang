<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "account",
            "title" => "account",
            "icon"  => "fas fa-user-circle"
        ];

        $this->_list_gender = [
            0 => "Tidak diketahui",
            1 => "Laki - laki",
            2 => "Perempuan",
        ];

        $this->_col_profile = [
            'name' => [
                'type'  => 'like',
                'title' => "name",
                'required',
            ],
            'picture' => [
                'type'  => 'file',
                'title' => "picture",
                'no-detail',
                'no-table',
                'accept' => "image/*",
            ],
            'email' => [
                'type'  => 'like',
                'title' => "Email",
                'required',
            ],
            'gender' => [
                'type'  => 'option',
                'title' => 'gender',
                'data' => $this->_list_gender,
                'select2',
            ],
        ];
    }

    public function index()
    {
        $value = Auth::user();

        $data['menu']  = $this->_var["menu"];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['profile'] = dja_form($this->_col_profile, $value);

        return view('account.index', $data);
    }

    public function update(Request $request)
    {
        $value = Auth::user();

        $request->validate([
            "email"   => "required|unique:users,email,{$value->id}",
            "name"    => 'required',
            "gender"  => "required",
            "picture" => "mimes:png,jpg,jpeg",
        ]);

        $user['name']   = $request->name;
        $user['email']  = $request->email;
        $user['gender'] = $request->gender;

        if ($request->picture) {
            if (!empty($value->picture) && file_exists(public_path("uploads/users/$value->picture"))) {
                unlink(public_path("uploads/users/$value->picture"));
            }

            $file_name = time() . '_' . $request->picture->getClientOriginalName();
            $request->picture->move(public_path("uploads/users"), $file_name);
            $user['picture'] = $file_name;
        }

        User::find($value->id)->update($user);

        return redirect()->back()->with("alert-success", _l("data_successfully_changed"));
    }

    public function password(Request $request)
    {
        $value = Auth::user();

        $request->validate([
            "password"  => 'required',
            "new_pass1" => "required",
            "new_pass2" => "required",
        ]);

        if (!Hash::check($request->password, $value->password)) {
            return redirect()->back()->with("alert-danger", _l("wrong_password"));
        }

        User::find($value->id)->update([
            "password" => Hash::make($request->new_pass1)
        ]);

        return redirect()->back()->with("alert-success", _l("data_successfully_changed"));
    }

    public function delete_picture(Request $request)
    {

        $value = Auth::user();

        if (!empty($value->picture) && file_exists(public_path("uploads/users/$value->picture"))) {
            unlink(public_path("uploads/users/$value->picture"));
        }

        User::find($value->id)->update(['picture' => '']);

        return redirect()->back()->with("alert-success", _l("picture_successfully_deleted"));
    }
}
