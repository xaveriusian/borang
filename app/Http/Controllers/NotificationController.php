<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->_var = [
            "menu"  => "notification",
            "title" => "notification",
            "icon"  => "typcn typcn-bell",
        ];

        $this->_table = 'notifications';
        $this->_query = [];

        $this->_col = [
            'message' => [
                'type'  => 'like',
                'title' => "notification",
                'display' => [
                    'type' => 'link',
                    'link' => 'notification/',
                    'id'   => 'id',
                    'end_link' => 'edit',
                    'my-modal'
                ],
                'required'
            ],
        ];

        $this->_query['column_search'] = $this->_col;
    }

    public function index()
    {
        $data['menu']  = $this->_var['menu'];
        $data['title'] = _l($this->_var["title"]);
        $data['icon']  = $this->_var["icon"];

        $data['data'] = Notification::where(['user' => 'p-' . Auth::user()->id])->get();

        return view('notification.table', $data);
    }

    public function readNotification()
    {
        return Notification::where(['user' => 'p-' . Auth::user()->id])->update(['read' => 1]);
    }
}
